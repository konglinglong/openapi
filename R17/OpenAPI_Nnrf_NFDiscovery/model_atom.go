/*
NRF NFDiscovery Service

NRF NFDiscovery Service.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.5
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nnrf_NFDiscovery

import (
	"encoding/json"
	"fmt"
)

// checks if the Atom type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &Atom{}

// Atom contains a search parameter and its positive or negative content.
type Atom struct {
	// contains the name of a defined query parameter.
	Attr  string      `json:"attr"`
	Value interface{} `json:"value"`
	// indicates whether the negative condition applies for the query condition.
	Negative *bool `json:"negative,omitempty"`
}

type _Atom Atom

// NewAtom instantiates a new Atom object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAtom(attr string, value interface{}) *Atom {
	this := Atom{}
	this.Attr = attr
	this.Value = value
	return &this
}

// NewAtomWithDefaults instantiates a new Atom object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAtomWithDefaults() *Atom {
	this := Atom{}
	return &this
}

// GetAttr returns the Attr field value
func (o *Atom) GetAttr() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Attr
}

// GetAttrOk returns a tuple with the Attr field value
// and a boolean to check if the value has been set.
func (o *Atom) GetAttrOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Attr, true
}

// SetAttr sets field value
func (o *Atom) SetAttr(v string) {
	o.Attr = v
}

// GetValue returns the Value field value
// If the value is explicit nil, the zero value for interface{} will be returned
func (o *Atom) GetValue() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}

	return o.Value
}

// GetValueOk returns a tuple with the Value field value
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *Atom) GetValueOk() (*interface{}, bool) {
	if o == nil || IsNil(o.Value) {
		return nil, false
	}
	return &o.Value, true
}

// SetValue sets field value
func (o *Atom) SetValue(v interface{}) {
	o.Value = v
}

// GetNegative returns the Negative field value if set, zero value otherwise.
func (o *Atom) GetNegative() bool {
	if o == nil || IsNil(o.Negative) {
		var ret bool
		return ret
	}
	return *o.Negative
}

// GetNegativeOk returns a tuple with the Negative field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Atom) GetNegativeOk() (*bool, bool) {
	if o == nil || IsNil(o.Negative) {
		return nil, false
	}
	return o.Negative, true
}

// HasNegative returns a boolean if a field has been set.
func (o *Atom) HasNegative() bool {
	if o != nil && !IsNil(o.Negative) {
		return true
	}

	return false
}

// SetNegative gets a reference to the given bool and assigns it to the Negative field.
func (o *Atom) SetNegative(v bool) {
	o.Negative = &v
}

func (o Atom) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o Atom) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["attr"] = o.Attr
	if o.Value != nil {
		toSerialize["value"] = o.Value
	}
	if !IsNil(o.Negative) {
		toSerialize["negative"] = o.Negative
	}
	return toSerialize, nil
}

func (o *Atom) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"attr",
		"value",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varAtom := _Atom{}

	err = json.Unmarshal(bytes, &varAtom)

	if err != nil {
		return err
	}

	*o = Atom(varAtom)

	return err
}

type NullableAtom struct {
	value *Atom
	isSet bool
}

func (v NullableAtom) Get() *Atom {
	return v.value
}

func (v *NullableAtom) Set(val *Atom) {
	v.value = val
	v.isSet = true
}

func (v NullableAtom) IsSet() bool {
	return v.isSet
}

func (v *NullableAtom) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAtom(val *Atom) *NullableAtom {
	return &NullableAtom{value: val, isSet: true}
}

func (v NullableAtom) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAtom) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
