/*
Nmbstf-distsession

MBSTF Distribution Session Service.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.4
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nmbstf_DistSession

import (
	"encoding/json"
)

// checks if the MbStfIngestAddr type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MbStfIngestAddr{}

// MbStfIngestAddr MBSTF Ingest Addresses
type MbStfIngestAddr struct {
	AfEgressTunAddr     NullableTunnelAddress `json:"afEgressTunAddr,omitempty"`
	MbStfIngressTunAddr NullableTunnelAddress `json:"mbStfIngressTunAddr,omitempty"`
	AfSsm               *ExtSsm               `json:"afSsm,omitempty"`
	MbStfListenAddr     NullableTunnelAddress `json:"mbStfListenAddr,omitempty"`
}

// NewMbStfIngestAddr instantiates a new MbStfIngestAddr object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMbStfIngestAddr() *MbStfIngestAddr {
	this := MbStfIngestAddr{}
	return &this
}

// NewMbStfIngestAddrWithDefaults instantiates a new MbStfIngestAddr object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMbStfIngestAddrWithDefaults() *MbStfIngestAddr {
	this := MbStfIngestAddr{}
	return &this
}

// GetAfEgressTunAddr returns the AfEgressTunAddr field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MbStfIngestAddr) GetAfEgressTunAddr() TunnelAddress {
	if o == nil || IsNil(o.AfEgressTunAddr.Get()) {
		var ret TunnelAddress
		return ret
	}
	return *o.AfEgressTunAddr.Get()
}

// GetAfEgressTunAddrOk returns a tuple with the AfEgressTunAddr field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MbStfIngestAddr) GetAfEgressTunAddrOk() (*TunnelAddress, bool) {
	if o == nil {
		return nil, false
	}
	return o.AfEgressTunAddr.Get(), o.AfEgressTunAddr.IsSet()
}

// HasAfEgressTunAddr returns a boolean if a field has been set.
func (o *MbStfIngestAddr) HasAfEgressTunAddr() bool {
	if o != nil && o.AfEgressTunAddr.IsSet() {
		return true
	}

	return false
}

// SetAfEgressTunAddr gets a reference to the given NullableTunnelAddress and assigns it to the AfEgressTunAddr field.
func (o *MbStfIngestAddr) SetAfEgressTunAddr(v TunnelAddress) {
	o.AfEgressTunAddr.Set(&v)
}

// SetAfEgressTunAddrNil sets the value for AfEgressTunAddr to be an explicit nil
func (o *MbStfIngestAddr) SetAfEgressTunAddrNil() {
	o.AfEgressTunAddr.Set(nil)
}

// UnsetAfEgressTunAddr ensures that no value is present for AfEgressTunAddr, not even an explicit nil
func (o *MbStfIngestAddr) UnsetAfEgressTunAddr() {
	o.AfEgressTunAddr.Unset()
}

// GetMbStfIngressTunAddr returns the MbStfIngressTunAddr field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MbStfIngestAddr) GetMbStfIngressTunAddr() TunnelAddress {
	if o == nil || IsNil(o.MbStfIngressTunAddr.Get()) {
		var ret TunnelAddress
		return ret
	}
	return *o.MbStfIngressTunAddr.Get()
}

// GetMbStfIngressTunAddrOk returns a tuple with the MbStfIngressTunAddr field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MbStfIngestAddr) GetMbStfIngressTunAddrOk() (*TunnelAddress, bool) {
	if o == nil {
		return nil, false
	}
	return o.MbStfIngressTunAddr.Get(), o.MbStfIngressTunAddr.IsSet()
}

// HasMbStfIngressTunAddr returns a boolean if a field has been set.
func (o *MbStfIngestAddr) HasMbStfIngressTunAddr() bool {
	if o != nil && o.MbStfIngressTunAddr.IsSet() {
		return true
	}

	return false
}

// SetMbStfIngressTunAddr gets a reference to the given NullableTunnelAddress and assigns it to the MbStfIngressTunAddr field.
func (o *MbStfIngestAddr) SetMbStfIngressTunAddr(v TunnelAddress) {
	o.MbStfIngressTunAddr.Set(&v)
}

// SetMbStfIngressTunAddrNil sets the value for MbStfIngressTunAddr to be an explicit nil
func (o *MbStfIngestAddr) SetMbStfIngressTunAddrNil() {
	o.MbStfIngressTunAddr.Set(nil)
}

// UnsetMbStfIngressTunAddr ensures that no value is present for MbStfIngressTunAddr, not even an explicit nil
func (o *MbStfIngestAddr) UnsetMbStfIngressTunAddr() {
	o.MbStfIngressTunAddr.Unset()
}

// GetAfSsm returns the AfSsm field value if set, zero value otherwise.
func (o *MbStfIngestAddr) GetAfSsm() ExtSsm {
	if o == nil || IsNil(o.AfSsm) {
		var ret ExtSsm
		return ret
	}
	return *o.AfSsm
}

// GetAfSsmOk returns a tuple with the AfSsm field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MbStfIngestAddr) GetAfSsmOk() (*ExtSsm, bool) {
	if o == nil || IsNil(o.AfSsm) {
		return nil, false
	}
	return o.AfSsm, true
}

// HasAfSsm returns a boolean if a field has been set.
func (o *MbStfIngestAddr) HasAfSsm() bool {
	if o != nil && !IsNil(o.AfSsm) {
		return true
	}

	return false
}

// SetAfSsm gets a reference to the given ExtSsm and assigns it to the AfSsm field.
func (o *MbStfIngestAddr) SetAfSsm(v ExtSsm) {
	o.AfSsm = &v
}

// GetMbStfListenAddr returns the MbStfListenAddr field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *MbStfIngestAddr) GetMbStfListenAddr() TunnelAddress {
	if o == nil || IsNil(o.MbStfListenAddr.Get()) {
		var ret TunnelAddress
		return ret
	}
	return *o.MbStfListenAddr.Get()
}

// GetMbStfListenAddrOk returns a tuple with the MbStfListenAddr field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *MbStfIngestAddr) GetMbStfListenAddrOk() (*TunnelAddress, bool) {
	if o == nil {
		return nil, false
	}
	return o.MbStfListenAddr.Get(), o.MbStfListenAddr.IsSet()
}

// HasMbStfListenAddr returns a boolean if a field has been set.
func (o *MbStfIngestAddr) HasMbStfListenAddr() bool {
	if o != nil && o.MbStfListenAddr.IsSet() {
		return true
	}

	return false
}

// SetMbStfListenAddr gets a reference to the given NullableTunnelAddress and assigns it to the MbStfListenAddr field.
func (o *MbStfIngestAddr) SetMbStfListenAddr(v TunnelAddress) {
	o.MbStfListenAddr.Set(&v)
}

// SetMbStfListenAddrNil sets the value for MbStfListenAddr to be an explicit nil
func (o *MbStfIngestAddr) SetMbStfListenAddrNil() {
	o.MbStfListenAddr.Set(nil)
}

// UnsetMbStfListenAddr ensures that no value is present for MbStfListenAddr, not even an explicit nil
func (o *MbStfIngestAddr) UnsetMbStfListenAddr() {
	o.MbStfListenAddr.Unset()
}

func (o MbStfIngestAddr) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MbStfIngestAddr) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.AfEgressTunAddr.IsSet() {
		toSerialize["afEgressTunAddr"] = o.AfEgressTunAddr.Get()
	}
	if o.MbStfIngressTunAddr.IsSet() {
		toSerialize["mbStfIngressTunAddr"] = o.MbStfIngressTunAddr.Get()
	}
	if !IsNil(o.AfSsm) {
		toSerialize["afSsm"] = o.AfSsm
	}
	if o.MbStfListenAddr.IsSet() {
		toSerialize["mbStfListenAddr"] = o.MbStfListenAddr.Get()
	}
	return toSerialize, nil
}

type NullableMbStfIngestAddr struct {
	value *MbStfIngestAddr
	isSet bool
}

func (v NullableMbStfIngestAddr) Get() *MbStfIngestAddr {
	return v.value
}

func (v *NullableMbStfIngestAddr) Set(val *MbStfIngestAddr) {
	v.value = val
	v.isSet = true
}

func (v NullableMbStfIngestAddr) IsSet() bool {
	return v.isSet
}

func (v *NullableMbStfIngestAddr) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMbStfIngestAddr(val *MbStfIngestAddr) *NullableMbStfIngestAddr {
	return &NullableMbStfIngestAddr{value: val, isSet: true}
}

func (v NullableMbStfIngestAddr) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMbStfIngestAddr) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
