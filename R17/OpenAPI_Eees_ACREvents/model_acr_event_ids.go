/*
Eees_ACREvents

API for ACR events subscription and notification. © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 1.0.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Eees_ACREvents

import (
	"encoding/json"
	"fmt"
)

// ACREventIDs Possible values are - TARGET_INFORMATION: Represents the target information event. - ACR_COMPLETE: Represents the ACR complete event.
type ACREventIDs struct {
	string *string
}

// Unmarshal JSON data into any of the pointers in the struct
func (dst *ACREventIDs) UnmarshalJSON(data []byte) error {
	var err error
	// try to unmarshal JSON data into string
	err = json.Unmarshal(data, &dst.string)
	if err == nil {
		jsonstring, _ := json.Marshal(dst.string)
		if string(jsonstring) == "{}" { // empty struct
			dst.string = nil
		} else {
			return nil // data stored in dst.string, return on the first match
		}
	} else {
		dst.string = nil
	}

	return fmt.Errorf("data failed to match schemas in anyOf(ACREventIDs)")
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src *ACREventIDs) MarshalJSON() ([]byte, error) {
	if src.string != nil {
		return json.Marshal(&src.string)
	}

	return nil, nil // no data in anyOf schemas
}

type NullableACREventIDs struct {
	value *ACREventIDs
	isSet bool
}

func (v NullableACREventIDs) Get() *ACREventIDs {
	return v.value
}

func (v *NullableACREventIDs) Set(val *ACREventIDs) {
	v.value = val
	v.isSet = true
}

func (v NullableACREventIDs) IsSet() bool {
	return v.isSet
}

func (v *NullableACREventIDs) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableACREventIDs(val *ACREventIDs) *NullableACREventIDs {
	return &NullableACREventIDs{value: val, isSet: true}
}

func (v NullableACREventIDs) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableACREventIDs) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
