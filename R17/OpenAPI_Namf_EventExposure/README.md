# Go API client for OpenAPI_Namf_EventExposure

AMF Event Exposure Service.  
© 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.2.3
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_Namf_EventExposure "gitee.com/konglinglong/openapi/OpenAPI_Namf_EventExposure"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_Namf_EventExposure.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Namf_EventExposure.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_Namf_EventExposure.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Namf_EventExposure.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_Namf_EventExposure.ContextOperationServerIndices` and `OpenAPI_Namf_EventExposure.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Namf_EventExposure.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_Namf_EventExposure.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/namf-evts/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*IndividualSubscriptionDocumentAPI* | [**DeleteSubscription**](docs/IndividualSubscriptionDocumentAPI.md#deletesubscription) | **Delete** /subscriptions/{subscriptionId} | Namf_EventExposure Unsubscribe service Operation
*IndividualSubscriptionDocumentAPI* | [**ModifySubscription**](docs/IndividualSubscriptionDocumentAPI.md#modifysubscription) | **Patch** /subscriptions/{subscriptionId} | Namf_EventExposure Subscribe Modify service Operation
*SubscriptionsCollectionCollectionAPI* | [**CreateSubscription**](docs/SubscriptionsCollectionCollectionAPI.md#createsubscription) | **Post** /subscriptions | Namf_EventExposure Subscribe service Operation


## Documentation For Models

 - [AccessStateTransitionType](docs/AccessStateTransitionType.md)
 - [AccessTokenErr](docs/AccessTokenErr.md)
 - [AccessTokenReq](docs/AccessTokenReq.md)
 - [AccessType](docs/AccessType.md)
 - [AmfCreateEventSubscription](docs/AmfCreateEventSubscription.md)
 - [AmfCreatedEventSubscription](docs/AmfCreatedEventSubscription.md)
 - [AmfEvent](docs/AmfEvent.md)
 - [AmfEventArea](docs/AmfEventArea.md)
 - [AmfEventMode](docs/AmfEventMode.md)
 - [AmfEventNotification](docs/AmfEventNotification.md)
 - [AmfEventReport](docs/AmfEventReport.md)
 - [AmfEventState](docs/AmfEventState.md)
 - [AmfEventSubsSyncInfo](docs/AmfEventSubsSyncInfo.md)
 - [AmfEventSubscription](docs/AmfEventSubscription.md)
 - [AmfEventSubscriptionInfo](docs/AmfEventSubscriptionInfo.md)
 - [AmfEventTrigger](docs/AmfEventTrigger.md)
 - [AmfEventType](docs/AmfEventType.md)
 - [AmfUpdateEventOptionItem](docs/AmfUpdateEventOptionItem.md)
 - [AmfUpdateEventSubscriptionItem](docs/AmfUpdateEventSubscriptionItem.md)
 - [AmfUpdatedEventSubscription](docs/AmfUpdatedEventSubscription.md)
 - [CellGlobalId](docs/CellGlobalId.md)
 - [CmInfo](docs/CmInfo.md)
 - [CmState](docs/CmState.md)
 - [CommunicationFailure](docs/CommunicationFailure.md)
 - [DddTrafficDescriptor](docs/DddTrafficDescriptor.md)
 - [DispersionArea](docs/DispersionArea.md)
 - [Ecgi](docs/Ecgi.md)
 - [EutraLocation](docs/EutraLocation.md)
 - [ExtSnssai](docs/ExtSnssai.md)
 - [GNbId](docs/GNbId.md)
 - [GeraLocation](docs/GeraLocation.md)
 - [GlobalRanNodeId](docs/GlobalRanNodeId.md)
 - [Guami](docs/Guami.md)
 - [HfcNodeId](docs/HfcNodeId.md)
 - [IdleStatusIndication](docs/IdleStatusIndication.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [Ipv6Addr](docs/Ipv6Addr.md)
 - [LadnInfo](docs/LadnInfo.md)
 - [LineType](docs/LineType.md)
 - [LocationAreaId](docs/LocationAreaId.md)
 - [LocationFilter](docs/LocationFilter.md)
 - [LossOfConnectivityReason](docs/LossOfConnectivityReason.md)
 - [MmTransactionLocationReportItem](docs/MmTransactionLocationReportItem.md)
 - [MmTransactionSliceReportItem](docs/MmTransactionSliceReportItem.md)
 - [Model5GsUserState](docs/Model5GsUserState.md)
 - [Model5GsUserStateInfo](docs/Model5GsUserStateInfo.md)
 - [ModifySubscriptionRequest](docs/ModifySubscriptionRequest.md)
 - [N3gaLocation](docs/N3gaLocation.md)
 - [NFType](docs/NFType.md)
 - [Ncgi](docs/Ncgi.md)
 - [NgApCause](docs/NgApCause.md)
 - [NotificationFlag](docs/NotificationFlag.md)
 - [NrLocation](docs/NrLocation.md)
 - [PartitioningCriteria](docs/PartitioningCriteria.md)
 - [PlmnId](docs/PlmnId.md)
 - [PlmnIdNid](docs/PlmnIdNid.md)
 - [PresenceInfo](docs/PresenceInfo.md)
 - [PresenceState](docs/PresenceState.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [ReachabilityFilter](docs/ReachabilityFilter.md)
 - [RedirectResponse](docs/RedirectResponse.md)
 - [RmInfo](docs/RmInfo.md)
 - [RmState](docs/RmState.md)
 - [RoutingAreaId](docs/RoutingAreaId.md)
 - [SdRange](docs/SdRange.md)
 - [ServiceAreaId](docs/ServiceAreaId.md)
 - [Snssai](docs/Snssai.md)
 - [SnssaiExtension](docs/SnssaiExtension.md)
 - [SnssaiTaiMapping](docs/SnssaiTaiMapping.md)
 - [SupportedSnssai](docs/SupportedSnssai.md)
 - [TacRange](docs/TacRange.md)
 - [Tai](docs/Tai.md)
 - [TaiRange](docs/TaiRange.md)
 - [TargetArea](docs/TargetArea.md)
 - [TnapId](docs/TnapId.md)
 - [TrafficDescriptor](docs/TrafficDescriptor.md)
 - [TransportProtocol](docs/TransportProtocol.md)
 - [TwapId](docs/TwapId.md)
 - [UEIdExt](docs/UEIdExt.md)
 - [UeAccessBehaviorReportItem](docs/UeAccessBehaviorReportItem.md)
 - [UeInAreaFilter](docs/UeInAreaFilter.md)
 - [UeLocationTrendsReportItem](docs/UeLocationTrendsReportItem.md)
 - [UeReachability](docs/UeReachability.md)
 - [UeType](docs/UeType.md)
 - [UserLocation](docs/UserLocation.md)
 - [UtraLocation](docs/UtraLocation.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: 
 - **namf-evts**: Access to the Namf_EventExposure API

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_Namf_EventExposure.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_Namf_EventExposure.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



