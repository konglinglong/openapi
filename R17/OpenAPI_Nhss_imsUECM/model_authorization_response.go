/*
Nhss_imsUECM

Nhss UE Context Management Service for IMS.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.1.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nhss_imsUECM

import (
	"encoding/json"
	"fmt"
)

// checks if the AuthorizationResponse type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AuthorizationResponse{}

// AuthorizationResponse Ims Registration authorization information result
type AuthorizationResponse struct {
	AuthorizationResult          AuthorizationResult                         `json:"authorizationResult"`
	CscfServerName               *string                                     `json:"cscfServerName,omitempty"`
	ScscfSelectionAssistanceInfo NullableScscfSelectionAssistanceInformation `json:"scscfSelectionAssistanceInfo,omitempty"`
}

type _AuthorizationResponse AuthorizationResponse

// NewAuthorizationResponse instantiates a new AuthorizationResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAuthorizationResponse(authorizationResult AuthorizationResult) *AuthorizationResponse {
	this := AuthorizationResponse{}
	return &this
}

// NewAuthorizationResponseWithDefaults instantiates a new AuthorizationResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAuthorizationResponseWithDefaults() *AuthorizationResponse {
	this := AuthorizationResponse{}
	return &this
}

// GetAuthorizationResult returns the AuthorizationResult field value
func (o *AuthorizationResponse) GetAuthorizationResult() AuthorizationResult {
	if o == nil {
		var ret AuthorizationResult
		return ret
	}

	return o.AuthorizationResult
}

// GetAuthorizationResultOk returns a tuple with the AuthorizationResult field value
// and a boolean to check if the value has been set.
func (o *AuthorizationResponse) GetAuthorizationResultOk() (*AuthorizationResult, bool) {
	if o == nil {
		return nil, false
	}
	return &o.AuthorizationResult, true
}

// SetAuthorizationResult sets field value
func (o *AuthorizationResponse) SetAuthorizationResult(v AuthorizationResult) {
	o.AuthorizationResult = v
}

// GetCscfServerName returns the CscfServerName field value if set, zero value otherwise.
func (o *AuthorizationResponse) GetCscfServerName() string {
	if o == nil || IsNil(o.CscfServerName) {
		var ret string
		return ret
	}
	return *o.CscfServerName
}

// GetCscfServerNameOk returns a tuple with the CscfServerName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AuthorizationResponse) GetCscfServerNameOk() (*string, bool) {
	if o == nil || IsNil(o.CscfServerName) {
		return nil, false
	}
	return o.CscfServerName, true
}

// HasCscfServerName returns a boolean if a field has been set.
func (o *AuthorizationResponse) HasCscfServerName() bool {
	if o != nil && !IsNil(o.CscfServerName) {
		return true
	}

	return false
}

// SetCscfServerName gets a reference to the given string and assigns it to the CscfServerName field.
func (o *AuthorizationResponse) SetCscfServerName(v string) {
	o.CscfServerName = &v
}

// GetScscfSelectionAssistanceInfo returns the ScscfSelectionAssistanceInfo field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AuthorizationResponse) GetScscfSelectionAssistanceInfo() ScscfSelectionAssistanceInformation {
	if o == nil || IsNil(o.ScscfSelectionAssistanceInfo.Get()) {
		var ret ScscfSelectionAssistanceInformation
		return ret
	}
	return *o.ScscfSelectionAssistanceInfo.Get()
}

// GetScscfSelectionAssistanceInfoOk returns a tuple with the ScscfSelectionAssistanceInfo field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AuthorizationResponse) GetScscfSelectionAssistanceInfoOk() (*ScscfSelectionAssistanceInformation, bool) {
	if o == nil {
		return nil, false
	}
	return o.ScscfSelectionAssistanceInfo.Get(), o.ScscfSelectionAssistanceInfo.IsSet()
}

// HasScscfSelectionAssistanceInfo returns a boolean if a field has been set.
func (o *AuthorizationResponse) HasScscfSelectionAssistanceInfo() bool {
	if o != nil && o.ScscfSelectionAssistanceInfo.IsSet() {
		return true
	}

	return false
}

// SetScscfSelectionAssistanceInfo gets a reference to the given NullableScscfSelectionAssistanceInformation and assigns it to the ScscfSelectionAssistanceInfo field.
func (o *AuthorizationResponse) SetScscfSelectionAssistanceInfo(v ScscfSelectionAssistanceInformation) {
	o.ScscfSelectionAssistanceInfo.Set(&v)
}

// SetScscfSelectionAssistanceInfoNil sets the value for ScscfSelectionAssistanceInfo to be an explicit nil
func (o *AuthorizationResponse) SetScscfSelectionAssistanceInfoNil() {
	o.ScscfSelectionAssistanceInfo.Set(nil)
}

// UnsetScscfSelectionAssistanceInfo ensures that no value is present for ScscfSelectionAssistanceInfo, not even an explicit nil
func (o *AuthorizationResponse) UnsetScscfSelectionAssistanceInfo() {
	o.ScscfSelectionAssistanceInfo.Unset()
}

func (o AuthorizationResponse) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AuthorizationResponse) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["authorizationResult"] = o.AuthorizationResult
	if !IsNil(o.CscfServerName) {
		toSerialize["cscfServerName"] = o.CscfServerName
	}
	if o.ScscfSelectionAssistanceInfo.IsSet() {
		toSerialize["scscfSelectionAssistanceInfo"] = o.ScscfSelectionAssistanceInfo.Get()
	}
	return toSerialize, nil
}

func (o *AuthorizationResponse) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"authorizationResult",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varAuthorizationResponse := _AuthorizationResponse{}

	err = json.Unmarshal(bytes, &varAuthorizationResponse)

	if err != nil {
		return err
	}

	*o = AuthorizationResponse(varAuthorizationResponse)

	return err
}

type NullableAuthorizationResponse struct {
	value *AuthorizationResponse
	isSet bool
}

func (v NullableAuthorizationResponse) Get() *AuthorizationResponse {
	return v.value
}

func (v *NullableAuthorizationResponse) Set(val *AuthorizationResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableAuthorizationResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableAuthorizationResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAuthorizationResponse(val *AuthorizationResponse) *NullableAuthorizationResponse {
	return &NullableAuthorizationResponse{value: val, isSet: true}
}

func (v NullableAuthorizationResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAuthorizationResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
