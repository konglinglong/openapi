/*
Nhss_imsUECM

Nhss UE Context Management Service for IMS.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.1.1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nhss_imsUECM

import (
	"encoding/json"
	"fmt"
)

// checks if the DeregistrationReason type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DeregistrationReason{}

// DeregistrationReason Contains the reason for the network initiated de-registration (including a reason code, and a human-readable reason text)
type DeregistrationReason struct {
	ReasonCode DeregistrationReasonCode `json:"reasonCode"`
	ReasonText string                   `json:"reasonText"`
}

type _DeregistrationReason DeregistrationReason

// NewDeregistrationReason instantiates a new DeregistrationReason object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDeregistrationReason(reasonCode DeregistrationReasonCode, reasonText string) *DeregistrationReason {
	this := DeregistrationReason{}
	this.ReasonCode = reasonCode
	this.ReasonText = reasonText
	return &this
}

// NewDeregistrationReasonWithDefaults instantiates a new DeregistrationReason object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDeregistrationReasonWithDefaults() *DeregistrationReason {
	this := DeregistrationReason{}
	return &this
}

// GetReasonCode returns the ReasonCode field value
func (o *DeregistrationReason) GetReasonCode() DeregistrationReasonCode {
	if o == nil {
		var ret DeregistrationReasonCode
		return ret
	}

	return o.ReasonCode
}

// GetReasonCodeOk returns a tuple with the ReasonCode field value
// and a boolean to check if the value has been set.
func (o *DeregistrationReason) GetReasonCodeOk() (*DeregistrationReasonCode, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ReasonCode, true
}

// SetReasonCode sets field value
func (o *DeregistrationReason) SetReasonCode(v DeregistrationReasonCode) {
	o.ReasonCode = v
}

// GetReasonText returns the ReasonText field value
func (o *DeregistrationReason) GetReasonText() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ReasonText
}

// GetReasonTextOk returns a tuple with the ReasonText field value
// and a boolean to check if the value has been set.
func (o *DeregistrationReason) GetReasonTextOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ReasonText, true
}

// SetReasonText sets field value
func (o *DeregistrationReason) SetReasonText(v string) {
	o.ReasonText = v
}

func (o DeregistrationReason) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DeregistrationReason) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["reasonCode"] = o.ReasonCode
	toSerialize["reasonText"] = o.ReasonText
	return toSerialize, nil
}

func (o *DeregistrationReason) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"reasonCode",
		"reasonText",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varDeregistrationReason := _DeregistrationReason{}

	err = json.Unmarshal(bytes, &varDeregistrationReason)

	if err != nil {
		return err
	}

	*o = DeregistrationReason(varDeregistrationReason)

	return err
}

type NullableDeregistrationReason struct {
	value *DeregistrationReason
	isSet bool
}

func (v NullableDeregistrationReason) Get() *DeregistrationReason {
	return v.value
}

func (v *NullableDeregistrationReason) Set(val *DeregistrationReason) {
	v.value = val
	v.isSet = true
}

func (v NullableDeregistrationReason) IsSet() bool {
	return v.isSet
}

func (v *NullableDeregistrationReason) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDeregistrationReason(val *DeregistrationReason) *NullableDeregistrationReason {
	return &NullableDeregistrationReason{value: val, isSet: true}
}

func (v NullableDeregistrationReason) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDeregistrationReason) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
