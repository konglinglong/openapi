/*
Ntsctsf_TimeSynchronization Service API

TSCTSF Time Synchronization Service.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Ntsctsf_TimeSynchronization

import (
	"encoding/json"
	"fmt"
)

// checks if the TimeSyncCapability type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &TimeSyncCapability{}

// TimeSyncCapability Contains the capability of time synchronization service.
type TimeSyncCapability struct {
	// Integer where the allowed values correspond to the value range of an unsigned 64-bit integer.
	UpNodeId   int32           `json:"upNodeId"`
	GmCapables []GmCapable     `json:"gmCapables,omitempty"`
	AsTimeRes  *AsTimeResource `json:"asTimeRes,omitempty"`
	// Contains the PTP capabilities supported by each of the SUPI(s). The key of the map is the SUPI.
	PtpCapForUes *map[string]PtpCapabilitiesPerUe `json:"ptpCapForUes,omitempty"`
	// Contains the PTP capabilities supported by each of the GPSI(s). The key of the map is the GPSI.
	PtpCapForGpsis *map[string]PtpCapabilitiesPerUe `json:"ptpCapForGpsis,omitempty"`
}

type _TimeSyncCapability TimeSyncCapability

// NewTimeSyncCapability instantiates a new TimeSyncCapability object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTimeSyncCapability(upNodeId int32) *TimeSyncCapability {
	this := TimeSyncCapability{}
	return &this
}

// NewTimeSyncCapabilityWithDefaults instantiates a new TimeSyncCapability object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTimeSyncCapabilityWithDefaults() *TimeSyncCapability {
	this := TimeSyncCapability{}
	return &this
}

// GetUpNodeId returns the UpNodeId field value
func (o *TimeSyncCapability) GetUpNodeId() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.UpNodeId
}

// GetUpNodeIdOk returns a tuple with the UpNodeId field value
// and a boolean to check if the value has been set.
func (o *TimeSyncCapability) GetUpNodeIdOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UpNodeId, true
}

// SetUpNodeId sets field value
func (o *TimeSyncCapability) SetUpNodeId(v int32) {
	o.UpNodeId = v
}

// GetGmCapables returns the GmCapables field value if set, zero value otherwise.
func (o *TimeSyncCapability) GetGmCapables() []GmCapable {
	if o == nil || IsNil(o.GmCapables) {
		var ret []GmCapable
		return ret
	}
	return o.GmCapables
}

// GetGmCapablesOk returns a tuple with the GmCapables field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeSyncCapability) GetGmCapablesOk() ([]GmCapable, bool) {
	if o == nil || IsNil(o.GmCapables) {
		return nil, false
	}
	return o.GmCapables, true
}

// HasGmCapables returns a boolean if a field has been set.
func (o *TimeSyncCapability) HasGmCapables() bool {
	if o != nil && !IsNil(o.GmCapables) {
		return true
	}

	return false
}

// SetGmCapables gets a reference to the given []GmCapable and assigns it to the GmCapables field.
func (o *TimeSyncCapability) SetGmCapables(v []GmCapable) {
	o.GmCapables = v
}

// GetAsTimeRes returns the AsTimeRes field value if set, zero value otherwise.
func (o *TimeSyncCapability) GetAsTimeRes() AsTimeResource {
	if o == nil || IsNil(o.AsTimeRes) {
		var ret AsTimeResource
		return ret
	}
	return *o.AsTimeRes
}

// GetAsTimeResOk returns a tuple with the AsTimeRes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeSyncCapability) GetAsTimeResOk() (*AsTimeResource, bool) {
	if o == nil || IsNil(o.AsTimeRes) {
		return nil, false
	}
	return o.AsTimeRes, true
}

// HasAsTimeRes returns a boolean if a field has been set.
func (o *TimeSyncCapability) HasAsTimeRes() bool {
	if o != nil && !IsNil(o.AsTimeRes) {
		return true
	}

	return false
}

// SetAsTimeRes gets a reference to the given AsTimeResource and assigns it to the AsTimeRes field.
func (o *TimeSyncCapability) SetAsTimeRes(v AsTimeResource) {
	o.AsTimeRes = &v
}

// GetPtpCapForUes returns the PtpCapForUes field value if set, zero value otherwise.
func (o *TimeSyncCapability) GetPtpCapForUes() map[string]PtpCapabilitiesPerUe {
	if o == nil || IsNil(o.PtpCapForUes) {
		var ret map[string]PtpCapabilitiesPerUe
		return ret
	}
	return *o.PtpCapForUes
}

// GetPtpCapForUesOk returns a tuple with the PtpCapForUes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeSyncCapability) GetPtpCapForUesOk() (*map[string]PtpCapabilitiesPerUe, bool) {
	if o == nil || IsNil(o.PtpCapForUes) {
		return nil, false
	}
	return o.PtpCapForUes, true
}

// HasPtpCapForUes returns a boolean if a field has been set.
func (o *TimeSyncCapability) HasPtpCapForUes() bool {
	if o != nil && !IsNil(o.PtpCapForUes) {
		return true
	}

	return false
}

// SetPtpCapForUes gets a reference to the given map[string]PtpCapabilitiesPerUe and assigns it to the PtpCapForUes field.
func (o *TimeSyncCapability) SetPtpCapForUes(v map[string]PtpCapabilitiesPerUe) {
	o.PtpCapForUes = &v
}

// GetPtpCapForGpsis returns the PtpCapForGpsis field value if set, zero value otherwise.
func (o *TimeSyncCapability) GetPtpCapForGpsis() map[string]PtpCapabilitiesPerUe {
	if o == nil || IsNil(o.PtpCapForGpsis) {
		var ret map[string]PtpCapabilitiesPerUe
		return ret
	}
	return *o.PtpCapForGpsis
}

// GetPtpCapForGpsisOk returns a tuple with the PtpCapForGpsis field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TimeSyncCapability) GetPtpCapForGpsisOk() (*map[string]PtpCapabilitiesPerUe, bool) {
	if o == nil || IsNil(o.PtpCapForGpsis) {
		return nil, false
	}
	return o.PtpCapForGpsis, true
}

// HasPtpCapForGpsis returns a boolean if a field has been set.
func (o *TimeSyncCapability) HasPtpCapForGpsis() bool {
	if o != nil && !IsNil(o.PtpCapForGpsis) {
		return true
	}

	return false
}

// SetPtpCapForGpsis gets a reference to the given map[string]PtpCapabilitiesPerUe and assigns it to the PtpCapForGpsis field.
func (o *TimeSyncCapability) SetPtpCapForGpsis(v map[string]PtpCapabilitiesPerUe) {
	o.PtpCapForGpsis = &v
}

func (o TimeSyncCapability) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o TimeSyncCapability) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["upNodeId"] = o.UpNodeId
	if !IsNil(o.GmCapables) {
		toSerialize["gmCapables"] = o.GmCapables
	}
	if !IsNil(o.AsTimeRes) {
		toSerialize["asTimeRes"] = o.AsTimeRes
	}
	if !IsNil(o.PtpCapForUes) {
		toSerialize["ptpCapForUes"] = o.PtpCapForUes
	}
	if !IsNil(o.PtpCapForGpsis) {
		toSerialize["ptpCapForGpsis"] = o.PtpCapForGpsis
	}
	return toSerialize, nil
}

func (o *TimeSyncCapability) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"upNodeId",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varTimeSyncCapability := _TimeSyncCapability{}

	err = json.Unmarshal(bytes, &varTimeSyncCapability)

	if err != nil {
		return err
	}

	*o = TimeSyncCapability(varTimeSyncCapability)

	return err
}

type NullableTimeSyncCapability struct {
	value *TimeSyncCapability
	isSet bool
}

func (v NullableTimeSyncCapability) Get() *TimeSyncCapability {
	return v.value
}

func (v *NullableTimeSyncCapability) Set(val *TimeSyncCapability) {
	v.value = val
	v.isSet = true
}

func (v NullableTimeSyncCapability) IsSet() bool {
	return v.isSet
}

func (v *NullableTimeSyncCapability) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTimeSyncCapability(val *TimeSyncCapability) *NullableTimeSyncCapability {
	return &NullableTimeSyncCapability{value: val, isSet: true}
}

func (v NullableTimeSyncCapability) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTimeSyncCapability) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
