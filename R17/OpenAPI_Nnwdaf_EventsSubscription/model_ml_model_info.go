/*
Nnwdaf_EventsSubscription

Nnwdaf_EventsSubscription Service API.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nnwdaf_EventsSubscription

import (
	"encoding/json"
)

// checks if the MLModelInfo type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MLModelInfo{}

// MLModelInfo Contains information about an ML models.
type MLModelInfo struct {
	MlFileAddrs []MLModelAddr `json:"mlFileAddrs,omitempty"`
	// String uniquely identifying a NF instance. The format of the NF Instance ID shall be a  Universally Unique Identifier (UUID) version 4, as described in IETF RFC 4122.
	ModelProvId *string `json:"modelProvId,omitempty"`
	// NF Set Identifier (see clause 28.12 of 3GPP TS 23.003), formatted as the following string \"set<Set ID>.<nftype>set.5gc.mnc<MNC>.mcc<MCC>\", or  \"set<SetID>.<NFType>set.5gc.nid<NID>.mnc<MNC>.mcc<MCC>\" with  <MCC> encoded as defined in clause 5.4.2 (\"Mcc\" data type definition)  <MNC> encoding the Mobile Network Code part of the PLMN, comprising 3 digits.    If there are only 2 significant digits in the MNC, one \"0\" digit shall be inserted    at the left side to fill the 3 digits coding of MNC.  Pattern: '^[0-9]{3}$' <NFType> encoded as a value defined in Table 6.1.6.3.3-1 of 3GPP TS 29.510 but    with lower case characters <Set ID> encoded as a string of characters consisting of    alphabetic characters (A-Z and a-z), digits (0-9) and/or the hyphen (-) and that    shall end with either an alphabetic character or a digit.
	ModelProvSetId *string `json:"modelProvSetId,omitempty"`
}

// NewMLModelInfo instantiates a new MLModelInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMLModelInfo() *MLModelInfo {
	this := MLModelInfo{}
	return &this
}

// NewMLModelInfoWithDefaults instantiates a new MLModelInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMLModelInfoWithDefaults() *MLModelInfo {
	this := MLModelInfo{}
	return &this
}

// GetMlFileAddrs returns the MlFileAddrs field value if set, zero value otherwise.
func (o *MLModelInfo) GetMlFileAddrs() []MLModelAddr {
	if o == nil || IsNil(o.MlFileAddrs) {
		var ret []MLModelAddr
		return ret
	}
	return o.MlFileAddrs
}

// GetMlFileAddrsOk returns a tuple with the MlFileAddrs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MLModelInfo) GetMlFileAddrsOk() ([]MLModelAddr, bool) {
	if o == nil || IsNil(o.MlFileAddrs) {
		return nil, false
	}
	return o.MlFileAddrs, true
}

// HasMlFileAddrs returns a boolean if a field has been set.
func (o *MLModelInfo) HasMlFileAddrs() bool {
	if o != nil && !IsNil(o.MlFileAddrs) {
		return true
	}

	return false
}

// SetMlFileAddrs gets a reference to the given []MLModelAddr and assigns it to the MlFileAddrs field.
func (o *MLModelInfo) SetMlFileAddrs(v []MLModelAddr) {
	o.MlFileAddrs = v
}

// GetModelProvId returns the ModelProvId field value if set, zero value otherwise.
func (o *MLModelInfo) GetModelProvId() string {
	if o == nil || IsNil(o.ModelProvId) {
		var ret string
		return ret
	}
	return *o.ModelProvId
}

// GetModelProvIdOk returns a tuple with the ModelProvId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MLModelInfo) GetModelProvIdOk() (*string, bool) {
	if o == nil || IsNil(o.ModelProvId) {
		return nil, false
	}
	return o.ModelProvId, true
}

// HasModelProvId returns a boolean if a field has been set.
func (o *MLModelInfo) HasModelProvId() bool {
	if o != nil && !IsNil(o.ModelProvId) {
		return true
	}

	return false
}

// SetModelProvId gets a reference to the given string and assigns it to the ModelProvId field.
func (o *MLModelInfo) SetModelProvId(v string) {
	o.ModelProvId = &v
}

// GetModelProvSetId returns the ModelProvSetId field value if set, zero value otherwise.
func (o *MLModelInfo) GetModelProvSetId() string {
	if o == nil || IsNil(o.ModelProvSetId) {
		var ret string
		return ret
	}
	return *o.ModelProvSetId
}

// GetModelProvSetIdOk returns a tuple with the ModelProvSetId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MLModelInfo) GetModelProvSetIdOk() (*string, bool) {
	if o == nil || IsNil(o.ModelProvSetId) {
		return nil, false
	}
	return o.ModelProvSetId, true
}

// HasModelProvSetId returns a boolean if a field has been set.
func (o *MLModelInfo) HasModelProvSetId() bool {
	if o != nil && !IsNil(o.ModelProvSetId) {
		return true
	}

	return false
}

// SetModelProvSetId gets a reference to the given string and assigns it to the ModelProvSetId field.
func (o *MLModelInfo) SetModelProvSetId(v string) {
	o.ModelProvSetId = &v
}

func (o MLModelInfo) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MLModelInfo) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.MlFileAddrs) {
		toSerialize["mlFileAddrs"] = o.MlFileAddrs
	}
	if !IsNil(o.ModelProvId) {
		toSerialize["modelProvId"] = o.ModelProvId
	}
	if !IsNil(o.ModelProvSetId) {
		toSerialize["modelProvSetId"] = o.ModelProvSetId
	}
	return toSerialize, nil
}

type NullableMLModelInfo struct {
	value *MLModelInfo
	isSet bool
}

func (v NullableMLModelInfo) Get() *MLModelInfo {
	return v.value
}

func (v *NullableMLModelInfo) Set(val *MLModelInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableMLModelInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableMLModelInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMLModelInfo(val *MLModelInfo) *NullableMLModelInfo {
	return &NullableMLModelInfo{value: val, isSet: true}
}

func (v NullableMLModelInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMLModelInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
