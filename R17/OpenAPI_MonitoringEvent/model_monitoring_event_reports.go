/*
3gpp-monitoring-event

API for Monitoring Event.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_MonitoringEvent

import (
	"encoding/json"
	"fmt"
)

// checks if the MonitoringEventReports type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MonitoringEventReports{}

// MonitoringEventReports Represents a set of event monitoring reports.
type MonitoringEventReports struct {
	MonitoringEventReports []MonitoringEventReport `json:"monitoringEventReports"`
}

type _MonitoringEventReports MonitoringEventReports

// NewMonitoringEventReports instantiates a new MonitoringEventReports object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMonitoringEventReports(monitoringEventReports []MonitoringEventReport) *MonitoringEventReports {
	this := MonitoringEventReports{}
	this.MonitoringEventReports = monitoringEventReports
	return &this
}

// NewMonitoringEventReportsWithDefaults instantiates a new MonitoringEventReports object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMonitoringEventReportsWithDefaults() *MonitoringEventReports {
	this := MonitoringEventReports{}
	return &this
}

// GetMonitoringEventReports returns the MonitoringEventReports field value
func (o *MonitoringEventReports) GetMonitoringEventReports() []MonitoringEventReport {
	if o == nil {
		var ret []MonitoringEventReport
		return ret
	}

	return o.MonitoringEventReports
}

// GetMonitoringEventReportsOk returns a tuple with the MonitoringEventReports field value
// and a boolean to check if the value has been set.
func (o *MonitoringEventReports) GetMonitoringEventReportsOk() ([]MonitoringEventReport, bool) {
	if o == nil {
		return nil, false
	}
	return o.MonitoringEventReports, true
}

// SetMonitoringEventReports sets field value
func (o *MonitoringEventReports) SetMonitoringEventReports(v []MonitoringEventReport) {
	o.MonitoringEventReports = v
}

func (o MonitoringEventReports) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MonitoringEventReports) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["monitoringEventReports"] = o.MonitoringEventReports
	return toSerialize, nil
}

func (o *MonitoringEventReports) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"monitoringEventReports",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varMonitoringEventReports := _MonitoringEventReports{}

	err = json.Unmarshal(bytes, &varMonitoringEventReports)

	if err != nil {
		return err
	}

	*o = MonitoringEventReports(varMonitoringEventReports)

	return err
}

type NullableMonitoringEventReports struct {
	value *MonitoringEventReports
	isSet bool
}

func (v NullableMonitoringEventReports) Get() *MonitoringEventReports {
	return v.value
}

func (v *NullableMonitoringEventReports) Set(val *MonitoringEventReports) {
	v.value = val
	v.isSet = true
}

func (v NullableMonitoringEventReports) IsSet() bool {
	return v.isSet
}

func (v *NullableMonitoringEventReports) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMonitoringEventReports(val *MonitoringEventReports) *NullableMonitoringEventReports {
	return &NullableMonitoringEventReports{value: val, isSet: true}
}

func (v NullableMonitoringEventReports) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMonitoringEventReports) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
