/*
Nmfaf_3daDataManagement

MFAF 3GPP DCCF Adaptor (3DA) Data Management Service.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nmfaf_3daDataManagement

import (
	"encoding/json"
	"fmt"
)

// checks if the MessageConfiguration type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MessageConfiguration{}

// MessageConfiguration Represents the message configuration.
type MessageConfiguration struct {
	// If the configuration is used for mapping analytics or data collection, representing the Analytics Consumer Notification Correlation ID or Data Consumer Notification Correlation ID.
	CorreId        string                 `json:"correId"`
	FormatInstruct *FormattingInstruction `json:"formatInstruct,omitempty"`
	MfafNotiInfo   *MfafNotiInfo          `json:"mfafNotiInfo,omitempty"`
	// String providing an URI formatted according to RFC 3986.
	NotificationURI string                 `json:"notificationURI"`
	ProcInstruct    *ProcessingInstruction `json:"procInstruct,omitempty"`
	// String uniquely identifying a NF instance. The format of the NF Instance ID shall be a  Universally Unique Identifier (UUID) version 4, as described in IETF RFC 4122.
	AdrfId *string `json:"adrfId,omitempty"`
}

type _MessageConfiguration MessageConfiguration

// NewMessageConfiguration instantiates a new MessageConfiguration object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMessageConfiguration(correId string, notificationURI string) *MessageConfiguration {
	this := MessageConfiguration{}
	this.CorreId = correId
	this.NotificationURI = notificationURI
	return &this
}

// NewMessageConfigurationWithDefaults instantiates a new MessageConfiguration object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMessageConfigurationWithDefaults() *MessageConfiguration {
	this := MessageConfiguration{}
	return &this
}

// GetCorreId returns the CorreId field value
func (o *MessageConfiguration) GetCorreId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.CorreId
}

// GetCorreIdOk returns a tuple with the CorreId field value
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetCorreIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CorreId, true
}

// SetCorreId sets field value
func (o *MessageConfiguration) SetCorreId(v string) {
	o.CorreId = v
}

// GetFormatInstruct returns the FormatInstruct field value if set, zero value otherwise.
func (o *MessageConfiguration) GetFormatInstruct() FormattingInstruction {
	if o == nil || IsNil(o.FormatInstruct) {
		var ret FormattingInstruction
		return ret
	}
	return *o.FormatInstruct
}

// GetFormatInstructOk returns a tuple with the FormatInstruct field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetFormatInstructOk() (*FormattingInstruction, bool) {
	if o == nil || IsNil(o.FormatInstruct) {
		return nil, false
	}
	return o.FormatInstruct, true
}

// HasFormatInstruct returns a boolean if a field has been set.
func (o *MessageConfiguration) HasFormatInstruct() bool {
	if o != nil && !IsNil(o.FormatInstruct) {
		return true
	}

	return false
}

// SetFormatInstruct gets a reference to the given FormattingInstruction and assigns it to the FormatInstruct field.
func (o *MessageConfiguration) SetFormatInstruct(v FormattingInstruction) {
	o.FormatInstruct = &v
}

// GetMfafNotiInfo returns the MfafNotiInfo field value if set, zero value otherwise.
func (o *MessageConfiguration) GetMfafNotiInfo() MfafNotiInfo {
	if o == nil || IsNil(o.MfafNotiInfo) {
		var ret MfafNotiInfo
		return ret
	}
	return *o.MfafNotiInfo
}

// GetMfafNotiInfoOk returns a tuple with the MfafNotiInfo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetMfafNotiInfoOk() (*MfafNotiInfo, bool) {
	if o == nil || IsNil(o.MfafNotiInfo) {
		return nil, false
	}
	return o.MfafNotiInfo, true
}

// HasMfafNotiInfo returns a boolean if a field has been set.
func (o *MessageConfiguration) HasMfafNotiInfo() bool {
	if o != nil && !IsNil(o.MfafNotiInfo) {
		return true
	}

	return false
}

// SetMfafNotiInfo gets a reference to the given MfafNotiInfo and assigns it to the MfafNotiInfo field.
func (o *MessageConfiguration) SetMfafNotiInfo(v MfafNotiInfo) {
	o.MfafNotiInfo = &v
}

// GetNotificationURI returns the NotificationURI field value
func (o *MessageConfiguration) GetNotificationURI() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NotificationURI
}

// GetNotificationURIOk returns a tuple with the NotificationURI field value
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetNotificationURIOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NotificationURI, true
}

// SetNotificationURI sets field value
func (o *MessageConfiguration) SetNotificationURI(v string) {
	o.NotificationURI = v
}

// GetProcInstruct returns the ProcInstruct field value if set, zero value otherwise.
func (o *MessageConfiguration) GetProcInstruct() ProcessingInstruction {
	if o == nil || IsNil(o.ProcInstruct) {
		var ret ProcessingInstruction
		return ret
	}
	return *o.ProcInstruct
}

// GetProcInstructOk returns a tuple with the ProcInstruct field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetProcInstructOk() (*ProcessingInstruction, bool) {
	if o == nil || IsNil(o.ProcInstruct) {
		return nil, false
	}
	return o.ProcInstruct, true
}

// HasProcInstruct returns a boolean if a field has been set.
func (o *MessageConfiguration) HasProcInstruct() bool {
	if o != nil && !IsNil(o.ProcInstruct) {
		return true
	}

	return false
}

// SetProcInstruct gets a reference to the given ProcessingInstruction and assigns it to the ProcInstruct field.
func (o *MessageConfiguration) SetProcInstruct(v ProcessingInstruction) {
	o.ProcInstruct = &v
}

// GetAdrfId returns the AdrfId field value if set, zero value otherwise.
func (o *MessageConfiguration) GetAdrfId() string {
	if o == nil || IsNil(o.AdrfId) {
		var ret string
		return ret
	}
	return *o.AdrfId
}

// GetAdrfIdOk returns a tuple with the AdrfId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageConfiguration) GetAdrfIdOk() (*string, bool) {
	if o == nil || IsNil(o.AdrfId) {
		return nil, false
	}
	return o.AdrfId, true
}

// HasAdrfId returns a boolean if a field has been set.
func (o *MessageConfiguration) HasAdrfId() bool {
	if o != nil && !IsNil(o.AdrfId) {
		return true
	}

	return false
}

// SetAdrfId gets a reference to the given string and assigns it to the AdrfId field.
func (o *MessageConfiguration) SetAdrfId(v string) {
	o.AdrfId = &v
}

func (o MessageConfiguration) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MessageConfiguration) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["correId"] = o.CorreId
	if !IsNil(o.FormatInstruct) {
		toSerialize["formatInstruct"] = o.FormatInstruct
	}
	if !IsNil(o.MfafNotiInfo) {
		toSerialize["mfafNotiInfo"] = o.MfafNotiInfo
	}
	toSerialize["notificationURI"] = o.NotificationURI
	if !IsNil(o.ProcInstruct) {
		toSerialize["procInstruct"] = o.ProcInstruct
	}
	if !IsNil(o.AdrfId) {
		toSerialize["adrfId"] = o.AdrfId
	}
	return toSerialize, nil
}

func (o *MessageConfiguration) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"correId",
		"notificationURI",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varMessageConfiguration := _MessageConfiguration{}

	err = json.Unmarshal(bytes, &varMessageConfiguration)

	if err != nil {
		return err
	}

	*o = MessageConfiguration(varMessageConfiguration)

	return err
}

type NullableMessageConfiguration struct {
	value *MessageConfiguration
	isSet bool
}

func (v NullableMessageConfiguration) Get() *MessageConfiguration {
	return v.value
}

func (v *NullableMessageConfiguration) Set(val *MessageConfiguration) {
	v.value = val
	v.isSet = true
}

func (v NullableMessageConfiguration) IsSet() bool {
	return v.isSet
}

func (v *NullableMessageConfiguration) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessageConfiguration(val *MessageConfiguration) *NullableMessageConfiguration {
	return &NullableMessageConfiguration{value: val, isSet: true}
}

func (v NullableMessageConfiguration) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessageConfiguration) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
