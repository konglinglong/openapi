/*
Fault Supervision MnS

OAS 3.0.1 definition of the Fault Supervision MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.3.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_FaultMnS

import (
	"encoding/json"
	"fmt"
	"time"
)

// checks if the NotifyChangedAlarm type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &NotifyChangedAlarm{}

// NotifyChangedAlarm struct for NotifyChangedAlarm
type NotifyChangedAlarm struct {
	NotificationHeader
	AlarmId           string            `json:"alarmId"`
	AlarmType         AlarmType         `json:"alarmType"`
	ProbableCause     NullableInt32     `json:"probableCause"`
	PerceivedSeverity PerceivedSeverity `json:"perceivedSeverity"`
}

type _NotifyChangedAlarm NotifyChangedAlarm

// NewNotifyChangedAlarm instantiates a new NotifyChangedAlarm object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNotifyChangedAlarm(alarmId string, alarmType AlarmType, probableCause NullableInt32, perceivedSeverity PerceivedSeverity, href string, notificationId int32, notificationType NotificationType, eventTime time.Time, systemDN string) *NotifyChangedAlarm {
	this := NotifyChangedAlarm{}
	this.Href = href
	this.NotificationId = notificationId
	this.NotificationType = notificationType
	this.EventTime = eventTime
	this.SystemDN = systemDN
	this.AlarmId = alarmId
	this.AlarmType = alarmType
	this.ProbableCause = probableCause
	this.PerceivedSeverity = perceivedSeverity
	return &this
}

// NewNotifyChangedAlarmWithDefaults instantiates a new NotifyChangedAlarm object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNotifyChangedAlarmWithDefaults() *NotifyChangedAlarm {
	this := NotifyChangedAlarm{}
	return &this
}

// GetAlarmId returns the AlarmId field value
func (o *NotifyChangedAlarm) GetAlarmId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.AlarmId
}

// GetAlarmIdOk returns a tuple with the AlarmId field value
// and a boolean to check if the value has been set.
func (o *NotifyChangedAlarm) GetAlarmIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.AlarmId, true
}

// SetAlarmId sets field value
func (o *NotifyChangedAlarm) SetAlarmId(v string) {
	o.AlarmId = v
}

// GetAlarmType returns the AlarmType field value
func (o *NotifyChangedAlarm) GetAlarmType() AlarmType {
	if o == nil {
		var ret AlarmType
		return ret
	}

	return o.AlarmType
}

// GetAlarmTypeOk returns a tuple with the AlarmType field value
// and a boolean to check if the value has been set.
func (o *NotifyChangedAlarm) GetAlarmTypeOk() (*AlarmType, bool) {
	if o == nil {
		return nil, false
	}
	return &o.AlarmType, true
}

// SetAlarmType sets field value
func (o *NotifyChangedAlarm) SetAlarmType(v AlarmType) {
	o.AlarmType = v
}

// GetProbableCause returns the ProbableCause field value
// If the value is explicit nil, the zero value for int32 will be returned
func (o *NotifyChangedAlarm) GetProbableCause() int32 {
	if o == nil || o.ProbableCause.Get() == nil {
		var ret int32
		return ret
	}

	return *o.ProbableCause.Get()
}

// GetProbableCauseOk returns a tuple with the ProbableCause field value
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *NotifyChangedAlarm) GetProbableCauseOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.ProbableCause.Get(), o.ProbableCause.IsSet()
}

// SetProbableCause sets field value
func (o *NotifyChangedAlarm) SetProbableCause(v int32) {
	o.ProbableCause.Set(&v)
}

// GetPerceivedSeverity returns the PerceivedSeverity field value
func (o *NotifyChangedAlarm) GetPerceivedSeverity() PerceivedSeverity {
	if o == nil {
		var ret PerceivedSeverity
		return ret
	}

	return o.PerceivedSeverity
}

// GetPerceivedSeverityOk returns a tuple with the PerceivedSeverity field value
// and a boolean to check if the value has been set.
func (o *NotifyChangedAlarm) GetPerceivedSeverityOk() (*PerceivedSeverity, bool) {
	if o == nil {
		return nil, false
	}
	return &o.PerceivedSeverity, true
}

// SetPerceivedSeverity sets field value
func (o *NotifyChangedAlarm) SetPerceivedSeverity(v PerceivedSeverity) {
	o.PerceivedSeverity = v
}

func (o NotifyChangedAlarm) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o NotifyChangedAlarm) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedNotificationHeader, errNotificationHeader := json.Marshal(o.NotificationHeader)
	if errNotificationHeader != nil {
		return map[string]interface{}{}, errNotificationHeader
	}
	errNotificationHeader = json.Unmarshal([]byte(serializedNotificationHeader), &toSerialize)
	if errNotificationHeader != nil {
		return map[string]interface{}{}, errNotificationHeader
	}
	toSerialize["alarmId"] = o.AlarmId
	toSerialize["alarmType"] = o.AlarmType
	toSerialize["probableCause"] = o.ProbableCause.Get()
	toSerialize["perceivedSeverity"] = o.PerceivedSeverity
	return toSerialize, nil
}

func (o *NotifyChangedAlarm) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"alarmId",
		"alarmType",
		"probableCause",
		"perceivedSeverity",
		"href",
		"notificationId",
		"notificationType",
		"eventTime",
		"systemDN",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varNotifyChangedAlarm := _NotifyChangedAlarm{}

	err = json.Unmarshal(bytes, &varNotifyChangedAlarm)

	if err != nil {
		return err
	}

	*o = NotifyChangedAlarm(varNotifyChangedAlarm)

	return err
}

type NullableNotifyChangedAlarm struct {
	value *NotifyChangedAlarm
	isSet bool
}

func (v NullableNotifyChangedAlarm) Get() *NotifyChangedAlarm {
	return v.value
}

func (v *NullableNotifyChangedAlarm) Set(val *NotifyChangedAlarm) {
	v.value = val
	v.isSet = true
}

func (v NullableNotifyChangedAlarm) IsSet() bool {
	return v.isSet
}

func (v *NullableNotifyChangedAlarm) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNotifyChangedAlarm(val *NotifyChangedAlarm) *NullableNotifyChangedAlarm {
	return &NullableNotifyChangedAlarm{value: val, isSet: true}
}

func (v NullableNotifyChangedAlarm) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNotifyChangedAlarm) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
