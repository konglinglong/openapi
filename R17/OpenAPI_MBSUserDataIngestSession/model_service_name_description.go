/*
3gpp-mbs-ud-ingest

API for MBS User Data Ingest Session.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_MBSUserDataIngestSession

import (
	"encoding/json"
	"fmt"
)

// checks if the ServiceNameDescription type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ServiceNameDescription{}

// ServiceNameDescription Represents a set of per language service name and/or service description.
type ServiceNameDescription struct {
	ServName    *string `json:"servName,omitempty"`
	ServDescrip *string `json:"servDescrip,omitempty"`
	Language    string  `json:"language"`
}

type _ServiceNameDescription ServiceNameDescription

// NewServiceNameDescription instantiates a new ServiceNameDescription object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewServiceNameDescription(language string) *ServiceNameDescription {
	this := ServiceNameDescription{}
	return &this
}

// NewServiceNameDescriptionWithDefaults instantiates a new ServiceNameDescription object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewServiceNameDescriptionWithDefaults() *ServiceNameDescription {
	this := ServiceNameDescription{}
	return &this
}

// GetServName returns the ServName field value if set, zero value otherwise.
func (o *ServiceNameDescription) GetServName() string {
	if o == nil || IsNil(o.ServName) {
		var ret string
		return ret
	}
	return *o.ServName
}

// GetServNameOk returns a tuple with the ServName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServiceNameDescription) GetServNameOk() (*string, bool) {
	if o == nil || IsNil(o.ServName) {
		return nil, false
	}
	return o.ServName, true
}

// HasServName returns a boolean if a field has been set.
func (o *ServiceNameDescription) HasServName() bool {
	if o != nil && !IsNil(o.ServName) {
		return true
	}

	return false
}

// SetServName gets a reference to the given string and assigns it to the ServName field.
func (o *ServiceNameDescription) SetServName(v string) {
	o.ServName = &v
}

// GetServDescrip returns the ServDescrip field value if set, zero value otherwise.
func (o *ServiceNameDescription) GetServDescrip() string {
	if o == nil || IsNil(o.ServDescrip) {
		var ret string
		return ret
	}
	return *o.ServDescrip
}

// GetServDescripOk returns a tuple with the ServDescrip field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ServiceNameDescription) GetServDescripOk() (*string, bool) {
	if o == nil || IsNil(o.ServDescrip) {
		return nil, false
	}
	return o.ServDescrip, true
}

// HasServDescrip returns a boolean if a field has been set.
func (o *ServiceNameDescription) HasServDescrip() bool {
	if o != nil && !IsNil(o.ServDescrip) {
		return true
	}

	return false
}

// SetServDescrip gets a reference to the given string and assigns it to the ServDescrip field.
func (o *ServiceNameDescription) SetServDescrip(v string) {
	o.ServDescrip = &v
}

// GetLanguage returns the Language field value
func (o *ServiceNameDescription) GetLanguage() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Language
}

// GetLanguageOk returns a tuple with the Language field value
// and a boolean to check if the value has been set.
func (o *ServiceNameDescription) GetLanguageOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Language, true
}

// SetLanguage sets field value
func (o *ServiceNameDescription) SetLanguage(v string) {
	o.Language = v
}

func (o ServiceNameDescription) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ServiceNameDescription) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.ServName) {
		toSerialize["servName"] = o.ServName
	}
	if !IsNil(o.ServDescrip) {
		toSerialize["servDescrip"] = o.ServDescrip
	}
	toSerialize["language"] = o.Language
	return toSerialize, nil
}

func (o *ServiceNameDescription) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"language",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varServiceNameDescription := _ServiceNameDescription{}

	err = json.Unmarshal(bytes, &varServiceNameDescription)

	if err != nil {
		return err
	}

	*o = ServiceNameDescription(varServiceNameDescription)

	return err
}

type NullableServiceNameDescription struct {
	value *ServiceNameDescription
	isSet bool
}

func (v NullableServiceNameDescription) Get() *ServiceNameDescription {
	return v.value
}

func (v *NullableServiceNameDescription) Set(val *ServiceNameDescription) {
	v.value = val
	v.isSet = true
}

func (v NullableServiceNameDescription) IsSet() bool {
	return v.isSet
}

func (v *NullableServiceNameDescription) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableServiceNameDescription(val *ServiceNameDescription) *NullableServiceNameDescription {
	return &NullableServiceNameDescription{value: val, isSet: true}
}

func (v NullableServiceNameDescription) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableServiceNameDescription) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
