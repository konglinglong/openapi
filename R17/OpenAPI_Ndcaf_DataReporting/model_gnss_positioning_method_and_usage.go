/*
Ndcaf_DataReporting

Data Collection AF: Data Collection and Reporting Configuration API and Data Reporting API © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 1.2.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Ndcaf_DataReporting

import (
	"encoding/json"
	"fmt"
)

// checks if the GnssPositioningMethodAndUsage type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GnssPositioningMethodAndUsage{}

// GnssPositioningMethodAndUsage Indicates the usage of a Global Navigation Satellite System (GNSS) positioning method.
type GnssPositioningMethodAndUsage struct {
	Mode  PositioningMode `json:"mode"`
	Gnss  GnssId          `json:"gnss"`
	Usage Usage           `json:"usage"`
}

type _GnssPositioningMethodAndUsage GnssPositioningMethodAndUsage

// NewGnssPositioningMethodAndUsage instantiates a new GnssPositioningMethodAndUsage object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGnssPositioningMethodAndUsage(mode PositioningMode, gnss GnssId, usage Usage) *GnssPositioningMethodAndUsage {
	this := GnssPositioningMethodAndUsage{}
	this.Mode = mode
	this.Gnss = gnss
	this.Usage = usage
	return &this
}

// NewGnssPositioningMethodAndUsageWithDefaults instantiates a new GnssPositioningMethodAndUsage object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGnssPositioningMethodAndUsageWithDefaults() *GnssPositioningMethodAndUsage {
	this := GnssPositioningMethodAndUsage{}
	return &this
}

// GetMode returns the Mode field value
func (o *GnssPositioningMethodAndUsage) GetMode() PositioningMode {
	if o == nil {
		var ret PositioningMode
		return ret
	}

	return o.Mode
}

// GetModeOk returns a tuple with the Mode field value
// and a boolean to check if the value has been set.
func (o *GnssPositioningMethodAndUsage) GetModeOk() (*PositioningMode, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Mode, true
}

// SetMode sets field value
func (o *GnssPositioningMethodAndUsage) SetMode(v PositioningMode) {
	o.Mode = v
}

// GetGnss returns the Gnss field value
func (o *GnssPositioningMethodAndUsage) GetGnss() GnssId {
	if o == nil {
		var ret GnssId
		return ret
	}

	return o.Gnss
}

// GetGnssOk returns a tuple with the Gnss field value
// and a boolean to check if the value has been set.
func (o *GnssPositioningMethodAndUsage) GetGnssOk() (*GnssId, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Gnss, true
}

// SetGnss sets field value
func (o *GnssPositioningMethodAndUsage) SetGnss(v GnssId) {
	o.Gnss = v
}

// GetUsage returns the Usage field value
func (o *GnssPositioningMethodAndUsage) GetUsage() Usage {
	if o == nil {
		var ret Usage
		return ret
	}

	return o.Usage
}

// GetUsageOk returns a tuple with the Usage field value
// and a boolean to check if the value has been set.
func (o *GnssPositioningMethodAndUsage) GetUsageOk() (*Usage, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Usage, true
}

// SetUsage sets field value
func (o *GnssPositioningMethodAndUsage) SetUsage(v Usage) {
	o.Usage = v
}

func (o GnssPositioningMethodAndUsage) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GnssPositioningMethodAndUsage) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["mode"] = o.Mode
	toSerialize["gnss"] = o.Gnss
	toSerialize["usage"] = o.Usage
	return toSerialize, nil
}

func (o *GnssPositioningMethodAndUsage) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"mode",
		"gnss",
		"usage",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varGnssPositioningMethodAndUsage := _GnssPositioningMethodAndUsage{}

	err = json.Unmarshal(bytes, &varGnssPositioningMethodAndUsage)

	if err != nil {
		return err
	}

	*o = GnssPositioningMethodAndUsage(varGnssPositioningMethodAndUsage)

	return err
}

type NullableGnssPositioningMethodAndUsage struct {
	value *GnssPositioningMethodAndUsage
	isSet bool
}

func (v NullableGnssPositioningMethodAndUsage) Get() *GnssPositioningMethodAndUsage {
	return v.value
}

func (v *NullableGnssPositioningMethodAndUsage) Set(val *GnssPositioningMethodAndUsage) {
	v.value = val
	v.isSet = true
}

func (v NullableGnssPositioningMethodAndUsage) IsSet() bool {
	return v.isSet
}

func (v *NullableGnssPositioningMethodAndUsage) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGnssPositioningMethodAndUsage(val *GnssPositioningMethodAndUsage) *NullableGnssPositioningMethodAndUsage {
	return &NullableGnssPositioningMethodAndUsage{value: val, isSet: true}
}

func (v NullableGnssPositioningMethodAndUsage) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGnssPositioningMethodAndUsage) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
