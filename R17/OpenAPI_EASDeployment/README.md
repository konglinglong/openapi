# Go API client for OpenAPI_EASDeployment

API for AF provisioned EAS Deployment.  
© 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0.2
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_EASDeployment "gitee.com/konglinglong/openapi/OpenAPI_EASDeployment"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_EASDeployment.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_EASDeployment.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_EASDeployment.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_EASDeployment.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_EASDeployment.ContextOperationServerIndices` and `OpenAPI_EASDeployment.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_EASDeployment.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_EASDeployment.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/3gpp-eas-deployment/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*EASDeploymentInformationCollectionAPI* | [**CreateAnDeployment**](docs/EASDeploymentInformationCollectionAPI.md#createandeployment) | **Post** /{afId}/eas-deployment-info | Create a new Individual EAS Deployment information resource.
*EASDeploymentInformationCollectionAPI* | [**ReadAllDeployment**](docs/EASDeploymentInformationCollectionAPI.md#readalldeployment) | **Get** /{afId}/eas-deployment-info | Read all EAS Deployment information for a given AF
*EASDeploymentInformationRemovalAPI* | [**DeleteEDIs**](docs/EASDeploymentInformationRemovalAPI.md#deleteedis) | **Post** /remove-edis | Remove EAS Deployment Information based on given criteria.
*IndividualEASDeploymentInformationAPI* | [**DeleteAnDeployment**](docs/IndividualEASDeploymentInformationAPI.md#deleteandeployment) | **Delete** /{afId}/eas-deployment-info/{easDeployInfoId} | Deletes an already existing EAS Deployment information resource
*IndividualEASDeploymentInformationAPI* | [**FullyUpdateAnDeployment**](docs/IndividualEASDeploymentInformationAPI.md#fullyupdateandeployment) | **Put** /{afId}/eas-deployment-info/{easDeployInfoId} | Fully updates/replaces an existing resource
*IndividualEASDeploymentInformationAPI* | [**ReadAnDeployment**](docs/IndividualEASDeploymentInformationAPI.md#readandeployment) | **Get** /{afId}/eas-deployment-info/{easDeployInfoId} | Read an active Individual EAS Deployment Information resource for the AF


## Documentation For Models

 - [AccessTokenErr](docs/AccessTokenErr.md)
 - [AccessTokenReq](docs/AccessTokenReq.md)
 - [DnaiInformation](docs/DnaiInformation.md)
 - [DnnSnssaiInformation](docs/DnnSnssaiInformation.md)
 - [DnsServerIdentifier](docs/DnsServerIdentifier.md)
 - [EasDeployInfo](docs/EasDeployInfo.md)
 - [EdiDeleteCriteria](docs/EdiDeleteCriteria.md)
 - [FqdnPatternMatchingRule](docs/FqdnPatternMatchingRule.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [InvalidParam1](docs/InvalidParam1.md)
 - [IpAddr](docs/IpAddr.md)
 - [Ipv6Addr](docs/Ipv6Addr.md)
 - [Ipv6Prefix](docs/Ipv6Prefix.md)
 - [MatchingOperator](docs/MatchingOperator.md)
 - [NFType](docs/NFType.md)
 - [PlmnId](docs/PlmnId.md)
 - [PlmnIdNid](docs/PlmnIdNid.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [ProblemDetails1](docs/ProblemDetails1.md)
 - [Snssai](docs/Snssai.md)
 - [StringMatchingCondition](docs/StringMatchingCondition.md)
 - [StringMatchingRule](docs/StringMatchingRule.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: N/A

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_EASDeployment.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_EASDeployment.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



