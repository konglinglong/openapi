# Go API client for OpenAPI_Naanf_AKMA

API for Naanf_AKMA.  
© 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0.2
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_Naanf_AKMA "gitee.com/konglinglong/openapi/OpenAPI_Naanf_AKMA"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_Naanf_AKMA.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Naanf_AKMA.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_Naanf_AKMA.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Naanf_AKMA.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_Naanf_AKMA.ContextOperationServerIndices` and `OpenAPI_Naanf_AKMA.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Naanf_AKMA.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_Naanf_AKMA.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/naanf-akma/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*RegisterTheAKMARelatedKeyMaterialAPI* | [**RegisterAKMAKey**](docs/RegisterTheAKMARelatedKeyMaterialAPI.md#registerakmakey) | **Post** /register-anchorkey | Store AKMA related key material.
*RemoveTheAKMAApplicationKeyMaterialDeletionAPI* | [**RemoveContext**](docs/RemoveTheAKMAApplicationKeyMaterialDeletionAPI.md#removecontext) | **Post** /remove-context | Request to remove the AKMA related key material.
*RetrieveTheAKMAApplicationKeyMaterialCollectionAPI* | [**GetAKMAAPPKeyMaterial**](docs/RetrieveTheAKMAApplicationKeyMaterialCollectionAPI.md#getakmaappkeymaterial) | **Post** /retrieve-applicationkey | Request to retrieve AKMA Application Key information.


## Documentation For Models

 - [AccessTokenErr](docs/AccessTokenErr.md)
 - [AccessTokenReq](docs/AccessTokenReq.md)
 - [AkmaAfKeyData](docs/AkmaAfKeyData.md)
 - [AkmaAfKeyRequest](docs/AkmaAfKeyRequest.md)
 - [AkmaKeyInfo](docs/AkmaKeyInfo.md)
 - [CtxRemove](docs/CtxRemove.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [NFType](docs/NFType.md)
 - [PlmnId](docs/PlmnId.md)
 - [PlmnIdNid](docs/PlmnIdNid.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [RedirectResponse](docs/RedirectResponse.md)
 - [Snssai](docs/Snssai.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: 
 - **naanf_akma**: Access to the Naanf_AKMA API

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_Naanf_AKMA.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_Naanf_AKMA.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



