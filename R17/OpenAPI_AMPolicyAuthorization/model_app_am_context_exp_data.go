/*
3gpp-am-policyauthorization

API for AM policy authorization.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_AMPolicyAuthorization

import (
	"encoding/json"
	"fmt"
)

// checks if the AppAmContextExpData type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AppAmContextExpData{}

// AppAmContextExpData Represents an Individual application AM context exposure resource.
type AppAmContextExpData struct {
	// string formatted according to IETF RFC 3986 identifying a referenced resource.
	Self     *string            `json:"self,omitempty"`
	EvSubscs *AmEventsSubscData `json:"evSubscs,omitempty"`
	// String identifying a Gpsi shall contain either an External Id or an MSISDN.  It shall be formatted as follows -External Identifier= \"extid-'extid', where 'extid'  shall be formatted according to clause 19.7.2 of 3GPP TS 23.003 that describes an  External Identifier.
	Gpsi        string             `json:"gpsi"`
	HighThruInd *bool              `json:"highThruInd,omitempty"`
	CovReqs     []GeographicalArea `json:"covReqs,omitempty"`
	// Unsigned integer identifying a period of time in units of seconds with \"nullable=true\" property.
	PolicyDuration NullableInt32 `json:"policyDuration,omitempty"`
	// A string used to indicate the features supported by an API that is used as defined in clause  6.6 in 3GPP TS 29.500. The string shall contain a bitmask indicating supported features in  hexadecimal representation Each character in the string shall take a value of \"0\" to \"9\",  \"a\" to \"f\" or \"A\" to \"F\" and shall represent the support of 4 features as described in  table 5.2.2-3. The most significant character representing the highest-numbered features shall  appear first in the string, and the character representing features 1 to 4 shall appear last  in the string. The list of features and their numbering (starting with 1) are defined  separately for each API. If the string contains a lower number of characters than there are  defined features for an API, all features that would be represented by characters that are not  present in the string are not supported.
	SuppFeat *string `json:"suppFeat,omitempty"`
	// Set to true by the AF to request the NEF to send a test notification as defined in clause 5.2.5.3 of 3GPP TS 29.122. Set to false or omitted otherwise.
	RequestTestNotification *bool               `json:"requestTestNotification,omitempty"`
	WebsockNotifConfig      *WebsockNotifConfig `json:"websockNotifConfig,omitempty"`
}

type _AppAmContextExpData AppAmContextExpData

// NewAppAmContextExpData instantiates a new AppAmContextExpData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAppAmContextExpData(gpsi string) *AppAmContextExpData {
	this := AppAmContextExpData{}
	return &this
}

// NewAppAmContextExpDataWithDefaults instantiates a new AppAmContextExpData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAppAmContextExpDataWithDefaults() *AppAmContextExpData {
	this := AppAmContextExpData{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetSelf() string {
	if o == nil || IsNil(o.Self) {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetSelfOk() (*string, bool) {
	if o == nil || IsNil(o.Self) {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasSelf() bool {
	if o != nil && !IsNil(o.Self) {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *AppAmContextExpData) SetSelf(v string) {
	o.Self = &v
}

// GetEvSubscs returns the EvSubscs field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetEvSubscs() AmEventsSubscData {
	if o == nil || IsNil(o.EvSubscs) {
		var ret AmEventsSubscData
		return ret
	}
	return *o.EvSubscs
}

// GetEvSubscsOk returns a tuple with the EvSubscs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetEvSubscsOk() (*AmEventsSubscData, bool) {
	if o == nil || IsNil(o.EvSubscs) {
		return nil, false
	}
	return o.EvSubscs, true
}

// HasEvSubscs returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasEvSubscs() bool {
	if o != nil && !IsNil(o.EvSubscs) {
		return true
	}

	return false
}

// SetEvSubscs gets a reference to the given AmEventsSubscData and assigns it to the EvSubscs field.
func (o *AppAmContextExpData) SetEvSubscs(v AmEventsSubscData) {
	o.EvSubscs = &v
}

// GetGpsi returns the Gpsi field value
func (o *AppAmContextExpData) GetGpsi() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Gpsi
}

// GetGpsiOk returns a tuple with the Gpsi field value
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetGpsiOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Gpsi, true
}

// SetGpsi sets field value
func (o *AppAmContextExpData) SetGpsi(v string) {
	o.Gpsi = v
}

// GetHighThruInd returns the HighThruInd field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetHighThruInd() bool {
	if o == nil || IsNil(o.HighThruInd) {
		var ret bool
		return ret
	}
	return *o.HighThruInd
}

// GetHighThruIndOk returns a tuple with the HighThruInd field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetHighThruIndOk() (*bool, bool) {
	if o == nil || IsNil(o.HighThruInd) {
		return nil, false
	}
	return o.HighThruInd, true
}

// HasHighThruInd returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasHighThruInd() bool {
	if o != nil && !IsNil(o.HighThruInd) {
		return true
	}

	return false
}

// SetHighThruInd gets a reference to the given bool and assigns it to the HighThruInd field.
func (o *AppAmContextExpData) SetHighThruInd(v bool) {
	o.HighThruInd = &v
}

// GetCovReqs returns the CovReqs field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AppAmContextExpData) GetCovReqs() []GeographicalArea {
	if o == nil {
		var ret []GeographicalArea
		return ret
	}
	return o.CovReqs
}

// GetCovReqsOk returns a tuple with the CovReqs field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AppAmContextExpData) GetCovReqsOk() ([]GeographicalArea, bool) {
	if o == nil || IsNil(o.CovReqs) {
		return nil, false
	}
	return o.CovReqs, true
}

// HasCovReqs returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasCovReqs() bool {
	if o != nil && IsNil(o.CovReqs) {
		return true
	}

	return false
}

// SetCovReqs gets a reference to the given []GeographicalArea and assigns it to the CovReqs field.
func (o *AppAmContextExpData) SetCovReqs(v []GeographicalArea) {
	o.CovReqs = v
}

// GetPolicyDuration returns the PolicyDuration field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *AppAmContextExpData) GetPolicyDuration() int32 {
	if o == nil || IsNil(o.PolicyDuration.Get()) {
		var ret int32
		return ret
	}
	return *o.PolicyDuration.Get()
}

// GetPolicyDurationOk returns a tuple with the PolicyDuration field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *AppAmContextExpData) GetPolicyDurationOk() (*int32, bool) {
	if o == nil {
		return nil, false
	}
	return o.PolicyDuration.Get(), o.PolicyDuration.IsSet()
}

// HasPolicyDuration returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasPolicyDuration() bool {
	if o != nil && o.PolicyDuration.IsSet() {
		return true
	}

	return false
}

// SetPolicyDuration gets a reference to the given NullableInt32 and assigns it to the PolicyDuration field.
func (o *AppAmContextExpData) SetPolicyDuration(v int32) {
	o.PolicyDuration.Set(&v)
}

// SetPolicyDurationNil sets the value for PolicyDuration to be an explicit nil
func (o *AppAmContextExpData) SetPolicyDurationNil() {
	o.PolicyDuration.Set(nil)
}

// UnsetPolicyDuration ensures that no value is present for PolicyDuration, not even an explicit nil
func (o *AppAmContextExpData) UnsetPolicyDuration() {
	o.PolicyDuration.Unset()
}

// GetSuppFeat returns the SuppFeat field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetSuppFeat() string {
	if o == nil || IsNil(o.SuppFeat) {
		var ret string
		return ret
	}
	return *o.SuppFeat
}

// GetSuppFeatOk returns a tuple with the SuppFeat field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetSuppFeatOk() (*string, bool) {
	if o == nil || IsNil(o.SuppFeat) {
		return nil, false
	}
	return o.SuppFeat, true
}

// HasSuppFeat returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasSuppFeat() bool {
	if o != nil && !IsNil(o.SuppFeat) {
		return true
	}

	return false
}

// SetSuppFeat gets a reference to the given string and assigns it to the SuppFeat field.
func (o *AppAmContextExpData) SetSuppFeat(v string) {
	o.SuppFeat = &v
}

// GetRequestTestNotification returns the RequestTestNotification field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetRequestTestNotification() bool {
	if o == nil || IsNil(o.RequestTestNotification) {
		var ret bool
		return ret
	}
	return *o.RequestTestNotification
}

// GetRequestTestNotificationOk returns a tuple with the RequestTestNotification field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetRequestTestNotificationOk() (*bool, bool) {
	if o == nil || IsNil(o.RequestTestNotification) {
		return nil, false
	}
	return o.RequestTestNotification, true
}

// HasRequestTestNotification returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasRequestTestNotification() bool {
	if o != nil && !IsNil(o.RequestTestNotification) {
		return true
	}

	return false
}

// SetRequestTestNotification gets a reference to the given bool and assigns it to the RequestTestNotification field.
func (o *AppAmContextExpData) SetRequestTestNotification(v bool) {
	o.RequestTestNotification = &v
}

// GetWebsockNotifConfig returns the WebsockNotifConfig field value if set, zero value otherwise.
func (o *AppAmContextExpData) GetWebsockNotifConfig() WebsockNotifConfig {
	if o == nil || IsNil(o.WebsockNotifConfig) {
		var ret WebsockNotifConfig
		return ret
	}
	return *o.WebsockNotifConfig
}

// GetWebsockNotifConfigOk returns a tuple with the WebsockNotifConfig field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AppAmContextExpData) GetWebsockNotifConfigOk() (*WebsockNotifConfig, bool) {
	if o == nil || IsNil(o.WebsockNotifConfig) {
		return nil, false
	}
	return o.WebsockNotifConfig, true
}

// HasWebsockNotifConfig returns a boolean if a field has been set.
func (o *AppAmContextExpData) HasWebsockNotifConfig() bool {
	if o != nil && !IsNil(o.WebsockNotifConfig) {
		return true
	}

	return false
}

// SetWebsockNotifConfig gets a reference to the given WebsockNotifConfig and assigns it to the WebsockNotifConfig field.
func (o *AppAmContextExpData) SetWebsockNotifConfig(v WebsockNotifConfig) {
	o.WebsockNotifConfig = &v
}

func (o AppAmContextExpData) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AppAmContextExpData) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Self) {
		toSerialize["self"] = o.Self
	}
	if !IsNil(o.EvSubscs) {
		toSerialize["evSubscs"] = o.EvSubscs
	}
	toSerialize["gpsi"] = o.Gpsi
	if !IsNil(o.HighThruInd) {
		toSerialize["highThruInd"] = o.HighThruInd
	}
	if o.CovReqs != nil {
		toSerialize["covReqs"] = o.CovReqs
	}
	if o.PolicyDuration.IsSet() {
		toSerialize["policyDuration"] = o.PolicyDuration.Get()
	}
	if !IsNil(o.SuppFeat) {
		toSerialize["suppFeat"] = o.SuppFeat
	}
	if !IsNil(o.RequestTestNotification) {
		toSerialize["requestTestNotification"] = o.RequestTestNotification
	}
	if !IsNil(o.WebsockNotifConfig) {
		toSerialize["websockNotifConfig"] = o.WebsockNotifConfig
	}
	return toSerialize, nil
}

func (o *AppAmContextExpData) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"gpsi",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varAppAmContextExpData := _AppAmContextExpData{}

	err = json.Unmarshal(bytes, &varAppAmContextExpData)

	if err != nil {
		return err
	}

	*o = AppAmContextExpData(varAppAmContextExpData)

	return err
}

type NullableAppAmContextExpData struct {
	value *AppAmContextExpData
	isSet bool
}

func (v NullableAppAmContextExpData) Get() *AppAmContextExpData {
	return v.value
}

func (v *NullableAppAmContextExpData) Set(val *AppAmContextExpData) {
	v.value = val
	v.isSet = true
}

func (v NullableAppAmContextExpData) IsSet() bool {
	return v.isSet
}

func (v *NullableAppAmContextExpData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAppAmContextExpData(val *AppAmContextExpData) *NullableAppAmContextExpData {
	return &NullableAppAmContextExpData{value: val, isSet: true}
}

func (v NullableAppAmContextExpData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAppAmContextExpData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
