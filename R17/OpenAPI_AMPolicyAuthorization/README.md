# Go API client for OpenAPI_AMPolicyAuthorization

API for AM policy authorization.  
© 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0.2
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_AMPolicyAuthorization "gitee.com/konglinglong/openapi/OpenAPI_AMPolicyAuthorization"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_AMPolicyAuthorization.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_AMPolicyAuthorization.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_AMPolicyAuthorization.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_AMPolicyAuthorization.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_AMPolicyAuthorization.ContextOperationServerIndices` and `OpenAPI_AMPolicyAuthorization.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_AMPolicyAuthorization.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_AMPolicyAuthorization.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/3gpp-am-policyauthorization/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AMPolicyEventsSubscriptionAPI* | [**DeleteAmEventsSubsc**](docs/AMPolicyEventsSubscriptionAPI.md#deleteameventssubsc) | **Delete** /{afId}/app-am-contexts/{appAmContextId}/events-subscription | deletes the AM Policy Events Subscription sub-resource
*AMPolicyEventsSubscriptionAPI* | [**UpdateAmEventsSubsc**](docs/AMPolicyEventsSubscriptionAPI.md#updateameventssubsc) | **Put** /{afId}/app-am-contexts/{appAmContextId}/events-subscription | creates or modifies an AM Policy Events Subscription sub-resource.
*ApplicationAMContextsAPI* | [**PostAppAmContexts**](docs/ApplicationAMContextsAPI.md#postappamcontexts) | **Post** /{afId}/app-am-contexts | Creates a new Individual application AM Context resource
*IndividualApplicationAMContextAPI* | [**DeleteAppAmContext**](docs/IndividualApplicationAMContextAPI.md#deleteappamcontext) | **Delete** /{afId}/app-am-contexts/{appAmContextId} | Deletes an existing Individual Application AM Context
*IndividualApplicationAMContextAPI* | [**GetAppAmContext**](docs/IndividualApplicationAMContextAPI.md#getappamcontext) | **Get** /{afId}/app-am-contexts/{appAmContextId} | read an existing Individual application AM context
*IndividualApplicationAMContextAPI* | [**ModAppAmContext**](docs/IndividualApplicationAMContextAPI.md#modappamcontext) | **Patch** /{afId}/app-am-contexts/{appAmContextId} | partial modifies an existing Individual application AM context


## Documentation For Models

 - [AmEvent](docs/AmEvent.md)
 - [AmEventData](docs/AmEventData.md)
 - [AmEventNotification](docs/AmEventNotification.md)
 - [AmEventsNotification](docs/AmEventsNotification.md)
 - [AmEventsSubscData](docs/AmEventsSubscData.md)
 - [AmEventsSubscDataRm](docs/AmEventsSubscDataRm.md)
 - [AmEventsSubscRespData](docs/AmEventsSubscRespData.md)
 - [AppAmContextData](docs/AppAmContextData.md)
 - [AppAmContextExpData](docs/AppAmContextExpData.md)
 - [AppAmContextExpRespData](docs/AppAmContextExpRespData.md)
 - [AppAmContextExpUpdateData](docs/AppAmContextExpUpdateData.md)
 - [AsTimeDistributionParam](docs/AsTimeDistributionParam.md)
 - [CivicAddress](docs/CivicAddress.md)
 - [EllipsoidArc](docs/EllipsoidArc.md)
 - [GADShape](docs/GADShape.md)
 - [GeographicArea](docs/GeographicArea.md)
 - [GeographicalArea](docs/GeographicalArea.md)
 - [GeographicalCoordinates](docs/GeographicalCoordinates.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [Local2dPointUncertaintyEllipse](docs/Local2dPointUncertaintyEllipse.md)
 - [Local3dPointUncertaintyEllipsoid](docs/Local3dPointUncertaintyEllipsoid.md)
 - [LocalOrigin](docs/LocalOrigin.md)
 - [NotificationMethod](docs/NotificationMethod.md)
 - [PduidInformation](docs/PduidInformation.md)
 - [PlmnIdNid](docs/PlmnIdNid.md)
 - [Point](docs/Point.md)
 - [PointAltitude](docs/PointAltitude.md)
 - [PointAltitudeUncertainty](docs/PointAltitudeUncertainty.md)
 - [PointUncertaintyCircle](docs/PointUncertaintyCircle.md)
 - [PointUncertaintyEllipse](docs/PointUncertaintyEllipse.md)
 - [Polygon](docs/Polygon.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [RelativeCartesianLocation](docs/RelativeCartesianLocation.md)
 - [ServiceAreaCoverageInfo](docs/ServiceAreaCoverageInfo.md)
 - [SupportedGADShapes](docs/SupportedGADShapes.md)
 - [UncertaintyEllipse](docs/UncertaintyEllipse.md)
 - [UncertaintyEllipsoid](docs/UncertaintyEllipsoid.md)
 - [WebsockNotifConfig](docs/WebsockNotifConfig.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: N/A

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_AMPolicyAuthorization.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_AMPolicyAuthorization.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



