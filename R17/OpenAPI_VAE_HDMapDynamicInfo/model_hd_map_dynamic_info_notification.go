/*
VAE_HDMapDynamicInfo

API for VAE HDMapDynamicInfo Service   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_VAE_HDMapDynamicInfo

import (
	"encoding/json"
	"fmt"
)

// checks if the HdMapDynamicInfoNotification type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &HdMapDynamicInfoNotification{}

// HdMapDynamicInfoNotification Represents a notificaton of HD map dynamic info corresponding to the subscription.
type HdMapDynamicInfoNotification struct {
	// String providing an URI formatted according to RFC 3986.
	ResourceUri string `json:"resourceUri"`
	// Contains the informaiotn of nearby UEs.
	NearbyUeInfo []NearbyUeInfo `json:"nearbyUeInfo"`
}

type _HdMapDynamicInfoNotification HdMapDynamicInfoNotification

// NewHdMapDynamicInfoNotification instantiates a new HdMapDynamicInfoNotification object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewHdMapDynamicInfoNotification(resourceUri string, nearbyUeInfo []NearbyUeInfo) *HdMapDynamicInfoNotification {
	this := HdMapDynamicInfoNotification{}
	this.ResourceUri = resourceUri
	this.NearbyUeInfo = nearbyUeInfo
	return &this
}

// NewHdMapDynamicInfoNotificationWithDefaults instantiates a new HdMapDynamicInfoNotification object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewHdMapDynamicInfoNotificationWithDefaults() *HdMapDynamicInfoNotification {
	this := HdMapDynamicInfoNotification{}
	return &this
}

// GetResourceUri returns the ResourceUri field value
func (o *HdMapDynamicInfoNotification) GetResourceUri() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ResourceUri
}

// GetResourceUriOk returns a tuple with the ResourceUri field value
// and a boolean to check if the value has been set.
func (o *HdMapDynamicInfoNotification) GetResourceUriOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ResourceUri, true
}

// SetResourceUri sets field value
func (o *HdMapDynamicInfoNotification) SetResourceUri(v string) {
	o.ResourceUri = v
}

// GetNearbyUeInfo returns the NearbyUeInfo field value
func (o *HdMapDynamicInfoNotification) GetNearbyUeInfo() []NearbyUeInfo {
	if o == nil {
		var ret []NearbyUeInfo
		return ret
	}

	return o.NearbyUeInfo
}

// GetNearbyUeInfoOk returns a tuple with the NearbyUeInfo field value
// and a boolean to check if the value has been set.
func (o *HdMapDynamicInfoNotification) GetNearbyUeInfoOk() ([]NearbyUeInfo, bool) {
	if o == nil {
		return nil, false
	}
	return o.NearbyUeInfo, true
}

// SetNearbyUeInfo sets field value
func (o *HdMapDynamicInfoNotification) SetNearbyUeInfo(v []NearbyUeInfo) {
	o.NearbyUeInfo = v
}

func (o HdMapDynamicInfoNotification) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o HdMapDynamicInfoNotification) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["resourceUri"] = o.ResourceUri
	toSerialize["nearbyUeInfo"] = o.NearbyUeInfo
	return toSerialize, nil
}

func (o *HdMapDynamicInfoNotification) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"resourceUri",
		"nearbyUeInfo",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varHdMapDynamicInfoNotification := _HdMapDynamicInfoNotification{}

	err = json.Unmarshal(bytes, &varHdMapDynamicInfoNotification)

	if err != nil {
		return err
	}

	*o = HdMapDynamicInfoNotification(varHdMapDynamicInfoNotification)

	return err
}

type NullableHdMapDynamicInfoNotification struct {
	value *HdMapDynamicInfoNotification
	isSet bool
}

func (v NullableHdMapDynamicInfoNotification) Get() *HdMapDynamicInfoNotification {
	return v.value
}

func (v *NullableHdMapDynamicInfoNotification) Set(val *HdMapDynamicInfoNotification) {
	v.value = val
	v.isSet = true
}

func (v NullableHdMapDynamicInfoNotification) IsSet() bool {
	return v.isSet
}

func (v *NullableHdMapDynamicInfoNotification) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableHdMapDynamicInfoNotification(val *HdMapDynamicInfoNotification) *NullableHdMapDynamicInfoNotification {
	return &NullableHdMapDynamicInfoNotification{value: val, isSet: true}
}

func (v NullableHdMapDynamicInfoNotification) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableHdMapDynamicInfoNotification) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
