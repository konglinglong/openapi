# Go API client for OpenAPI_Nnef_EventExposure

NEF Event Exposure Service.  
© 2022 , 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.2.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_Nnef_EventExposure "gitee.com/konglinglong/openapi/OpenAPI_Nnef_EventExposure"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_Nnef_EventExposure.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Nnef_EventExposure.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_Nnef_EventExposure.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Nnef_EventExposure.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_Nnef_EventExposure.ContextOperationServerIndices` and `OpenAPI_Nnef_EventExposure.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_Nnef_EventExposure.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_Nnef_EventExposure.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/nnef-eventexposure/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*IndividualSubscriptionDocumentAPI* | [**DeleteIndividualSubcription**](docs/IndividualSubscriptionDocumentAPI.md#deleteindividualsubcription) | **Delete** /subscriptions/{subscriptionId} | unsubscribe from notifications
*IndividualSubscriptionDocumentAPI* | [**GetIndividualSubcription**](docs/IndividualSubscriptionDocumentAPI.md#getindividualsubcription) | **Get** /subscriptions/{subscriptionId} | retrieve subscription
*IndividualSubscriptionDocumentAPI* | [**ReplaceIndividualSubcription**](docs/IndividualSubscriptionDocumentAPI.md#replaceindividualsubcription) | **Put** /subscriptions/{subscriptionId} | update subscription
*SubscriptionsCollectionAPI* | [**CreateIndividualSubcription**](docs/SubscriptionsCollectionAPI.md#createindividualsubcription) | **Post** /subscriptions | subscribe to notifications


## Documentation For Models

 - [AccessTokenErr](docs/AccessTokenErr.md)
 - [AccessTokenReq](docs/AccessTokenReq.md)
 - [AddrFqdn](docs/AddrFqdn.md)
 - [BaseRecord](docs/BaseRecord.md)
 - [CacheStatus](docs/CacheStatus.md)
 - [CellGlobalId](docs/CellGlobalId.md)
 - [CivicAddress](docs/CivicAddress.md)
 - [CollectiveBehaviourFilter](docs/CollectiveBehaviourFilter.md)
 - [CollectiveBehaviourFilterType](docs/CollectiveBehaviourFilterType.md)
 - [CollectiveBehaviourInfo](docs/CollectiveBehaviourInfo.md)
 - [CommunicationCollection](docs/CommunicationCollection.md)
 - [DispersionCollection](docs/DispersionCollection.md)
 - [DynamicPolicy](docs/DynamicPolicy.md)
 - [Ecgi](docs/Ecgi.md)
 - [EllipsoidArc](docs/EllipsoidArc.md)
 - [EndpointAddress](docs/EndpointAddress.md)
 - [EthFlowDescription](docs/EthFlowDescription.md)
 - [EutraLocation](docs/EutraLocation.md)
 - [Exception](docs/Exception.md)
 - [ExceptionId](docs/ExceptionId.md)
 - [ExceptionInfo](docs/ExceptionInfo.md)
 - [ExceptionTrend](docs/ExceptionTrend.md)
 - [FlowDirection](docs/FlowDirection.md)
 - [FlowInfo](docs/FlowInfo.md)
 - [GADShape](docs/GADShape.md)
 - [GNbId](docs/GNbId.md)
 - [GeographicArea](docs/GeographicArea.md)
 - [GeographicalCoordinates](docs/GeographicalCoordinates.md)
 - [GeraLocation](docs/GeraLocation.md)
 - [GlobalRanNodeId](docs/GlobalRanNodeId.md)
 - [HfcNodeId](docs/HfcNodeId.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [IpAddr](docs/IpAddr.md)
 - [IpPacketFilterSet](docs/IpPacketFilterSet.md)
 - [Ipv6Addr](docs/Ipv6Addr.md)
 - [Ipv6Prefix](docs/Ipv6Prefix.md)
 - [LineType](docs/LineType.md)
 - [Local2dPointUncertaintyEllipse](docs/Local2dPointUncertaintyEllipse.md)
 - [Local3dPointUncertaintyEllipsoid](docs/Local3dPointUncertaintyEllipsoid.md)
 - [LocalOrigin](docs/LocalOrigin.md)
 - [LocationArea5G](docs/LocationArea5G.md)
 - [LocationAreaId](docs/LocationAreaId.md)
 - [M5QoSSpecification](docs/M5QoSSpecification.md)
 - [MSAccessActivityCollection](docs/MSAccessActivityCollection.md)
 - [MediaStreamingAccessRecord](docs/MediaStreamingAccessRecord.md)
 - [MediaStreamingAccessRecordAllOfConnectionMetrics](docs/MediaStreamingAccessRecordAllOfConnectionMetrics.md)
 - [MediaStreamingAccessRecordAllOfRequestMessage](docs/MediaStreamingAccessRecordAllOfRequestMessage.md)
 - [MediaStreamingAccessRecordAllOfResponseMessage](docs/MediaStreamingAccessRecordAllOfResponseMessage.md)
 - [MediaType](docs/MediaType.md)
 - [MsConsumptionCollection](docs/MsConsumptionCollection.md)
 - [MsDynPolicyInvocationCollection](docs/MsDynPolicyInvocationCollection.md)
 - [MsNetAssInvocationCollection](docs/MsNetAssInvocationCollection.md)
 - [MsQoeMetricsCollection](docs/MsQoeMetricsCollection.md)
 - [N3gaLocation](docs/N3gaLocation.md)
 - [NFType](docs/NFType.md)
 - [Ncgi](docs/Ncgi.md)
 - [NefEvent](docs/NefEvent.md)
 - [NefEventExposureNotif](docs/NefEventExposureNotif.md)
 - [NefEventExposureSubsc](docs/NefEventExposureSubsc.md)
 - [NefEventFilter](docs/NefEventFilter.md)
 - [NefEventNotification](docs/NefEventNotification.md)
 - [NefEventSubs](docs/NefEventSubs.md)
 - [NetworkAreaInfo](docs/NetworkAreaInfo.md)
 - [NetworkAssistanceSession](docs/NetworkAssistanceSession.md)
 - [NotificationFlag](docs/NotificationFlag.md)
 - [NotificationMethod](docs/NotificationMethod.md)
 - [NrLocation](docs/NrLocation.md)
 - [PartitioningCriteria](docs/PartitioningCriteria.md)
 - [PerUeAttribute](docs/PerUeAttribute.md)
 - [PerformanceData](docs/PerformanceData.md)
 - [PerformanceDataInfo](docs/PerformanceDataInfo.md)
 - [PlmnId](docs/PlmnId.md)
 - [PlmnIdNid](docs/PlmnIdNid.md)
 - [Point](docs/Point.md)
 - [PointAltitude](docs/PointAltitude.md)
 - [PointAltitudeUncertainty](docs/PointAltitudeUncertainty.md)
 - [PointUncertaintyCircle](docs/PointUncertaintyCircle.md)
 - [PointUncertaintyEllipse](docs/PointUncertaintyEllipse.md)
 - [Polygon](docs/Polygon.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [RedirectResponse](docs/RedirectResponse.md)
 - [RelativeCartesianLocation](docs/RelativeCartesianLocation.md)
 - [ReportingInformation](docs/ReportingInformation.md)
 - [RoutingAreaId](docs/RoutingAreaId.md)
 - [ServiceAreaId](docs/ServiceAreaId.md)
 - [ServiceDataFlowDescription](docs/ServiceDataFlowDescription.md)
 - [ServiceExperienceInfo](docs/ServiceExperienceInfo.md)
 - [ServiceExperienceInfoPerFlow](docs/ServiceExperienceInfoPerFlow.md)
 - [Snssai](docs/Snssai.md)
 - [SupportedGADShapes](docs/SupportedGADShapes.md)
 - [SvcExperience](docs/SvcExperience.md)
 - [Tai](docs/Tai.md)
 - [TargetUeIdentification](docs/TargetUeIdentification.md)
 - [TimeWindow](docs/TimeWindow.md)
 - [TnapId](docs/TnapId.md)
 - [TransportProtocol](docs/TransportProtocol.md)
 - [TwapId](docs/TwapId.md)
 - [UeCommunicationInfo](docs/UeCommunicationInfo.md)
 - [UeMobilityInfo](docs/UeMobilityInfo.md)
 - [UeTrajectoryInfo](docs/UeTrajectoryInfo.md)
 - [UncertaintyEllipse](docs/UncertaintyEllipse.md)
 - [UncertaintyEllipsoid](docs/UncertaintyEllipsoid.md)
 - [UsageThreshold](docs/UsageThreshold.md)
 - [UserDataCongestionCollection](docs/UserDataCongestionCollection.md)
 - [UserLocation](docs/UserLocation.md)
 - [UtraLocation](docs/UtraLocation.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: 
 - **nnef-eventexposure**: Access to the Nnef_EventExposure API

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_Nnef_EventExposure.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_Nnef_EventExposure.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



