/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// checks if the RadioNetworkExpectation type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &RadioNetworkExpectation{}

// RadioNetworkExpectation This data type is the \"IntentExpectation\" data type with specialisations to represent MnS consumer's expectations for radio network delivering and performance assurance
type RadioNetworkExpectation struct {
	ExpectationId             string                                           `json:"expectationId"`
	ExpectationVerb           *ExpectationVerb                                 `json:"expectationVerb,omitempty"`
	ExpectationObject         *RadioNetworkExpectationObject                   `json:"expectationObject,omitempty"`
	ExpectationTargets        []RadioNetworkExpectationExpectationTargetsInner `json:"expectationTargets,omitempty"`
	ExpectationContexts       []ExpectationContext                             `json:"expectationContexts,omitempty"`
	ExpectationfulfilmentInfo *FulfilmentInfo                                  `json:"expectationfulfilmentInfo,omitempty"`
}

type _RadioNetworkExpectation RadioNetworkExpectation

// NewRadioNetworkExpectation instantiates a new RadioNetworkExpectation object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewRadioNetworkExpectation(expectationId string) *RadioNetworkExpectation {
	this := RadioNetworkExpectation{}
	this.ExpectationId = expectationId
	return &this
}

// NewRadioNetworkExpectationWithDefaults instantiates a new RadioNetworkExpectation object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewRadioNetworkExpectationWithDefaults() *RadioNetworkExpectation {
	this := RadioNetworkExpectation{}
	return &this
}

// GetExpectationId returns the ExpectationId field value
func (o *RadioNetworkExpectation) GetExpectationId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.ExpectationId
}

// GetExpectationIdOk returns a tuple with the ExpectationId field value
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ExpectationId, true
}

// SetExpectationId sets field value
func (o *RadioNetworkExpectation) SetExpectationId(v string) {
	o.ExpectationId = v
}

// GetExpectationVerb returns the ExpectationVerb field value if set, zero value otherwise.
func (o *RadioNetworkExpectation) GetExpectationVerb() ExpectationVerb {
	if o == nil || IsNil(o.ExpectationVerb) {
		var ret ExpectationVerb
		return ret
	}
	return *o.ExpectationVerb
}

// GetExpectationVerbOk returns a tuple with the ExpectationVerb field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationVerbOk() (*ExpectationVerb, bool) {
	if o == nil || IsNil(o.ExpectationVerb) {
		return nil, false
	}
	return o.ExpectationVerb, true
}

// HasExpectationVerb returns a boolean if a field has been set.
func (o *RadioNetworkExpectation) HasExpectationVerb() bool {
	if o != nil && !IsNil(o.ExpectationVerb) {
		return true
	}

	return false
}

// SetExpectationVerb gets a reference to the given ExpectationVerb and assigns it to the ExpectationVerb field.
func (o *RadioNetworkExpectation) SetExpectationVerb(v ExpectationVerb) {
	o.ExpectationVerb = &v
}

// GetExpectationObject returns the ExpectationObject field value if set, zero value otherwise.
func (o *RadioNetworkExpectation) GetExpectationObject() RadioNetworkExpectationObject {
	if o == nil || IsNil(o.ExpectationObject) {
		var ret RadioNetworkExpectationObject
		return ret
	}
	return *o.ExpectationObject
}

// GetExpectationObjectOk returns a tuple with the ExpectationObject field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationObjectOk() (*RadioNetworkExpectationObject, bool) {
	if o == nil || IsNil(o.ExpectationObject) {
		return nil, false
	}
	return o.ExpectationObject, true
}

// HasExpectationObject returns a boolean if a field has been set.
func (o *RadioNetworkExpectation) HasExpectationObject() bool {
	if o != nil && !IsNil(o.ExpectationObject) {
		return true
	}

	return false
}

// SetExpectationObject gets a reference to the given RadioNetworkExpectationObject and assigns it to the ExpectationObject field.
func (o *RadioNetworkExpectation) SetExpectationObject(v RadioNetworkExpectationObject) {
	o.ExpectationObject = &v
}

// GetExpectationTargets returns the ExpectationTargets field value if set, zero value otherwise.
func (o *RadioNetworkExpectation) GetExpectationTargets() []RadioNetworkExpectationExpectationTargetsInner {
	if o == nil || IsNil(o.ExpectationTargets) {
		var ret []RadioNetworkExpectationExpectationTargetsInner
		return ret
	}
	return o.ExpectationTargets
}

// GetExpectationTargetsOk returns a tuple with the ExpectationTargets field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationTargetsOk() ([]RadioNetworkExpectationExpectationTargetsInner, bool) {
	if o == nil || IsNil(o.ExpectationTargets) {
		return nil, false
	}
	return o.ExpectationTargets, true
}

// HasExpectationTargets returns a boolean if a field has been set.
func (o *RadioNetworkExpectation) HasExpectationTargets() bool {
	if o != nil && !IsNil(o.ExpectationTargets) {
		return true
	}

	return false
}

// SetExpectationTargets gets a reference to the given []RadioNetworkExpectationExpectationTargetsInner and assigns it to the ExpectationTargets field.
func (o *RadioNetworkExpectation) SetExpectationTargets(v []RadioNetworkExpectationExpectationTargetsInner) {
	o.ExpectationTargets = v
}

// GetExpectationContexts returns the ExpectationContexts field value if set, zero value otherwise.
func (o *RadioNetworkExpectation) GetExpectationContexts() []ExpectationContext {
	if o == nil || IsNil(o.ExpectationContexts) {
		var ret []ExpectationContext
		return ret
	}
	return o.ExpectationContexts
}

// GetExpectationContextsOk returns a tuple with the ExpectationContexts field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationContextsOk() ([]ExpectationContext, bool) {
	if o == nil || IsNil(o.ExpectationContexts) {
		return nil, false
	}
	return o.ExpectationContexts, true
}

// HasExpectationContexts returns a boolean if a field has been set.
func (o *RadioNetworkExpectation) HasExpectationContexts() bool {
	if o != nil && !IsNil(o.ExpectationContexts) {
		return true
	}

	return false
}

// SetExpectationContexts gets a reference to the given []ExpectationContext and assigns it to the ExpectationContexts field.
func (o *RadioNetworkExpectation) SetExpectationContexts(v []ExpectationContext) {
	o.ExpectationContexts = v
}

// GetExpectationfulfilmentInfo returns the ExpectationfulfilmentInfo field value if set, zero value otherwise.
func (o *RadioNetworkExpectation) GetExpectationfulfilmentInfo() FulfilmentInfo {
	if o == nil || IsNil(o.ExpectationfulfilmentInfo) {
		var ret FulfilmentInfo
		return ret
	}
	return *o.ExpectationfulfilmentInfo
}

// GetExpectationfulfilmentInfoOk returns a tuple with the ExpectationfulfilmentInfo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *RadioNetworkExpectation) GetExpectationfulfilmentInfoOk() (*FulfilmentInfo, bool) {
	if o == nil || IsNil(o.ExpectationfulfilmentInfo) {
		return nil, false
	}
	return o.ExpectationfulfilmentInfo, true
}

// HasExpectationfulfilmentInfo returns a boolean if a field has been set.
func (o *RadioNetworkExpectation) HasExpectationfulfilmentInfo() bool {
	if o != nil && !IsNil(o.ExpectationfulfilmentInfo) {
		return true
	}

	return false
}

// SetExpectationfulfilmentInfo gets a reference to the given FulfilmentInfo and assigns it to the ExpectationfulfilmentInfo field.
func (o *RadioNetworkExpectation) SetExpectationfulfilmentInfo(v FulfilmentInfo) {
	o.ExpectationfulfilmentInfo = &v
}

func (o RadioNetworkExpectation) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o RadioNetworkExpectation) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["expectationId"] = o.ExpectationId
	if !IsNil(o.ExpectationVerb) {
		toSerialize["expectationVerb"] = o.ExpectationVerb
	}
	if !IsNil(o.ExpectationObject) {
		toSerialize["expectationObject"] = o.ExpectationObject
	}
	if !IsNil(o.ExpectationTargets) {
		toSerialize["expectationTargets"] = o.ExpectationTargets
	}
	if !IsNil(o.ExpectationContexts) {
		toSerialize["expectationContexts"] = o.ExpectationContexts
	}
	if !IsNil(o.ExpectationfulfilmentInfo) {
		toSerialize["expectationfulfilmentInfo"] = o.ExpectationfulfilmentInfo
	}
	return toSerialize, nil
}

func (o *RadioNetworkExpectation) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"expectationId",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varRadioNetworkExpectation := _RadioNetworkExpectation{}

	err = json.Unmarshal(bytes, &varRadioNetworkExpectation)

	if err != nil {
		return err
	}

	*o = RadioNetworkExpectation(varRadioNetworkExpectation)

	return err
}

type NullableRadioNetworkExpectation struct {
	value *RadioNetworkExpectation
	isSet bool
}

func (v NullableRadioNetworkExpectation) Get() *RadioNetworkExpectation {
	return v.value
}

func (v *NullableRadioNetworkExpectation) Set(val *RadioNetworkExpectation) {
	v.value = val
	v.isSet = true
}

func (v NullableRadioNetworkExpectation) IsSet() bool {
	return v.isSet
}

func (v *NullableRadioNetworkExpectation) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableRadioNetworkExpectation(val *RadioNetworkExpectation) *NullableRadioNetworkExpectation {
	return &NullableRadioNetworkExpectation{value: val, isSet: true}
}

func (v NullableRadioNetworkExpectation) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableRadioNetworkExpectation) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
