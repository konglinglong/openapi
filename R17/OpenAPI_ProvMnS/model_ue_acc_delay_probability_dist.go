/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
)

// checks if the UeAccDelayProbabilityDist type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UeAccDelayProbabilityDist{}

// UeAccDelayProbabilityDist struct for UeAccDelayProbabilityDist
type UeAccDelayProbabilityDist struct {
	TargetProbability *int32 `json:"targetProbability,omitempty"`
	Accessdelay       *int32 `json:"accessdelay,omitempty"`
}

// NewUeAccDelayProbabilityDist instantiates a new UeAccDelayProbabilityDist object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUeAccDelayProbabilityDist() *UeAccDelayProbabilityDist {
	this := UeAccDelayProbabilityDist{}
	return &this
}

// NewUeAccDelayProbabilityDistWithDefaults instantiates a new UeAccDelayProbabilityDist object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUeAccDelayProbabilityDistWithDefaults() *UeAccDelayProbabilityDist {
	this := UeAccDelayProbabilityDist{}
	return &this
}

// GetTargetProbability returns the TargetProbability field value if set, zero value otherwise.
func (o *UeAccDelayProbabilityDist) GetTargetProbability() int32 {
	if o == nil || IsNil(o.TargetProbability) {
		var ret int32
		return ret
	}
	return *o.TargetProbability
}

// GetTargetProbabilityOk returns a tuple with the TargetProbability field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UeAccDelayProbabilityDist) GetTargetProbabilityOk() (*int32, bool) {
	if o == nil || IsNil(o.TargetProbability) {
		return nil, false
	}
	return o.TargetProbability, true
}

// HasTargetProbability returns a boolean if a field has been set.
func (o *UeAccDelayProbabilityDist) HasTargetProbability() bool {
	if o != nil && !IsNil(o.TargetProbability) {
		return true
	}

	return false
}

// SetTargetProbability gets a reference to the given int32 and assigns it to the TargetProbability field.
func (o *UeAccDelayProbabilityDist) SetTargetProbability(v int32) {
	o.TargetProbability = &v
}

// GetAccessdelay returns the Accessdelay field value if set, zero value otherwise.
func (o *UeAccDelayProbabilityDist) GetAccessdelay() int32 {
	if o == nil || IsNil(o.Accessdelay) {
		var ret int32
		return ret
	}
	return *o.Accessdelay
}

// GetAccessdelayOk returns a tuple with the Accessdelay field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UeAccDelayProbabilityDist) GetAccessdelayOk() (*int32, bool) {
	if o == nil || IsNil(o.Accessdelay) {
		return nil, false
	}
	return o.Accessdelay, true
}

// HasAccessdelay returns a boolean if a field has been set.
func (o *UeAccDelayProbabilityDist) HasAccessdelay() bool {
	if o != nil && !IsNil(o.Accessdelay) {
		return true
	}

	return false
}

// SetAccessdelay gets a reference to the given int32 and assigns it to the Accessdelay field.
func (o *UeAccDelayProbabilityDist) SetAccessdelay(v int32) {
	o.Accessdelay = &v
}

func (o UeAccDelayProbabilityDist) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UeAccDelayProbabilityDist) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.TargetProbability) {
		toSerialize["targetProbability"] = o.TargetProbability
	}
	if !IsNil(o.Accessdelay) {
		toSerialize["accessdelay"] = o.Accessdelay
	}
	return toSerialize, nil
}

type NullableUeAccDelayProbabilityDist struct {
	value *UeAccDelayProbabilityDist
	isSet bool
}

func (v NullableUeAccDelayProbabilityDist) Get() *UeAccDelayProbabilityDist {
	return v.value
}

func (v *NullableUeAccDelayProbabilityDist) Set(val *UeAccDelayProbabilityDist) {
	v.value = val
	v.isSet = true
}

func (v NullableUeAccDelayProbabilityDist) IsSet() bool {
	return v.isSet
}

func (v *NullableUeAccDelayProbabilityDist) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUeAccDelayProbabilityDist(val *UeAccDelayProbabilityDist) *NullableUeAccDelayProbabilityDist {
	return &NullableUeAccDelayProbabilityDist{value: val, isSet: true}
}

func (v NullableUeAccDelayProbabilityDist) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUeAccDelayProbabilityDist) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
