/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// QOffsetRange the model 'QOffsetRange'
type QOffsetRange int32

// List of QOffsetRange
const (
	QOFFSETRANGE__MINUS_24 QOffsetRange = -24
	QOFFSETRANGE__MINUS_22 QOffsetRange = -22
	QOFFSETRANGE__MINUS_20 QOffsetRange = -20
	QOFFSETRANGE__MINUS_18 QOffsetRange = -18
	QOFFSETRANGE__MINUS_16 QOffsetRange = -16
	QOFFSETRANGE__MINUS_14 QOffsetRange = -14
	QOFFSETRANGE__MINUS_12 QOffsetRange = -12
	QOFFSETRANGE__MINUS_10 QOffsetRange = -10
	QOFFSETRANGE__MINUS_8  QOffsetRange = -8
	QOFFSETRANGE__MINUS_6  QOffsetRange = -6
	QOFFSETRANGE__MINUS_5  QOffsetRange = -5
	QOFFSETRANGE__MINUS_4  QOffsetRange = -4
	QOFFSETRANGE__MINUS_3  QOffsetRange = -3
	QOFFSETRANGE__MINUS_2  QOffsetRange = -2
	QOFFSETRANGE__MINUS_1  QOffsetRange = -1
	QOFFSETRANGE__0        QOffsetRange = 0
	QOFFSETRANGE__24       QOffsetRange = 24
	QOFFSETRANGE__22       QOffsetRange = 22
	QOFFSETRANGE__20       QOffsetRange = 20
	QOFFSETRANGE__18       QOffsetRange = 18
	QOFFSETRANGE__16       QOffsetRange = 16
	QOFFSETRANGE__14       QOffsetRange = 14
	QOFFSETRANGE__12       QOffsetRange = 12
	QOFFSETRANGE__10       QOffsetRange = 10
	QOFFSETRANGE__8        QOffsetRange = 8
	QOFFSETRANGE__6        QOffsetRange = 6
	QOFFSETRANGE__5        QOffsetRange = 5
	QOFFSETRANGE__4        QOffsetRange = 4
	QOFFSETRANGE__3        QOffsetRange = 3
	QOFFSETRANGE__2        QOffsetRange = 2
	QOFFSETRANGE__1        QOffsetRange = 1
)

// All allowed values of QOffsetRange enum
var AllowedQOffsetRangeEnumValues = []QOffsetRange{
	-24,
	-22,
	-20,
	-18,
	-16,
	-14,
	-12,
	-10,
	-8,
	-6,
	-5,
	-4,
	-3,
	-2,
	-1,
	0,
	24,
	22,
	20,
	18,
	16,
	14,
	12,
	10,
	8,
	6,
	5,
	4,
	3,
	2,
	1,
}

func (v *QOffsetRange) UnmarshalJSON(src []byte) error {
	var value int32
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := QOffsetRange(value)
	for _, existing := range AllowedQOffsetRangeEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid QOffsetRange", value)
}

// NewQOffsetRangeFromValue returns a pointer to a valid QOffsetRange
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewQOffsetRangeFromValue(v int32) (*QOffsetRange, error) {
	ev := QOffsetRange(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for QOffsetRange: valid values are %v", v, AllowedQOffsetRangeEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v QOffsetRange) IsValid() bool {
	for _, existing := range AllowedQOffsetRangeEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to QOffsetRange value
func (v QOffsetRange) Ptr() *QOffsetRange {
	return &v
}

type NullableQOffsetRange struct {
	value *QOffsetRange
	isSet bool
}

func (v NullableQOffsetRange) Get() *QOffsetRange {
	return v.value
}

func (v *NullableQOffsetRange) Set(val *QOffsetRange) {
	v.value = val
	v.isSet = true
}

func (v NullableQOffsetRange) IsSet() bool {
	return v.isSet
}

func (v *NullableQOffsetRange) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableQOffsetRange(val *QOffsetRange) *NullableQOffsetRange {
	return &NullableQOffsetRange{value: val, isSet: true}
}

func (v NullableQOffsetRange) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableQOffsetRange) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
