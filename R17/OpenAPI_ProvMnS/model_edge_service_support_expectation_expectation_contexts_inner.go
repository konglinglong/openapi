/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// EdgeServiceSupportExpectationExpectationContextsInner - struct for EdgeServiceSupportExpectationExpectationContextsInner
type EdgeServiceSupportExpectationExpectationContextsInner struct {
	ExpectationContext          *ExpectationContext
	ResourceSharingLevelContext *ResourceSharingLevelContext
	ServiceEndTimeContext       *ServiceEndTimeContext
	ServiceStartTimeContext     *ServiceStartTimeContext
	UEMobilityLevelContext      *UEMobilityLevelContext
}

// ExpectationContextAsEdgeServiceSupportExpectationExpectationContextsInner is a convenience function that returns ExpectationContext wrapped in EdgeServiceSupportExpectationExpectationContextsInner
func ExpectationContextAsEdgeServiceSupportExpectationExpectationContextsInner(v *ExpectationContext) EdgeServiceSupportExpectationExpectationContextsInner {
	return EdgeServiceSupportExpectationExpectationContextsInner{
		ExpectationContext: v,
	}
}

// ResourceSharingLevelContextAsEdgeServiceSupportExpectationExpectationContextsInner is a convenience function that returns ResourceSharingLevelContext wrapped in EdgeServiceSupportExpectationExpectationContextsInner
func ResourceSharingLevelContextAsEdgeServiceSupportExpectationExpectationContextsInner(v *ResourceSharingLevelContext) EdgeServiceSupportExpectationExpectationContextsInner {
	return EdgeServiceSupportExpectationExpectationContextsInner{
		ResourceSharingLevelContext: v,
	}
}

// ServiceEndTimeContextAsEdgeServiceSupportExpectationExpectationContextsInner is a convenience function that returns ServiceEndTimeContext wrapped in EdgeServiceSupportExpectationExpectationContextsInner
func ServiceEndTimeContextAsEdgeServiceSupportExpectationExpectationContextsInner(v *ServiceEndTimeContext) EdgeServiceSupportExpectationExpectationContextsInner {
	return EdgeServiceSupportExpectationExpectationContextsInner{
		ServiceEndTimeContext: v,
	}
}

// ServiceStartTimeContextAsEdgeServiceSupportExpectationExpectationContextsInner is a convenience function that returns ServiceStartTimeContext wrapped in EdgeServiceSupportExpectationExpectationContextsInner
func ServiceStartTimeContextAsEdgeServiceSupportExpectationExpectationContextsInner(v *ServiceStartTimeContext) EdgeServiceSupportExpectationExpectationContextsInner {
	return EdgeServiceSupportExpectationExpectationContextsInner{
		ServiceStartTimeContext: v,
	}
}

// UEMobilityLevelContextAsEdgeServiceSupportExpectationExpectationContextsInner is a convenience function that returns UEMobilityLevelContext wrapped in EdgeServiceSupportExpectationExpectationContextsInner
func UEMobilityLevelContextAsEdgeServiceSupportExpectationExpectationContextsInner(v *UEMobilityLevelContext) EdgeServiceSupportExpectationExpectationContextsInner {
	return EdgeServiceSupportExpectationExpectationContextsInner{
		UEMobilityLevelContext: v,
	}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *EdgeServiceSupportExpectationExpectationContextsInner) UnmarshalJSON(data []byte) error {
	var err error
	match := 0
	// try to unmarshal data into ExpectationContext
	err = newStrictDecoder(data).Decode(&dst.ExpectationContext)
	if err == nil {
		jsonExpectationContext, _ := json.Marshal(dst.ExpectationContext)
		if string(jsonExpectationContext) == "{}" { // empty struct
			dst.ExpectationContext = nil
		} else {
			match++
		}
	} else {
		dst.ExpectationContext = nil
	}

	// try to unmarshal data into ResourceSharingLevelContext
	err = newStrictDecoder(data).Decode(&dst.ResourceSharingLevelContext)
	if err == nil {
		jsonResourceSharingLevelContext, _ := json.Marshal(dst.ResourceSharingLevelContext)
		if string(jsonResourceSharingLevelContext) == "{}" { // empty struct
			dst.ResourceSharingLevelContext = nil
		} else {
			match++
		}
	} else {
		dst.ResourceSharingLevelContext = nil
	}

	// try to unmarshal data into ServiceEndTimeContext
	err = newStrictDecoder(data).Decode(&dst.ServiceEndTimeContext)
	if err == nil {
		jsonServiceEndTimeContext, _ := json.Marshal(dst.ServiceEndTimeContext)
		if string(jsonServiceEndTimeContext) == "{}" { // empty struct
			dst.ServiceEndTimeContext = nil
		} else {
			match++
		}
	} else {
		dst.ServiceEndTimeContext = nil
	}

	// try to unmarshal data into ServiceStartTimeContext
	err = newStrictDecoder(data).Decode(&dst.ServiceStartTimeContext)
	if err == nil {
		jsonServiceStartTimeContext, _ := json.Marshal(dst.ServiceStartTimeContext)
		if string(jsonServiceStartTimeContext) == "{}" { // empty struct
			dst.ServiceStartTimeContext = nil
		} else {
			match++
		}
	} else {
		dst.ServiceStartTimeContext = nil
	}

	// try to unmarshal data into UEMobilityLevelContext
	err = newStrictDecoder(data).Decode(&dst.UEMobilityLevelContext)
	if err == nil {
		jsonUEMobilityLevelContext, _ := json.Marshal(dst.UEMobilityLevelContext)
		if string(jsonUEMobilityLevelContext) == "{}" { // empty struct
			dst.UEMobilityLevelContext = nil
		} else {
			match++
		}
	} else {
		dst.UEMobilityLevelContext = nil
	}

	if match > 1 { // more than 1 match
		// reset to nil
		dst.ExpectationContext = nil
		dst.ResourceSharingLevelContext = nil
		dst.ServiceEndTimeContext = nil
		dst.ServiceStartTimeContext = nil
		dst.UEMobilityLevelContext = nil

		return fmt.Errorf("data matches more than one schema in oneOf(EdgeServiceSupportExpectationExpectationContextsInner)")
	} else if match == 1 {
		return nil // exactly one match
	} else { // no match
		return fmt.Errorf("data failed to match schemas in oneOf(EdgeServiceSupportExpectationExpectationContextsInner)")
	}
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src EdgeServiceSupportExpectationExpectationContextsInner) MarshalJSON() ([]byte, error) {
	if src.ExpectationContext != nil {
		return json.Marshal(&src.ExpectationContext)
	}

	if src.ResourceSharingLevelContext != nil {
		return json.Marshal(&src.ResourceSharingLevelContext)
	}

	if src.ServiceEndTimeContext != nil {
		return json.Marshal(&src.ServiceEndTimeContext)
	}

	if src.ServiceStartTimeContext != nil {
		return json.Marshal(&src.ServiceStartTimeContext)
	}

	if src.UEMobilityLevelContext != nil {
		return json.Marshal(&src.UEMobilityLevelContext)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *EdgeServiceSupportExpectationExpectationContextsInner) GetActualInstance() interface{} {
	if obj == nil {
		return nil
	}
	if obj.ExpectationContext != nil {
		return obj.ExpectationContext
	}

	if obj.ResourceSharingLevelContext != nil {
		return obj.ResourceSharingLevelContext
	}

	if obj.ServiceEndTimeContext != nil {
		return obj.ServiceEndTimeContext
	}

	if obj.ServiceStartTimeContext != nil {
		return obj.ServiceStartTimeContext
	}

	if obj.UEMobilityLevelContext != nil {
		return obj.UEMobilityLevelContext
	}

	// all schemas are nil
	return nil
}

type NullableEdgeServiceSupportExpectationExpectationContextsInner struct {
	value *EdgeServiceSupportExpectationExpectationContextsInner
	isSet bool
}

func (v NullableEdgeServiceSupportExpectationExpectationContextsInner) Get() *EdgeServiceSupportExpectationExpectationContextsInner {
	return v.value
}

func (v *NullableEdgeServiceSupportExpectationExpectationContextsInner) Set(val *EdgeServiceSupportExpectationExpectationContextsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableEdgeServiceSupportExpectationExpectationContextsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableEdgeServiceSupportExpectationExpectationContextsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableEdgeServiceSupportExpectationExpectationContextsInner(val *EdgeServiceSupportExpectationExpectationContextsInner) *NullableEdgeServiceSupportExpectationExpectationContextsInner {
	return &NullableEdgeServiceSupportExpectationExpectationContextsInner{value: val, isSet: true}
}

func (v NullableEdgeServiceSupportExpectationExpectationContextsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableEdgeServiceSupportExpectationExpectationContextsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
