/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
)

// checks if the UdmInfo type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &UdmInfo{}

// UdmInfo struct for UdmInfo
type UdmInfo struct {
	NFSrvGroupId *string `json:"nFSrvGroupId,omitempty"`
}

// NewUdmInfo instantiates a new UdmInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewUdmInfo() *UdmInfo {
	this := UdmInfo{}
	return &this
}

// NewUdmInfoWithDefaults instantiates a new UdmInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewUdmInfoWithDefaults() *UdmInfo {
	this := UdmInfo{}
	return &this
}

// GetNFSrvGroupId returns the NFSrvGroupId field value if set, zero value otherwise.
func (o *UdmInfo) GetNFSrvGroupId() string {
	if o == nil || IsNil(o.NFSrvGroupId) {
		var ret string
		return ret
	}
	return *o.NFSrvGroupId
}

// GetNFSrvGroupIdOk returns a tuple with the NFSrvGroupId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *UdmInfo) GetNFSrvGroupIdOk() (*string, bool) {
	if o == nil || IsNil(o.NFSrvGroupId) {
		return nil, false
	}
	return o.NFSrvGroupId, true
}

// HasNFSrvGroupId returns a boolean if a field has been set.
func (o *UdmInfo) HasNFSrvGroupId() bool {
	if o != nil && !IsNil(o.NFSrvGroupId) {
		return true
	}

	return false
}

// SetNFSrvGroupId gets a reference to the given string and assigns it to the NFSrvGroupId field.
func (o *UdmInfo) SetNFSrvGroupId(v string) {
	o.NFSrvGroupId = &v
}

func (o UdmInfo) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o UdmInfo) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.NFSrvGroupId) {
		toSerialize["nFSrvGroupId"] = o.NFSrvGroupId
	}
	return toSerialize, nil
}

type NullableUdmInfo struct {
	value *UdmInfo
	isSet bool
}

func (v NullableUdmInfo) Get() *UdmInfo {
	return v.value
}

func (v *NullableUdmInfo) Set(val *UdmInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableUdmInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableUdmInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUdmInfo(val *UdmInfo) *NullableUdmInfo {
	return &NullableUdmInfo{value: val, isSet: true}
}

func (v NullableUdmInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUdmInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
