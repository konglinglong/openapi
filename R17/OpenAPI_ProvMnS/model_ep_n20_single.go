/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// checks if the EPN20Single type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &EPN20Single{}

// EPN20Single struct for EPN20Single
type EPN20Single struct {
	Top
	Attributes *EPN20SingleAllOfAttributes `json:"attributes,omitempty"`
}

type _EPN20Single EPN20Single

// NewEPN20Single instantiates a new EPN20Single object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewEPN20Single(id NullableString) *EPN20Single {
	this := EPN20Single{}
	this.Id = id
	return &this
}

// NewEPN20SingleWithDefaults instantiates a new EPN20Single object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewEPN20SingleWithDefaults() *EPN20Single {
	this := EPN20Single{}
	return &this
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *EPN20Single) GetAttributes() EPN20SingleAllOfAttributes {
	if o == nil || IsNil(o.Attributes) {
		var ret EPN20SingleAllOfAttributes
		return ret
	}
	return *o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *EPN20Single) GetAttributesOk() (*EPN20SingleAllOfAttributes, bool) {
	if o == nil || IsNil(o.Attributes) {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *EPN20Single) HasAttributes() bool {
	if o != nil && !IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given EPN20SingleAllOfAttributes and assigns it to the Attributes field.
func (o *EPN20Single) SetAttributes(v EPN20SingleAllOfAttributes) {
	o.Attributes = &v
}

func (o EPN20Single) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o EPN20Single) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedTop, errTop := json.Marshal(o.Top)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	errTop = json.Unmarshal([]byte(serializedTop), &toSerialize)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	if !IsNil(o.Attributes) {
		toSerialize["attributes"] = o.Attributes
	}
	return toSerialize, nil
}

func (o *EPN20Single) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"id",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varEPN20Single := _EPN20Single{}

	err = json.Unmarshal(bytes, &varEPN20Single)

	if err != nil {
		return err
	}

	*o = EPN20Single(varEPN20Single)

	return err
}

type NullableEPN20Single struct {
	value *EPN20Single
	isSet bool
}

func (v NullableEPN20Single) Get() *EPN20Single {
	return v.value
}

func (v *NullableEPN20Single) Set(val *EPN20Single) {
	v.value = val
	v.isSet = true
}

func (v NullableEPN20Single) IsSet() bool {
	return v.isSet
}

func (v *NullableEPN20Single) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableEPN20Single(val *EPN20Single) *NullableEPN20Single {
	return &NullableEPN20Single{value: val, isSet: true}
}

func (v NullableEPN20Single) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableEPN20Single) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
