/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// checks if the EPN6Single type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &EPN6Single{}

// EPN6Single struct for EPN6Single
type EPN6Single struct {
	Top
	Attributes *EPN6SingleAllOfAttributes `json:"attributes,omitempty"`
}

type _EPN6Single EPN6Single

// NewEPN6Single instantiates a new EPN6Single object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewEPN6Single(id NullableString) *EPN6Single {
	this := EPN6Single{}
	this.Id = id
	return &this
}

// NewEPN6SingleWithDefaults instantiates a new EPN6Single object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewEPN6SingleWithDefaults() *EPN6Single {
	this := EPN6Single{}
	return &this
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *EPN6Single) GetAttributes() EPN6SingleAllOfAttributes {
	if o == nil || IsNil(o.Attributes) {
		var ret EPN6SingleAllOfAttributes
		return ret
	}
	return *o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *EPN6Single) GetAttributesOk() (*EPN6SingleAllOfAttributes, bool) {
	if o == nil || IsNil(o.Attributes) {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *EPN6Single) HasAttributes() bool {
	if o != nil && !IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given EPN6SingleAllOfAttributes and assigns it to the Attributes field.
func (o *EPN6Single) SetAttributes(v EPN6SingleAllOfAttributes) {
	o.Attributes = &v
}

func (o EPN6Single) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o EPN6Single) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedTop, errTop := json.Marshal(o.Top)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	errTop = json.Unmarshal([]byte(serializedTop), &toSerialize)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	if !IsNil(o.Attributes) {
		toSerialize["attributes"] = o.Attributes
	}
	return toSerialize, nil
}

func (o *EPN6Single) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"id",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varEPN6Single := _EPN6Single{}

	err = json.Unmarshal(bytes, &varEPN6Single)

	if err != nil {
		return err
	}

	*o = EPN6Single(varEPN6Single)

	return err
}

type NullableEPN6Single struct {
	value *EPN6Single
	isSet bool
}

func (v NullableEPN6Single) Get() *EPN6Single {
	return v.value
}

func (v *NullableEPN6Single) Set(val *EPN6Single) {
	v.value = val
	v.isSet = true
}

func (v NullableEPN6Single) IsSet() bool {
	return v.isSet
}

func (v *NullableEPN6Single) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableEPN6Single(val *EPN6Single) *NullableEPN6Single {
	return &NullableEPN6Single{value: val, isSet: true}
}

func (v NullableEPN6Single) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableEPN6Single) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
