/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// checks if the EPN9Single type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &EPN9Single{}

// EPN9Single struct for EPN9Single
type EPN9Single struct {
	Top
	Attributes *EPN9SingleAllOfAttributes `json:"attributes,omitempty"`
}

type _EPN9Single EPN9Single

// NewEPN9Single instantiates a new EPN9Single object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewEPN9Single(id NullableString) *EPN9Single {
	this := EPN9Single{}
	this.Id = id
	return &this
}

// NewEPN9SingleWithDefaults instantiates a new EPN9Single object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewEPN9SingleWithDefaults() *EPN9Single {
	this := EPN9Single{}
	return &this
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *EPN9Single) GetAttributes() EPN9SingleAllOfAttributes {
	if o == nil || IsNil(o.Attributes) {
		var ret EPN9SingleAllOfAttributes
		return ret
	}
	return *o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *EPN9Single) GetAttributesOk() (*EPN9SingleAllOfAttributes, bool) {
	if o == nil || IsNil(o.Attributes) {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *EPN9Single) HasAttributes() bool {
	if o != nil && !IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given EPN9SingleAllOfAttributes and assigns it to the Attributes field.
func (o *EPN9Single) SetAttributes(v EPN9SingleAllOfAttributes) {
	o.Attributes = &v
}

func (o EPN9Single) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o EPN9Single) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedTop, errTop := json.Marshal(o.Top)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	errTop = json.Unmarshal([]byte(serializedTop), &toSerialize)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	if !IsNil(o.Attributes) {
		toSerialize["attributes"] = o.Attributes
	}
	return toSerialize, nil
}

func (o *EPN9Single) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"id",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varEPN9Single := _EPN9Single{}

	err = json.Unmarshal(bytes, &varEPN9Single)

	if err != nil {
		return err
	}

	*o = EPN9Single(varEPN9Single)

	return err
}

type NullableEPN9Single struct {
	value *EPN9Single
	isSet bool
}

func (v NullableEPN9Single) Get() *EPN9Single {
	return v.value
}

func (v *NullableEPN9Single) Set(val *EPN9Single) {
	v.value = val
	v.isSet = true
}

func (v NullableEPN9Single) IsSet() bool {
	return v.isSet
}

func (v *NullableEPN9Single) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableEPN9Single(val *EPN9Single) *NullableEPN9Single {
	return &NullableEPN9Single{value: val, isSet: true}
}

func (v NullableEPN9Single) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableEPN9Single) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
