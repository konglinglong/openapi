/*
Provisioning MnS

OAS 3.0.1 definition of the Provisioning MnS © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC). All rights reserved.

API version: 17.6.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_ProvMnS

import (
	"encoding/json"
	"fmt"
)

// checks if the ExternalNssfFunctionSingle type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ExternalNssfFunctionSingle{}

// ExternalNssfFunctionSingle struct for ExternalNssfFunctionSingle
type ExternalNssfFunctionSingle struct {
	Top
	Attributes *ExternalNssfFunctionSingleAllOfAttributes `json:"attributes,omitempty"`
}

type _ExternalNssfFunctionSingle ExternalNssfFunctionSingle

// NewExternalNssfFunctionSingle instantiates a new ExternalNssfFunctionSingle object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewExternalNssfFunctionSingle(id NullableString) *ExternalNssfFunctionSingle {
	this := ExternalNssfFunctionSingle{}
	this.Id = id
	return &this
}

// NewExternalNssfFunctionSingleWithDefaults instantiates a new ExternalNssfFunctionSingle object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewExternalNssfFunctionSingleWithDefaults() *ExternalNssfFunctionSingle {
	this := ExternalNssfFunctionSingle{}
	return &this
}

// GetAttributes returns the Attributes field value if set, zero value otherwise.
func (o *ExternalNssfFunctionSingle) GetAttributes() ExternalNssfFunctionSingleAllOfAttributes {
	if o == nil || IsNil(o.Attributes) {
		var ret ExternalNssfFunctionSingleAllOfAttributes
		return ret
	}
	return *o.Attributes
}

// GetAttributesOk returns a tuple with the Attributes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ExternalNssfFunctionSingle) GetAttributesOk() (*ExternalNssfFunctionSingleAllOfAttributes, bool) {
	if o == nil || IsNil(o.Attributes) {
		return nil, false
	}
	return o.Attributes, true
}

// HasAttributes returns a boolean if a field has been set.
func (o *ExternalNssfFunctionSingle) HasAttributes() bool {
	if o != nil && !IsNil(o.Attributes) {
		return true
	}

	return false
}

// SetAttributes gets a reference to the given ExternalNssfFunctionSingleAllOfAttributes and assigns it to the Attributes field.
func (o *ExternalNssfFunctionSingle) SetAttributes(v ExternalNssfFunctionSingleAllOfAttributes) {
	o.Attributes = &v
}

func (o ExternalNssfFunctionSingle) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ExternalNssfFunctionSingle) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedTop, errTop := json.Marshal(o.Top)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	errTop = json.Unmarshal([]byte(serializedTop), &toSerialize)
	if errTop != nil {
		return map[string]interface{}{}, errTop
	}
	if !IsNil(o.Attributes) {
		toSerialize["attributes"] = o.Attributes
	}
	return toSerialize, nil
}

func (o *ExternalNssfFunctionSingle) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"id",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varExternalNssfFunctionSingle := _ExternalNssfFunctionSingle{}

	err = json.Unmarshal(bytes, &varExternalNssfFunctionSingle)

	if err != nil {
		return err
	}

	*o = ExternalNssfFunctionSingle(varExternalNssfFunctionSingle)

	return err
}

type NullableExternalNssfFunctionSingle struct {
	value *ExternalNssfFunctionSingle
	isSet bool
}

func (v NullableExternalNssfFunctionSingle) Get() *ExternalNssfFunctionSingle {
	return v.value
}

func (v *NullableExternalNssfFunctionSingle) Set(val *ExternalNssfFunctionSingle) {
	v.value = val
	v.isSet = true
}

func (v NullableExternalNssfFunctionSingle) IsSet() bool {
	return v.isSet
}

func (v *NullableExternalNssfFunctionSingle) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableExternalNssfFunctionSingle(val *ExternalNssfFunctionSingle) *NullableExternalNssfFunctionSingle {
	return &NullableExternalNssfFunctionSingle{value: val, isSet: true}
}

func (v NullableExternalNssfFunctionSingle) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableExternalNssfFunctionSingle) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
