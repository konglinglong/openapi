/*
3gpp-analyticsexposure

API for Analytics Exposure.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.1.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_AnalyticsExposure

import (
	"encoding/json"
	"fmt"
)

// checks if the CongestInfo type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &CongestInfo{}

// CongestInfo Represents a UE's user data congestion information.
type CongestInfo struct {
	LocArea LocationArea5G        `json:"locArea"`
	CngAnas []CongestionAnalytics `json:"cngAnas"`
}

type _CongestInfo CongestInfo

// NewCongestInfo instantiates a new CongestInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCongestInfo(locArea LocationArea5G, cngAnas []CongestionAnalytics) *CongestInfo {
	this := CongestInfo{}
	this.LocArea = locArea
	this.CngAnas = cngAnas
	return &this
}

// NewCongestInfoWithDefaults instantiates a new CongestInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCongestInfoWithDefaults() *CongestInfo {
	this := CongestInfo{}
	return &this
}

// GetLocArea returns the LocArea field value
func (o *CongestInfo) GetLocArea() LocationArea5G {
	if o == nil {
		var ret LocationArea5G
		return ret
	}

	return o.LocArea
}

// GetLocAreaOk returns a tuple with the LocArea field value
// and a boolean to check if the value has been set.
func (o *CongestInfo) GetLocAreaOk() (*LocationArea5G, bool) {
	if o == nil {
		return nil, false
	}
	return &o.LocArea, true
}

// SetLocArea sets field value
func (o *CongestInfo) SetLocArea(v LocationArea5G) {
	o.LocArea = v
}

// GetCngAnas returns the CngAnas field value
func (o *CongestInfo) GetCngAnas() []CongestionAnalytics {
	if o == nil {
		var ret []CongestionAnalytics
		return ret
	}

	return o.CngAnas
}

// GetCngAnasOk returns a tuple with the CngAnas field value
// and a boolean to check if the value has been set.
func (o *CongestInfo) GetCngAnasOk() ([]CongestionAnalytics, bool) {
	if o == nil {
		return nil, false
	}
	return o.CngAnas, true
}

// SetCngAnas sets field value
func (o *CongestInfo) SetCngAnas(v []CongestionAnalytics) {
	o.CngAnas = v
}

func (o CongestInfo) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o CongestInfo) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["locArea"] = o.LocArea
	toSerialize["cngAnas"] = o.CngAnas
	return toSerialize, nil
}

func (o *CongestInfo) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"locArea",
		"cngAnas",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varCongestInfo := _CongestInfo{}

	err = json.Unmarshal(bytes, &varCongestInfo)

	if err != nil {
		return err
	}

	*o = CongestInfo(varCongestInfo)

	return err
}

type NullableCongestInfo struct {
	value *CongestInfo
	isSet bool
}

func (v NullableCongestInfo) Get() *CongestInfo {
	return v.value
}

func (v *NullableCongestInfo) Set(val *CongestInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableCongestInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableCongestInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCongestInfo(val *CongestInfo) *NullableCongestInfo {
	return &NullableCongestInfo{value: val, isSet: true}
}

func (v NullableCongestInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCongestInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
