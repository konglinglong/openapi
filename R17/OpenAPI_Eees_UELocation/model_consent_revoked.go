/*
EES UE Location Information_API

API for EES UE Location Information.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Eees_UELocation

import (
	"encoding/json"
	"fmt"
)

// checks if the ConsentRevoked type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ConsentRevoked{}

// ConsentRevoked Represents the information related to a revoked user consent.
type ConsentRevoked struct {
	UcPurpose UcPurpose `json:"ucPurpose"`
	// string containing a local identifier followed by \"@\" and a domain identifier. Both the local identifier and the domain identifier shall be encoded as strings that do not contain any \"@\" characters. See Clause 4.6.2 of 3GPP TS 23.682 for more information.
	ExternalId *string `json:"externalId,omitempty"`
	// String identifying a Gpsi shall contain either an External Id or an MSISDN.  It shall be formatted as follows -External Identifier= \"extid-'extid', where 'extid'  shall be formatted according to clause 19.7.2 of 3GPP TS 23.003 that describes an  External Identifier.
	UeId *string `json:"ueId,omitempty"`
}

type _ConsentRevoked ConsentRevoked

// NewConsentRevoked instantiates a new ConsentRevoked object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewConsentRevoked(ucPurpose UcPurpose) *ConsentRevoked {
	this := ConsentRevoked{}
	return &this
}

// NewConsentRevokedWithDefaults instantiates a new ConsentRevoked object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewConsentRevokedWithDefaults() *ConsentRevoked {
	this := ConsentRevoked{}
	return &this
}

// GetUcPurpose returns the UcPurpose field value
func (o *ConsentRevoked) GetUcPurpose() UcPurpose {
	if o == nil {
		var ret UcPurpose
		return ret
	}

	return o.UcPurpose
}

// GetUcPurposeOk returns a tuple with the UcPurpose field value
// and a boolean to check if the value has been set.
func (o *ConsentRevoked) GetUcPurposeOk() (*UcPurpose, bool) {
	if o == nil {
		return nil, false
	}
	return &o.UcPurpose, true
}

// SetUcPurpose sets field value
func (o *ConsentRevoked) SetUcPurpose(v UcPurpose) {
	o.UcPurpose = v
}

// GetExternalId returns the ExternalId field value if set, zero value otherwise.
func (o *ConsentRevoked) GetExternalId() string {
	if o == nil || IsNil(o.ExternalId) {
		var ret string
		return ret
	}
	return *o.ExternalId
}

// GetExternalIdOk returns a tuple with the ExternalId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConsentRevoked) GetExternalIdOk() (*string, bool) {
	if o == nil || IsNil(o.ExternalId) {
		return nil, false
	}
	return o.ExternalId, true
}

// HasExternalId returns a boolean if a field has been set.
func (o *ConsentRevoked) HasExternalId() bool {
	if o != nil && !IsNil(o.ExternalId) {
		return true
	}

	return false
}

// SetExternalId gets a reference to the given string and assigns it to the ExternalId field.
func (o *ConsentRevoked) SetExternalId(v string) {
	o.ExternalId = &v
}

// GetUeId returns the UeId field value if set, zero value otherwise.
func (o *ConsentRevoked) GetUeId() string {
	if o == nil || IsNil(o.UeId) {
		var ret string
		return ret
	}
	return *o.UeId
}

// GetUeIdOk returns a tuple with the UeId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ConsentRevoked) GetUeIdOk() (*string, bool) {
	if o == nil || IsNil(o.UeId) {
		return nil, false
	}
	return o.UeId, true
}

// HasUeId returns a boolean if a field has been set.
func (o *ConsentRevoked) HasUeId() bool {
	if o != nil && !IsNil(o.UeId) {
		return true
	}

	return false
}

// SetUeId gets a reference to the given string and assigns it to the UeId field.
func (o *ConsentRevoked) SetUeId(v string) {
	o.UeId = &v
}

func (o ConsentRevoked) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ConsentRevoked) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["ucPurpose"] = o.UcPurpose
	if !IsNil(o.ExternalId) {
		toSerialize["externalId"] = o.ExternalId
	}
	if !IsNil(o.UeId) {
		toSerialize["ueId"] = o.UeId
	}
	return toSerialize, nil
}

func (o *ConsentRevoked) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"ucPurpose",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varConsentRevoked := _ConsentRevoked{}

	err = json.Unmarshal(bytes, &varConsentRevoked)

	if err != nil {
		return err
	}

	*o = ConsentRevoked(varConsentRevoked)

	return err
}

type NullableConsentRevoked struct {
	value *ConsentRevoked
	isSet bool
}

func (v NullableConsentRevoked) Get() *ConsentRevoked {
	return v.value
}

func (v *NullableConsentRevoked) Set(val *ConsentRevoked) {
	v.value = val
	v.isSet = true
}

func (v NullableConsentRevoked) IsSet() bool {
	return v.isSet
}

func (v *NullableConsentRevoked) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableConsentRevoked(val *ConsentRevoked) *NullableConsentRevoked {
	return &NullableConsentRevoked{value: val, isSet: true}
}

func (v NullableConsentRevoked) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableConsentRevoked) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
