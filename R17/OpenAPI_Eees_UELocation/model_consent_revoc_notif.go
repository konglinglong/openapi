/*
EES UE Location Information_API

API for EES UE Location Information.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Eees_UELocation

import (
	"encoding/json"
	"fmt"
)

// checks if the ConsentRevocNotif type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ConsentRevocNotif{}

// ConsentRevocNotif Represents the user consent revocation information conveyed in a user consent revocation notification.
type ConsentRevocNotif struct {
	SubscriptionId  string           `json:"subscriptionId"`
	ConsentsRevoked []ConsentRevoked `json:"consentsRevoked"`
}

type _ConsentRevocNotif ConsentRevocNotif

// NewConsentRevocNotif instantiates a new ConsentRevocNotif object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewConsentRevocNotif(subscriptionId string, consentsRevoked []ConsentRevoked) *ConsentRevocNotif {
	this := ConsentRevocNotif{}
	this.SubscriptionId = subscriptionId
	this.ConsentsRevoked = consentsRevoked
	return &this
}

// NewConsentRevocNotifWithDefaults instantiates a new ConsentRevocNotif object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewConsentRevocNotifWithDefaults() *ConsentRevocNotif {
	this := ConsentRevocNotif{}
	return &this
}

// GetSubscriptionId returns the SubscriptionId field value
func (o *ConsentRevocNotif) GetSubscriptionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.SubscriptionId
}

// GetSubscriptionIdOk returns a tuple with the SubscriptionId field value
// and a boolean to check if the value has been set.
func (o *ConsentRevocNotif) GetSubscriptionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SubscriptionId, true
}

// SetSubscriptionId sets field value
func (o *ConsentRevocNotif) SetSubscriptionId(v string) {
	o.SubscriptionId = v
}

// GetConsentsRevoked returns the ConsentsRevoked field value
func (o *ConsentRevocNotif) GetConsentsRevoked() []ConsentRevoked {
	if o == nil {
		var ret []ConsentRevoked
		return ret
	}

	return o.ConsentsRevoked
}

// GetConsentsRevokedOk returns a tuple with the ConsentsRevoked field value
// and a boolean to check if the value has been set.
func (o *ConsentRevocNotif) GetConsentsRevokedOk() ([]ConsentRevoked, bool) {
	if o == nil {
		return nil, false
	}
	return o.ConsentsRevoked, true
}

// SetConsentsRevoked sets field value
func (o *ConsentRevocNotif) SetConsentsRevoked(v []ConsentRevoked) {
	o.ConsentsRevoked = v
}

func (o ConsentRevocNotif) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ConsentRevocNotif) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["subscriptionId"] = o.SubscriptionId
	toSerialize["consentsRevoked"] = o.ConsentsRevoked
	return toSerialize, nil
}

func (o *ConsentRevocNotif) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"subscriptionId",
		"consentsRevoked",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varConsentRevocNotif := _ConsentRevocNotif{}

	err = json.Unmarshal(bytes, &varConsentRevocNotif)

	if err != nil {
		return err
	}

	*o = ConsentRevocNotif(varConsentRevocNotif)

	return err
}

type NullableConsentRevocNotif struct {
	value *ConsentRevocNotif
	isSet bool
}

func (v NullableConsentRevocNotif) Get() *ConsentRevocNotif {
	return v.value
}

func (v *NullableConsentRevocNotif) Set(val *ConsentRevocNotif) {
	v.value = val
	v.isSet = true
}

func (v NullableConsentRevocNotif) IsSet() bool {
	return v.isSet
}

func (v *NullableConsentRevocNotif) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableConsentRevocNotif(val *ConsentRevocNotif) *NullableConsentRevocNotif {
	return &NullableConsentRevocNotif{value: val, isSet: true}
}

func (v NullableConsentRevocNotif) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableConsentRevocNotif) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
