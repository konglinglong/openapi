/*
3gpp-data-reporting

API for 3GPP Data Reporting.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_DataReporting

import (
	"encoding/json"
	"fmt"
)

// checks if the DataReportingSessionReportingConditionsInner type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &DataReportingSessionReportingConditionsInner{}

// DataReportingSessionReportingConditionsInner struct for DataReportingSessionReportingConditionsInner
type DataReportingSessionReportingConditionsInner struct {
	DataDomain DataDomain           `json:"dataDomain"`
	Conditions []ReportingCondition `json:"conditions"`
}

type _DataReportingSessionReportingConditionsInner DataReportingSessionReportingConditionsInner

// NewDataReportingSessionReportingConditionsInner instantiates a new DataReportingSessionReportingConditionsInner object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewDataReportingSessionReportingConditionsInner(dataDomain DataDomain, conditions []ReportingCondition) *DataReportingSessionReportingConditionsInner {
	this := DataReportingSessionReportingConditionsInner{}
	this.DataDomain = dataDomain
	this.Conditions = conditions
	return &this
}

// NewDataReportingSessionReportingConditionsInnerWithDefaults instantiates a new DataReportingSessionReportingConditionsInner object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewDataReportingSessionReportingConditionsInnerWithDefaults() *DataReportingSessionReportingConditionsInner {
	this := DataReportingSessionReportingConditionsInner{}
	return &this
}

// GetDataDomain returns the DataDomain field value
func (o *DataReportingSessionReportingConditionsInner) GetDataDomain() DataDomain {
	if o == nil {
		var ret DataDomain
		return ret
	}

	return o.DataDomain
}

// GetDataDomainOk returns a tuple with the DataDomain field value
// and a boolean to check if the value has been set.
func (o *DataReportingSessionReportingConditionsInner) GetDataDomainOk() (*DataDomain, bool) {
	if o == nil {
		return nil, false
	}
	return &o.DataDomain, true
}

// SetDataDomain sets field value
func (o *DataReportingSessionReportingConditionsInner) SetDataDomain(v DataDomain) {
	o.DataDomain = v
}

// GetConditions returns the Conditions field value
func (o *DataReportingSessionReportingConditionsInner) GetConditions() []ReportingCondition {
	if o == nil {
		var ret []ReportingCondition
		return ret
	}

	return o.Conditions
}

// GetConditionsOk returns a tuple with the Conditions field value
// and a boolean to check if the value has been set.
func (o *DataReportingSessionReportingConditionsInner) GetConditionsOk() ([]ReportingCondition, bool) {
	if o == nil {
		return nil, false
	}
	return o.Conditions, true
}

// SetConditions sets field value
func (o *DataReportingSessionReportingConditionsInner) SetConditions(v []ReportingCondition) {
	o.Conditions = v
}

func (o DataReportingSessionReportingConditionsInner) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o DataReportingSessionReportingConditionsInner) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["dataDomain"] = o.DataDomain
	toSerialize["conditions"] = o.Conditions
	return toSerialize, nil
}

func (o *DataReportingSessionReportingConditionsInner) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"dataDomain",
		"conditions",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varDataReportingSessionReportingConditionsInner := _DataReportingSessionReportingConditionsInner{}

	err = json.Unmarshal(bytes, &varDataReportingSessionReportingConditionsInner)

	if err != nil {
		return err
	}

	*o = DataReportingSessionReportingConditionsInner(varDataReportingSessionReportingConditionsInner)

	return err
}

type NullableDataReportingSessionReportingConditionsInner struct {
	value *DataReportingSessionReportingConditionsInner
	isSet bool
}

func (v NullableDataReportingSessionReportingConditionsInner) Get() *DataReportingSessionReportingConditionsInner {
	return v.value
}

func (v *NullableDataReportingSessionReportingConditionsInner) Set(val *DataReportingSessionReportingConditionsInner) {
	v.value = val
	v.isSet = true
}

func (v NullableDataReportingSessionReportingConditionsInner) IsSet() bool {
	return v.isSet
}

func (v *NullableDataReportingSessionReportingConditionsInner) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableDataReportingSessionReportingConditionsInner(val *DataReportingSessionReportingConditionsInner) *NullableDataReportingSessionReportingConditionsInner {
	return &NullableDataReportingSessionReportingConditionsInner{value: val, isSet: true}
}

func (v NullableDataReportingSessionReportingConditionsInner) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableDataReportingSessionReportingConditionsInner) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
