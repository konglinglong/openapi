/*
3gpp-data-reporting

API for 3GPP Data Reporting.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_DataReporting

import (
	"encoding/json"
	"fmt"
	"time"
)

// checks if the LocationRecord type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &LocationRecord{}

// LocationRecord A data reporting record for UE location.
type LocationRecord struct {
	BaseRecord
	Location LocationData `json:"location"`
}

type _LocationRecord LocationRecord

// NewLocationRecord instantiates a new LocationRecord object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLocationRecord(location LocationData, timestamp time.Time) *LocationRecord {
	this := LocationRecord{}
	this.Timestamp = timestamp
	this.Location = location
	return &this
}

// NewLocationRecordWithDefaults instantiates a new LocationRecord object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLocationRecordWithDefaults() *LocationRecord {
	this := LocationRecord{}
	return &this
}

// GetLocation returns the Location field value
func (o *LocationRecord) GetLocation() LocationData {
	if o == nil {
		var ret LocationData
		return ret
	}

	return o.Location
}

// GetLocationOk returns a tuple with the Location field value
// and a boolean to check if the value has been set.
func (o *LocationRecord) GetLocationOk() (*LocationData, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Location, true
}

// SetLocation sets field value
func (o *LocationRecord) SetLocation(v LocationData) {
	o.Location = v
}

func (o LocationRecord) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o LocationRecord) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	serializedBaseRecord, errBaseRecord := json.Marshal(o.BaseRecord)
	if errBaseRecord != nil {
		return map[string]interface{}{}, errBaseRecord
	}
	errBaseRecord = json.Unmarshal([]byte(serializedBaseRecord), &toSerialize)
	if errBaseRecord != nil {
		return map[string]interface{}{}, errBaseRecord
	}
	toSerialize["location"] = o.Location
	return toSerialize, nil
}

func (o *LocationRecord) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"location",
		"timestamp",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varLocationRecord := _LocationRecord{}

	err = json.Unmarshal(bytes, &varLocationRecord)

	if err != nil {
		return err
	}

	*o = LocationRecord(varLocationRecord)

	return err
}

type NullableLocationRecord struct {
	value *LocationRecord
	isSet bool
}

func (v NullableLocationRecord) Get() *LocationRecord {
	return v.value
}

func (v *NullableLocationRecord) Set(val *LocationRecord) {
	v.value = val
	v.isSet = true
}

func (v NullableLocationRecord) IsSet() bool {
	return v.isSet
}

func (v *NullableLocationRecord) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLocationRecord(val *LocationRecord) *NullableLocationRecord {
	return &NullableLocationRecord{value: val, isSet: true}
}

func (v NullableLocationRecord) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLocationRecord) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
