/*
3gpp-network-parameter-configuration

API for network parameter configuration.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_NpConfiguration

import (
	"encoding/json"
	"time"
)

// checks if the NpConfiguration type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &NpConfiguration{}

// NpConfiguration Represents a network parameters configuration.
type NpConfiguration struct {
	// string formatted according to IETF RFC 3986 identifying a referenced resource.
	Self *string `json:"self,omitempty"`
	// A string used to indicate the features supported by an API that is used as defined in clause  6.6 in 3GPP TS 29.500. The string shall contain a bitmask indicating supported features in  hexadecimal representation Each character in the string shall take a value of \"0\" to \"9\",  \"a\" to \"f\" or \"A\" to \"F\" and shall represent the support of 4 features as described in  table 5.2.2-3. The most significant character representing the highest-numbered features shall  appear first in the string, and the character representing features 1 to 4 shall appear last  in the string. The list of features and their numbering (starting with 1) are defined  separately for each API. If the string contains a lower number of characters than there are  defined features for an API, all features that would be represented by characters that are not  present in the string are not supported.
	SupportedFeatures *string `json:"supportedFeatures,omitempty"`
	// Identifies the MTC Service Provider and/or MTC Application.
	MtcProviderId *string `json:"mtcProviderId,omitempty"`
	// String representing a Data Network as defined in clause 9A of 3GPP TS 23.003;  it shall contain either a DNN Network Identifier, or a full DNN with both the Network  Identifier and Operator Identifier, as specified in 3GPP TS 23.003 clause 9.1.1 and 9.1.2. It shall be coded as string in which the labels are separated by dots  (e.g. \"Label1.Label2.Label3\").
	Dnn *string `json:"dnn,omitempty"`
	// string containing a local identifier followed by \"@\" and a domain identifier. Both the local identifier and the domain identifier shall be encoded as strings that do not contain any \"@\" characters. See Clause 4.6.2 of 3GPP TS 23.682 for more information.
	ExternalId *string `json:"externalId,omitempty"`
	// string formatted according to clause 3.3 of 3GPP TS 23.003 that describes an MSISDN.
	Msisdn *string `json:"msisdn,omitempty"`
	// string containing a local identifier followed by \"@\" and a domain identifier. Both the local identifier and the domain identifier shall be encoded as strings that do not contain any \"@\" characters. See Clauses 4.6.2 and 4.6.3 of 3GPP TS 23.682 for more information.
	ExternalGroupId *string `json:"externalGroupId,omitempty"`
	// Unsigned integer identifying a period of time in units of seconds.
	MaximumLatency *int32 `json:"maximumLatency,omitempty"`
	// Unsigned integer identifying a period of time in units of seconds.
	MaximumResponseTime *int32 `json:"maximumResponseTime,omitempty"`
	// This parameter may be included to identify the number of packets that the serving gateway shall buffer in case that the UE is not reachable.
	SuggestedNumberOfDlPackets *int32 `json:"suggestedNumberOfDlPackets,omitempty"`
	// Unsigned integer identifying a period of time in units of seconds.
	GroupReportingGuardTime *int32 `json:"groupReportingGuardTime,omitempty"`
	// string formatted according to IETF RFC 3986 identifying a referenced resource.
	NotificationDestination *string `json:"notificationDestination,omitempty"`
	// Set to true by the SCS/AS to request the SCEF to send a test notification as defined in clause 5.2.5.3. Set to false or omitted otherwise.
	RequestTestNotification *bool               `json:"requestTestNotification,omitempty"`
	WebsockNotifConfig      *WebsockNotifConfig `json:"websockNotifConfig,omitempty"`
	// string with format \"date-time\" as defined in OpenAPI.
	ValidityTime *time.Time     `json:"validityTime,omitempty"`
	Snssai       *Snssai        `json:"snssai,omitempty"`
	UeIpAddr     NullableIpAddr `json:"ueIpAddr,omitempty"`
	// String identifying a MAC address formatted in the hexadecimal notation according to clause 1.1 and clause 2.1 of RFC 7042.
	UeMacAddr *string `json:"ueMacAddr,omitempty"`
}

// NewNpConfiguration instantiates a new NpConfiguration object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNpConfiguration() *NpConfiguration {
	this := NpConfiguration{}
	return &this
}

// NewNpConfigurationWithDefaults instantiates a new NpConfiguration object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNpConfigurationWithDefaults() *NpConfiguration {
	this := NpConfiguration{}
	return &this
}

// GetSelf returns the Self field value if set, zero value otherwise.
func (o *NpConfiguration) GetSelf() string {
	if o == nil || IsNil(o.Self) {
		var ret string
		return ret
	}
	return *o.Self
}

// GetSelfOk returns a tuple with the Self field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetSelfOk() (*string, bool) {
	if o == nil || IsNil(o.Self) {
		return nil, false
	}
	return o.Self, true
}

// HasSelf returns a boolean if a field has been set.
func (o *NpConfiguration) HasSelf() bool {
	if o != nil && !IsNil(o.Self) {
		return true
	}

	return false
}

// SetSelf gets a reference to the given string and assigns it to the Self field.
func (o *NpConfiguration) SetSelf(v string) {
	o.Self = &v
}

// GetSupportedFeatures returns the SupportedFeatures field value if set, zero value otherwise.
func (o *NpConfiguration) GetSupportedFeatures() string {
	if o == nil || IsNil(o.SupportedFeatures) {
		var ret string
		return ret
	}
	return *o.SupportedFeatures
}

// GetSupportedFeaturesOk returns a tuple with the SupportedFeatures field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetSupportedFeaturesOk() (*string, bool) {
	if o == nil || IsNil(o.SupportedFeatures) {
		return nil, false
	}
	return o.SupportedFeatures, true
}

// HasSupportedFeatures returns a boolean if a field has been set.
func (o *NpConfiguration) HasSupportedFeatures() bool {
	if o != nil && !IsNil(o.SupportedFeatures) {
		return true
	}

	return false
}

// SetSupportedFeatures gets a reference to the given string and assigns it to the SupportedFeatures field.
func (o *NpConfiguration) SetSupportedFeatures(v string) {
	o.SupportedFeatures = &v
}

// GetMtcProviderId returns the MtcProviderId field value if set, zero value otherwise.
func (o *NpConfiguration) GetMtcProviderId() string {
	if o == nil || IsNil(o.MtcProviderId) {
		var ret string
		return ret
	}
	return *o.MtcProviderId
}

// GetMtcProviderIdOk returns a tuple with the MtcProviderId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetMtcProviderIdOk() (*string, bool) {
	if o == nil || IsNil(o.MtcProviderId) {
		return nil, false
	}
	return o.MtcProviderId, true
}

// HasMtcProviderId returns a boolean if a field has been set.
func (o *NpConfiguration) HasMtcProviderId() bool {
	if o != nil && !IsNil(o.MtcProviderId) {
		return true
	}

	return false
}

// SetMtcProviderId gets a reference to the given string and assigns it to the MtcProviderId field.
func (o *NpConfiguration) SetMtcProviderId(v string) {
	o.MtcProviderId = &v
}

// GetDnn returns the Dnn field value if set, zero value otherwise.
func (o *NpConfiguration) GetDnn() string {
	if o == nil || IsNil(o.Dnn) {
		var ret string
		return ret
	}
	return *o.Dnn
}

// GetDnnOk returns a tuple with the Dnn field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetDnnOk() (*string, bool) {
	if o == nil || IsNil(o.Dnn) {
		return nil, false
	}
	return o.Dnn, true
}

// HasDnn returns a boolean if a field has been set.
func (o *NpConfiguration) HasDnn() bool {
	if o != nil && !IsNil(o.Dnn) {
		return true
	}

	return false
}

// SetDnn gets a reference to the given string and assigns it to the Dnn field.
func (o *NpConfiguration) SetDnn(v string) {
	o.Dnn = &v
}

// GetExternalId returns the ExternalId field value if set, zero value otherwise.
func (o *NpConfiguration) GetExternalId() string {
	if o == nil || IsNil(o.ExternalId) {
		var ret string
		return ret
	}
	return *o.ExternalId
}

// GetExternalIdOk returns a tuple with the ExternalId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetExternalIdOk() (*string, bool) {
	if o == nil || IsNil(o.ExternalId) {
		return nil, false
	}
	return o.ExternalId, true
}

// HasExternalId returns a boolean if a field has been set.
func (o *NpConfiguration) HasExternalId() bool {
	if o != nil && !IsNil(o.ExternalId) {
		return true
	}

	return false
}

// SetExternalId gets a reference to the given string and assigns it to the ExternalId field.
func (o *NpConfiguration) SetExternalId(v string) {
	o.ExternalId = &v
}

// GetMsisdn returns the Msisdn field value if set, zero value otherwise.
func (o *NpConfiguration) GetMsisdn() string {
	if o == nil || IsNil(o.Msisdn) {
		var ret string
		return ret
	}
	return *o.Msisdn
}

// GetMsisdnOk returns a tuple with the Msisdn field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetMsisdnOk() (*string, bool) {
	if o == nil || IsNil(o.Msisdn) {
		return nil, false
	}
	return o.Msisdn, true
}

// HasMsisdn returns a boolean if a field has been set.
func (o *NpConfiguration) HasMsisdn() bool {
	if o != nil && !IsNil(o.Msisdn) {
		return true
	}

	return false
}

// SetMsisdn gets a reference to the given string and assigns it to the Msisdn field.
func (o *NpConfiguration) SetMsisdn(v string) {
	o.Msisdn = &v
}

// GetExternalGroupId returns the ExternalGroupId field value if set, zero value otherwise.
func (o *NpConfiguration) GetExternalGroupId() string {
	if o == nil || IsNil(o.ExternalGroupId) {
		var ret string
		return ret
	}
	return *o.ExternalGroupId
}

// GetExternalGroupIdOk returns a tuple with the ExternalGroupId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetExternalGroupIdOk() (*string, bool) {
	if o == nil || IsNil(o.ExternalGroupId) {
		return nil, false
	}
	return o.ExternalGroupId, true
}

// HasExternalGroupId returns a boolean if a field has been set.
func (o *NpConfiguration) HasExternalGroupId() bool {
	if o != nil && !IsNil(o.ExternalGroupId) {
		return true
	}

	return false
}

// SetExternalGroupId gets a reference to the given string and assigns it to the ExternalGroupId field.
func (o *NpConfiguration) SetExternalGroupId(v string) {
	o.ExternalGroupId = &v
}

// GetMaximumLatency returns the MaximumLatency field value if set, zero value otherwise.
func (o *NpConfiguration) GetMaximumLatency() int32 {
	if o == nil || IsNil(o.MaximumLatency) {
		var ret int32
		return ret
	}
	return *o.MaximumLatency
}

// GetMaximumLatencyOk returns a tuple with the MaximumLatency field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetMaximumLatencyOk() (*int32, bool) {
	if o == nil || IsNil(o.MaximumLatency) {
		return nil, false
	}
	return o.MaximumLatency, true
}

// HasMaximumLatency returns a boolean if a field has been set.
func (o *NpConfiguration) HasMaximumLatency() bool {
	if o != nil && !IsNil(o.MaximumLatency) {
		return true
	}

	return false
}

// SetMaximumLatency gets a reference to the given int32 and assigns it to the MaximumLatency field.
func (o *NpConfiguration) SetMaximumLatency(v int32) {
	o.MaximumLatency = &v
}

// GetMaximumResponseTime returns the MaximumResponseTime field value if set, zero value otherwise.
func (o *NpConfiguration) GetMaximumResponseTime() int32 {
	if o == nil || IsNil(o.MaximumResponseTime) {
		var ret int32
		return ret
	}
	return *o.MaximumResponseTime
}

// GetMaximumResponseTimeOk returns a tuple with the MaximumResponseTime field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetMaximumResponseTimeOk() (*int32, bool) {
	if o == nil || IsNil(o.MaximumResponseTime) {
		return nil, false
	}
	return o.MaximumResponseTime, true
}

// HasMaximumResponseTime returns a boolean if a field has been set.
func (o *NpConfiguration) HasMaximumResponseTime() bool {
	if o != nil && !IsNil(o.MaximumResponseTime) {
		return true
	}

	return false
}

// SetMaximumResponseTime gets a reference to the given int32 and assigns it to the MaximumResponseTime field.
func (o *NpConfiguration) SetMaximumResponseTime(v int32) {
	o.MaximumResponseTime = &v
}

// GetSuggestedNumberOfDlPackets returns the SuggestedNumberOfDlPackets field value if set, zero value otherwise.
func (o *NpConfiguration) GetSuggestedNumberOfDlPackets() int32 {
	if o == nil || IsNil(o.SuggestedNumberOfDlPackets) {
		var ret int32
		return ret
	}
	return *o.SuggestedNumberOfDlPackets
}

// GetSuggestedNumberOfDlPacketsOk returns a tuple with the SuggestedNumberOfDlPackets field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetSuggestedNumberOfDlPacketsOk() (*int32, bool) {
	if o == nil || IsNil(o.SuggestedNumberOfDlPackets) {
		return nil, false
	}
	return o.SuggestedNumberOfDlPackets, true
}

// HasSuggestedNumberOfDlPackets returns a boolean if a field has been set.
func (o *NpConfiguration) HasSuggestedNumberOfDlPackets() bool {
	if o != nil && !IsNil(o.SuggestedNumberOfDlPackets) {
		return true
	}

	return false
}

// SetSuggestedNumberOfDlPackets gets a reference to the given int32 and assigns it to the SuggestedNumberOfDlPackets field.
func (o *NpConfiguration) SetSuggestedNumberOfDlPackets(v int32) {
	o.SuggestedNumberOfDlPackets = &v
}

// GetGroupReportingGuardTime returns the GroupReportingGuardTime field value if set, zero value otherwise.
func (o *NpConfiguration) GetGroupReportingGuardTime() int32 {
	if o == nil || IsNil(o.GroupReportingGuardTime) {
		var ret int32
		return ret
	}
	return *o.GroupReportingGuardTime
}

// GetGroupReportingGuardTimeOk returns a tuple with the GroupReportingGuardTime field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetGroupReportingGuardTimeOk() (*int32, bool) {
	if o == nil || IsNil(o.GroupReportingGuardTime) {
		return nil, false
	}
	return o.GroupReportingGuardTime, true
}

// HasGroupReportingGuardTime returns a boolean if a field has been set.
func (o *NpConfiguration) HasGroupReportingGuardTime() bool {
	if o != nil && !IsNil(o.GroupReportingGuardTime) {
		return true
	}

	return false
}

// SetGroupReportingGuardTime gets a reference to the given int32 and assigns it to the GroupReportingGuardTime field.
func (o *NpConfiguration) SetGroupReportingGuardTime(v int32) {
	o.GroupReportingGuardTime = &v
}

// GetNotificationDestination returns the NotificationDestination field value if set, zero value otherwise.
func (o *NpConfiguration) GetNotificationDestination() string {
	if o == nil || IsNil(o.NotificationDestination) {
		var ret string
		return ret
	}
	return *o.NotificationDestination
}

// GetNotificationDestinationOk returns a tuple with the NotificationDestination field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetNotificationDestinationOk() (*string, bool) {
	if o == nil || IsNil(o.NotificationDestination) {
		return nil, false
	}
	return o.NotificationDestination, true
}

// HasNotificationDestination returns a boolean if a field has been set.
func (o *NpConfiguration) HasNotificationDestination() bool {
	if o != nil && !IsNil(o.NotificationDestination) {
		return true
	}

	return false
}

// SetNotificationDestination gets a reference to the given string and assigns it to the NotificationDestination field.
func (o *NpConfiguration) SetNotificationDestination(v string) {
	o.NotificationDestination = &v
}

// GetRequestTestNotification returns the RequestTestNotification field value if set, zero value otherwise.
func (o *NpConfiguration) GetRequestTestNotification() bool {
	if o == nil || IsNil(o.RequestTestNotification) {
		var ret bool
		return ret
	}
	return *o.RequestTestNotification
}

// GetRequestTestNotificationOk returns a tuple with the RequestTestNotification field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetRequestTestNotificationOk() (*bool, bool) {
	if o == nil || IsNil(o.RequestTestNotification) {
		return nil, false
	}
	return o.RequestTestNotification, true
}

// HasRequestTestNotification returns a boolean if a field has been set.
func (o *NpConfiguration) HasRequestTestNotification() bool {
	if o != nil && !IsNil(o.RequestTestNotification) {
		return true
	}

	return false
}

// SetRequestTestNotification gets a reference to the given bool and assigns it to the RequestTestNotification field.
func (o *NpConfiguration) SetRequestTestNotification(v bool) {
	o.RequestTestNotification = &v
}

// GetWebsockNotifConfig returns the WebsockNotifConfig field value if set, zero value otherwise.
func (o *NpConfiguration) GetWebsockNotifConfig() WebsockNotifConfig {
	if o == nil || IsNil(o.WebsockNotifConfig) {
		var ret WebsockNotifConfig
		return ret
	}
	return *o.WebsockNotifConfig
}

// GetWebsockNotifConfigOk returns a tuple with the WebsockNotifConfig field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetWebsockNotifConfigOk() (*WebsockNotifConfig, bool) {
	if o == nil || IsNil(o.WebsockNotifConfig) {
		return nil, false
	}
	return o.WebsockNotifConfig, true
}

// HasWebsockNotifConfig returns a boolean if a field has been set.
func (o *NpConfiguration) HasWebsockNotifConfig() bool {
	if o != nil && !IsNil(o.WebsockNotifConfig) {
		return true
	}

	return false
}

// SetWebsockNotifConfig gets a reference to the given WebsockNotifConfig and assigns it to the WebsockNotifConfig field.
func (o *NpConfiguration) SetWebsockNotifConfig(v WebsockNotifConfig) {
	o.WebsockNotifConfig = &v
}

// GetValidityTime returns the ValidityTime field value if set, zero value otherwise.
func (o *NpConfiguration) GetValidityTime() time.Time {
	if o == nil || IsNil(o.ValidityTime) {
		var ret time.Time
		return ret
	}
	return *o.ValidityTime
}

// GetValidityTimeOk returns a tuple with the ValidityTime field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetValidityTimeOk() (*time.Time, bool) {
	if o == nil || IsNil(o.ValidityTime) {
		return nil, false
	}
	return o.ValidityTime, true
}

// HasValidityTime returns a boolean if a field has been set.
func (o *NpConfiguration) HasValidityTime() bool {
	if o != nil && !IsNil(o.ValidityTime) {
		return true
	}

	return false
}

// SetValidityTime gets a reference to the given time.Time and assigns it to the ValidityTime field.
func (o *NpConfiguration) SetValidityTime(v time.Time) {
	o.ValidityTime = &v
}

// GetSnssai returns the Snssai field value if set, zero value otherwise.
func (o *NpConfiguration) GetSnssai() Snssai {
	if o == nil || IsNil(o.Snssai) {
		var ret Snssai
		return ret
	}
	return *o.Snssai
}

// GetSnssaiOk returns a tuple with the Snssai field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetSnssaiOk() (*Snssai, bool) {
	if o == nil || IsNil(o.Snssai) {
		return nil, false
	}
	return o.Snssai, true
}

// HasSnssai returns a boolean if a field has been set.
func (o *NpConfiguration) HasSnssai() bool {
	if o != nil && !IsNil(o.Snssai) {
		return true
	}

	return false
}

// SetSnssai gets a reference to the given Snssai and assigns it to the Snssai field.
func (o *NpConfiguration) SetSnssai(v Snssai) {
	o.Snssai = &v
}

// GetUeIpAddr returns the UeIpAddr field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *NpConfiguration) GetUeIpAddr() IpAddr {
	if o == nil || IsNil(o.UeIpAddr.Get()) {
		var ret IpAddr
		return ret
	}
	return *o.UeIpAddr.Get()
}

// GetUeIpAddrOk returns a tuple with the UeIpAddr field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *NpConfiguration) GetUeIpAddrOk() (*IpAddr, bool) {
	if o == nil {
		return nil, false
	}
	return o.UeIpAddr.Get(), o.UeIpAddr.IsSet()
}

// HasUeIpAddr returns a boolean if a field has been set.
func (o *NpConfiguration) HasUeIpAddr() bool {
	if o != nil && o.UeIpAddr.IsSet() {
		return true
	}

	return false
}

// SetUeIpAddr gets a reference to the given NullableIpAddr and assigns it to the UeIpAddr field.
func (o *NpConfiguration) SetUeIpAddr(v IpAddr) {
	o.UeIpAddr.Set(&v)
}

// SetUeIpAddrNil sets the value for UeIpAddr to be an explicit nil
func (o *NpConfiguration) SetUeIpAddrNil() {
	o.UeIpAddr.Set(nil)
}

// UnsetUeIpAddr ensures that no value is present for UeIpAddr, not even an explicit nil
func (o *NpConfiguration) UnsetUeIpAddr() {
	o.UeIpAddr.Unset()
}

// GetUeMacAddr returns the UeMacAddr field value if set, zero value otherwise.
func (o *NpConfiguration) GetUeMacAddr() string {
	if o == nil || IsNil(o.UeMacAddr) {
		var ret string
		return ret
	}
	return *o.UeMacAddr
}

// GetUeMacAddrOk returns a tuple with the UeMacAddr field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NpConfiguration) GetUeMacAddrOk() (*string, bool) {
	if o == nil || IsNil(o.UeMacAddr) {
		return nil, false
	}
	return o.UeMacAddr, true
}

// HasUeMacAddr returns a boolean if a field has been set.
func (o *NpConfiguration) HasUeMacAddr() bool {
	if o != nil && !IsNil(o.UeMacAddr) {
		return true
	}

	return false
}

// SetUeMacAddr gets a reference to the given string and assigns it to the UeMacAddr field.
func (o *NpConfiguration) SetUeMacAddr(v string) {
	o.UeMacAddr = &v
}

func (o NpConfiguration) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o NpConfiguration) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if !IsNil(o.Self) {
		toSerialize["self"] = o.Self
	}
	if !IsNil(o.SupportedFeatures) {
		toSerialize["supportedFeatures"] = o.SupportedFeatures
	}
	if !IsNil(o.MtcProviderId) {
		toSerialize["mtcProviderId"] = o.MtcProviderId
	}
	if !IsNil(o.Dnn) {
		toSerialize["dnn"] = o.Dnn
	}
	if !IsNil(o.ExternalId) {
		toSerialize["externalId"] = o.ExternalId
	}
	if !IsNil(o.Msisdn) {
		toSerialize["msisdn"] = o.Msisdn
	}
	if !IsNil(o.ExternalGroupId) {
		toSerialize["externalGroupId"] = o.ExternalGroupId
	}
	if !IsNil(o.MaximumLatency) {
		toSerialize["maximumLatency"] = o.MaximumLatency
	}
	if !IsNil(o.MaximumResponseTime) {
		toSerialize["maximumResponseTime"] = o.MaximumResponseTime
	}
	if !IsNil(o.SuggestedNumberOfDlPackets) {
		toSerialize["suggestedNumberOfDlPackets"] = o.SuggestedNumberOfDlPackets
	}
	if !IsNil(o.GroupReportingGuardTime) {
		toSerialize["groupReportingGuardTime"] = o.GroupReportingGuardTime
	}
	if !IsNil(o.NotificationDestination) {
		toSerialize["notificationDestination"] = o.NotificationDestination
	}
	if !IsNil(o.RequestTestNotification) {
		toSerialize["requestTestNotification"] = o.RequestTestNotification
	}
	if !IsNil(o.WebsockNotifConfig) {
		toSerialize["websockNotifConfig"] = o.WebsockNotifConfig
	}
	if !IsNil(o.ValidityTime) {
		toSerialize["validityTime"] = o.ValidityTime
	}
	if !IsNil(o.Snssai) {
		toSerialize["snssai"] = o.Snssai
	}
	if o.UeIpAddr.IsSet() {
		toSerialize["ueIpAddr"] = o.UeIpAddr.Get()
	}
	if !IsNil(o.UeMacAddr) {
		toSerialize["ueMacAddr"] = o.UeMacAddr
	}
	return toSerialize, nil
}

type NullableNpConfiguration struct {
	value *NpConfiguration
	isSet bool
}

func (v NullableNpConfiguration) Get() *NpConfiguration {
	return v.value
}

func (v *NullableNpConfiguration) Set(val *NpConfiguration) {
	v.value = val
	v.isSet = true
}

func (v NullableNpConfiguration) IsSet() bool {
	return v.isSet
}

func (v *NullableNpConfiguration) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNpConfiguration(val *NpConfiguration) *NullableNpConfiguration {
	return &NullableNpConfiguration{value: val, isSet: true}
}

func (v NullableNpConfiguration) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNpConfiguration) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
