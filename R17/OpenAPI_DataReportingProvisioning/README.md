# Go API client for OpenAPI_DataReportingProvisioning

API for 3GPP Data Reporting and Provisioning.  
© 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).  
All rights reserved.


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0.1
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```sh
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```go
import OpenAPI_DataReportingProvisioning "gitee.com/konglinglong/openapi/OpenAPI_DataReportingProvisioning"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```go
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `OpenAPI_DataReportingProvisioning.ContextServerIndex` of type `int`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_DataReportingProvisioning.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `OpenAPI_DataReportingProvisioning.ContextServerVariables` of type `map[string]string`.

```go
ctx := context.WithValue(context.Background(), OpenAPI_DataReportingProvisioning.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identified by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `OpenAPI_DataReportingProvisioning.ContextOperationServerIndices` and `OpenAPI_DataReportingProvisioning.ContextOperationServerVariables` context maps.

```go
ctx := context.WithValue(context.Background(), OpenAPI_DataReportingProvisioning.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), OpenAPI_DataReportingProvisioning.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *https://example.com/3gpp-data-reporting-provisioning/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DataReportingConfigurationsAPI* | [**CreateDataRepConfig**](docs/DataReportingConfigurationsAPI.md#createdatarepconfig) | **Post** /sessions/{sessionId}/configurations | Create a new Data Reporting Configuration resource.
*DataReportingProvisioningSessionsAPI* | [**CreateDataRepProvSession**](docs/DataReportingProvisioningSessionsAPI.md#createdatarepprovsession) | **Post** /sessions | Create a new Data Reporting Provisioning Session.
*IndividualDataReportingConfigurationAPI* | [**DeleteIndDataRepConfig**](docs/IndividualDataReportingConfigurationAPI.md#deleteinddatarepconfig) | **Delete** /sessions/{sessionId}/configurations/{configurationId} | Deletes an already existing Data Reporting Configuration resource.
*IndividualDataReportingConfigurationAPI* | [**GetIndDataRepConfig**](docs/IndividualDataReportingConfigurationAPI.md#getinddatarepconfig) | **Get** /sessions/{sessionId}/configurations/{configurationId} | Request the retrieval of an existing Individual Data Reporting Configuration resource.
*IndividualDataReportingConfigurationAPI* | [**ModifyIndDataRepConfig**](docs/IndividualDataReportingConfigurationAPI.md#modifyinddatarepconfig) | **Patch** /sessions/{sessionId}/configurations/{configurationId} | Request to modify an existing Individual Data Reporting Configuration resource.
*IndividualDataReportingConfigurationAPI* | [**UpdateIndDataRepConfig**](docs/IndividualDataReportingConfigurationAPI.md#updateinddatarepconfig) | **Put** /sessions/{sessionId}/configurations/{configurationId} | Request to update an existing Individual Data Reporting Configuration resource.
*IndividualDataReportingProvisioningSessionAPI* | [**DeleteIndDataRepProvSession**](docs/IndividualDataReportingProvisioningSessionAPI.md#deleteinddatarepprovsession) | **Delete** /sessions/{sessionId} | Deletes an already existing Individual Data Reporting Provisioning Session resource.
*IndividualDataReportingProvisioningSessionAPI* | [**GetIndDataRepProvSession**](docs/IndividualDataReportingProvisioningSessionAPI.md#getinddatarepprovsession) | **Get** /sessions/{sessionId} | Request the retrieval of an existing Individual Data Reporting Provisioning Session resource.


## Documentation For Models

 - [AfEvent](docs/AfEvent.md)
 - [CivicAddress](docs/CivicAddress.md)
 - [DataAccessProfile](docs/DataAccessProfile.md)
 - [DataAccessProfileLocationAccessRestrictions](docs/DataAccessProfileLocationAccessRestrictions.md)
 - [DataAccessProfileTimeAccessRestrictions](docs/DataAccessProfileTimeAccessRestrictions.md)
 - [DataAccessProfileUserAccessRestrictions](docs/DataAccessProfileUserAccessRestrictions.md)
 - [DataAccessProfileUserAccessRestrictionsUserIdsInner](docs/DataAccessProfileUserAccessRestrictionsUserIdsInner.md)
 - [DataAggregationFunctionType](docs/DataAggregationFunctionType.md)
 - [DataCollectionClientType](docs/DataCollectionClientType.md)
 - [DataReportingConfiguration](docs/DataReportingConfiguration.md)
 - [DataReportingConfigurationPatch](docs/DataReportingConfigurationPatch.md)
 - [DataReportingProvisioningSession](docs/DataReportingProvisioningSession.md)
 - [DataReportingRule](docs/DataReportingRule.md)
 - [DataSamplingRule](docs/DataSamplingRule.md)
 - [Ecgi](docs/Ecgi.md)
 - [EllipsoidArc](docs/EllipsoidArc.md)
 - [EventConsumerType](docs/EventConsumerType.md)
 - [GADShape](docs/GADShape.md)
 - [GNbId](docs/GNbId.md)
 - [GeographicArea](docs/GeographicArea.md)
 - [GeographicalCoordinates](docs/GeographicalCoordinates.md)
 - [GlobalRanNodeId](docs/GlobalRanNodeId.md)
 - [InvalidParam](docs/InvalidParam.md)
 - [Local2dPointUncertaintyEllipse](docs/Local2dPointUncertaintyEllipse.md)
 - [Local3dPointUncertaintyEllipsoid](docs/Local3dPointUncertaintyEllipsoid.md)
 - [LocalOrigin](docs/LocalOrigin.md)
 - [LocationArea5G](docs/LocationArea5G.md)
 - [Ncgi](docs/Ncgi.md)
 - [NetworkAreaInfo](docs/NetworkAreaInfo.md)
 - [PlmnId](docs/PlmnId.md)
 - [Point](docs/Point.md)
 - [PointAltitude](docs/PointAltitude.md)
 - [PointAltitudeUncertainty](docs/PointAltitudeUncertainty.md)
 - [PointUncertaintyCircle](docs/PointUncertaintyCircle.md)
 - [PointUncertaintyEllipse](docs/PointUncertaintyEllipse.md)
 - [Polygon](docs/Polygon.md)
 - [ProblemDetails](docs/ProblemDetails.md)
 - [RelativeCartesianLocation](docs/RelativeCartesianLocation.md)
 - [SupportedGADShapes](docs/SupportedGADShapes.md)
 - [Tai](docs/Tai.md)
 - [UncertaintyEllipse](docs/UncertaintyEllipse.md)
 - [UncertaintyEllipsoid](docs/UncertaintyEllipsoid.md)


## Documentation For Authorization


Authentication schemes defined for the API:
### oAuth2ClientCredentials


- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: N/A

Example

```go
auth := context.WithValue(context.Background(), OpenAPI_DataReportingProvisioning.ContextAccessToken, "ACCESSTOKENSTRING")
r, err := client.Service.Operation(auth, args)
```

Or via OAuth2 module to automatically refresh tokens and perform user authentication.

```go
import "golang.org/x/oauth2"

/* Perform OAuth2 round trip request and obtain a token */

tokenSource := oauth2cfg.TokenSource(createContext(httpClient), &token)
auth := context.WithValue(oauth2.NoContext, OpenAPI_DataReportingProvisioning.ContextOAuth2, tokenSource)
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



