/*
Namf_MBSBroadcast

AMF MBSBroadcast Service.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Namf_MBSBroadcast

import (
	"encoding/json"
	"fmt"
)

// checks if the ContextStatusNotification type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &ContextStatusNotification{}

// ContextStatusNotification Data within ContextStatusNotify Request
type ContextStatusNotification struct {
	MbsSessionId NullableMbsSessionId `json:"mbsSessionId"`
	// Integer where the allowed values correspond to the value range of an unsigned 16-bit integer.
	AreaSessionId   *int32           `json:"areaSessionId,omitempty"`
	N2MbsSmInfoList []N2MbsSmInfo    `json:"n2MbsSmInfoList,omitempty"`
	OperationEvents []OperationEvent `json:"operationEvents,omitempty"`
	OperationStatus *OperationStatus `json:"operationStatus,omitempty"`
	ReleasedInd     *bool            `json:"releasedInd,omitempty"`
}

type _ContextStatusNotification ContextStatusNotification

// NewContextStatusNotification instantiates a new ContextStatusNotification object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewContextStatusNotification(mbsSessionId NullableMbsSessionId) *ContextStatusNotification {
	this := ContextStatusNotification{}
	this.MbsSessionId = mbsSessionId
	return &this
}

// NewContextStatusNotificationWithDefaults instantiates a new ContextStatusNotification object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewContextStatusNotificationWithDefaults() *ContextStatusNotification {
	this := ContextStatusNotification{}
	return &this
}

// GetMbsSessionId returns the MbsSessionId field value
// If the value is explicit nil, the zero value for MbsSessionId will be returned
func (o *ContextStatusNotification) GetMbsSessionId() MbsSessionId {
	if o == nil || o.MbsSessionId.Get() == nil {
		var ret MbsSessionId
		return ret
	}

	return *o.MbsSessionId.Get()
}

// GetMbsSessionIdOk returns a tuple with the MbsSessionId field value
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *ContextStatusNotification) GetMbsSessionIdOk() (*MbsSessionId, bool) {
	if o == nil {
		return nil, false
	}
	return o.MbsSessionId.Get(), o.MbsSessionId.IsSet()
}

// SetMbsSessionId sets field value
func (o *ContextStatusNotification) SetMbsSessionId(v MbsSessionId) {
	o.MbsSessionId.Set(&v)
}

// GetAreaSessionId returns the AreaSessionId field value if set, zero value otherwise.
func (o *ContextStatusNotification) GetAreaSessionId() int32 {
	if o == nil || IsNil(o.AreaSessionId) {
		var ret int32
		return ret
	}
	return *o.AreaSessionId
}

// GetAreaSessionIdOk returns a tuple with the AreaSessionId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContextStatusNotification) GetAreaSessionIdOk() (*int32, bool) {
	if o == nil || IsNil(o.AreaSessionId) {
		return nil, false
	}
	return o.AreaSessionId, true
}

// HasAreaSessionId returns a boolean if a field has been set.
func (o *ContextStatusNotification) HasAreaSessionId() bool {
	if o != nil && !IsNil(o.AreaSessionId) {
		return true
	}

	return false
}

// SetAreaSessionId gets a reference to the given int32 and assigns it to the AreaSessionId field.
func (o *ContextStatusNotification) SetAreaSessionId(v int32) {
	o.AreaSessionId = &v
}

// GetN2MbsSmInfoList returns the N2MbsSmInfoList field value if set, zero value otherwise.
func (o *ContextStatusNotification) GetN2MbsSmInfoList() []N2MbsSmInfo {
	if o == nil || IsNil(o.N2MbsSmInfoList) {
		var ret []N2MbsSmInfo
		return ret
	}
	return o.N2MbsSmInfoList
}

// GetN2MbsSmInfoListOk returns a tuple with the N2MbsSmInfoList field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContextStatusNotification) GetN2MbsSmInfoListOk() ([]N2MbsSmInfo, bool) {
	if o == nil || IsNil(o.N2MbsSmInfoList) {
		return nil, false
	}
	return o.N2MbsSmInfoList, true
}

// HasN2MbsSmInfoList returns a boolean if a field has been set.
func (o *ContextStatusNotification) HasN2MbsSmInfoList() bool {
	if o != nil && !IsNil(o.N2MbsSmInfoList) {
		return true
	}

	return false
}

// SetN2MbsSmInfoList gets a reference to the given []N2MbsSmInfo and assigns it to the N2MbsSmInfoList field.
func (o *ContextStatusNotification) SetN2MbsSmInfoList(v []N2MbsSmInfo) {
	o.N2MbsSmInfoList = v
}

// GetOperationEvents returns the OperationEvents field value if set, zero value otherwise.
func (o *ContextStatusNotification) GetOperationEvents() []OperationEvent {
	if o == nil || IsNil(o.OperationEvents) {
		var ret []OperationEvent
		return ret
	}
	return o.OperationEvents
}

// GetOperationEventsOk returns a tuple with the OperationEvents field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContextStatusNotification) GetOperationEventsOk() ([]OperationEvent, bool) {
	if o == nil || IsNil(o.OperationEvents) {
		return nil, false
	}
	return o.OperationEvents, true
}

// HasOperationEvents returns a boolean if a field has been set.
func (o *ContextStatusNotification) HasOperationEvents() bool {
	if o != nil && !IsNil(o.OperationEvents) {
		return true
	}

	return false
}

// SetOperationEvents gets a reference to the given []OperationEvent and assigns it to the OperationEvents field.
func (o *ContextStatusNotification) SetOperationEvents(v []OperationEvent) {
	o.OperationEvents = v
}

// GetOperationStatus returns the OperationStatus field value if set, zero value otherwise.
func (o *ContextStatusNotification) GetOperationStatus() OperationStatus {
	if o == nil || IsNil(o.OperationStatus) {
		var ret OperationStatus
		return ret
	}
	return *o.OperationStatus
}

// GetOperationStatusOk returns a tuple with the OperationStatus field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContextStatusNotification) GetOperationStatusOk() (*OperationStatus, bool) {
	if o == nil || IsNil(o.OperationStatus) {
		return nil, false
	}
	return o.OperationStatus, true
}

// HasOperationStatus returns a boolean if a field has been set.
func (o *ContextStatusNotification) HasOperationStatus() bool {
	if o != nil && !IsNil(o.OperationStatus) {
		return true
	}

	return false
}

// SetOperationStatus gets a reference to the given OperationStatus and assigns it to the OperationStatus field.
func (o *ContextStatusNotification) SetOperationStatus(v OperationStatus) {
	o.OperationStatus = &v
}

// GetReleasedInd returns the ReleasedInd field value if set, zero value otherwise.
func (o *ContextStatusNotification) GetReleasedInd() bool {
	if o == nil || IsNil(o.ReleasedInd) {
		var ret bool
		return ret
	}
	return *o.ReleasedInd
}

// GetReleasedIndOk returns a tuple with the ReleasedInd field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ContextStatusNotification) GetReleasedIndOk() (*bool, bool) {
	if o == nil || IsNil(o.ReleasedInd) {
		return nil, false
	}
	return o.ReleasedInd, true
}

// HasReleasedInd returns a boolean if a field has been set.
func (o *ContextStatusNotification) HasReleasedInd() bool {
	if o != nil && !IsNil(o.ReleasedInd) {
		return true
	}

	return false
}

// SetReleasedInd gets a reference to the given bool and assigns it to the ReleasedInd field.
func (o *ContextStatusNotification) SetReleasedInd(v bool) {
	o.ReleasedInd = &v
}

func (o ContextStatusNotification) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o ContextStatusNotification) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["mbsSessionId"] = o.MbsSessionId.Get()
	if !IsNil(o.AreaSessionId) {
		toSerialize["areaSessionId"] = o.AreaSessionId
	}
	if !IsNil(o.N2MbsSmInfoList) {
		toSerialize["n2MbsSmInfoList"] = o.N2MbsSmInfoList
	}
	if !IsNil(o.OperationEvents) {
		toSerialize["operationEvents"] = o.OperationEvents
	}
	if !IsNil(o.OperationStatus) {
		toSerialize["operationStatus"] = o.OperationStatus
	}
	if !IsNil(o.ReleasedInd) {
		toSerialize["releasedInd"] = o.ReleasedInd
	}
	return toSerialize, nil
}

func (o *ContextStatusNotification) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"mbsSessionId",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varContextStatusNotification := _ContextStatusNotification{}

	err = json.Unmarshal(bytes, &varContextStatusNotification)

	if err != nil {
		return err
	}

	*o = ContextStatusNotification(varContextStatusNotification)

	return err
}

type NullableContextStatusNotification struct {
	value *ContextStatusNotification
	isSet bool
}

func (v NullableContextStatusNotification) Get() *ContextStatusNotification {
	return v.value
}

func (v *NullableContextStatusNotification) Set(val *ContextStatusNotification) {
	v.value = val
	v.isSet = true
}

func (v NullableContextStatusNotification) IsSet() bool {
	return v.isSet
}

func (v *NullableContextStatusNotification) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableContextStatusNotification(val *ContextStatusNotification) *NullableContextStatusNotification {
	return &NullableContextStatusNotification{value: val, isSet: true}
}

func (v NullableContextStatusNotification) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableContextStatusNotification) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
