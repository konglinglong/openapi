/*
Namf_MBSBroadcast

AMF MBSBroadcast Service.   © 2023, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.3
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Namf_MBSBroadcast

import (
	"encoding/json"
	"fmt"
)

// checks if the NgranFailureEvent type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &NgranFailureEvent{}

// NgranFailureEvent NG-RAN failure event for a NG-RAN
type NgranFailureEvent struct {
	NgranId                NullableGlobalRanNodeId `json:"ngranId"`
	NgranFailureIndication NgranFailureIndication  `json:"ngranFailureIndication"`
}

type _NgranFailureEvent NgranFailureEvent

// NewNgranFailureEvent instantiates a new NgranFailureEvent object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNgranFailureEvent(ngranId NullableGlobalRanNodeId, ngranFailureIndication NgranFailureIndication) *NgranFailureEvent {
	this := NgranFailureEvent{}
	this.NgranId = ngranId
	this.NgranFailureIndication = ngranFailureIndication
	return &this
}

// NewNgranFailureEventWithDefaults instantiates a new NgranFailureEvent object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNgranFailureEventWithDefaults() *NgranFailureEvent {
	this := NgranFailureEvent{}
	return &this
}

// GetNgranId returns the NgranId field value
// If the value is explicit nil, the zero value for GlobalRanNodeId will be returned
func (o *NgranFailureEvent) GetNgranId() GlobalRanNodeId {
	if o == nil || o.NgranId.Get() == nil {
		var ret GlobalRanNodeId
		return ret
	}

	return *o.NgranId.Get()
}

// GetNgranIdOk returns a tuple with the NgranId field value
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *NgranFailureEvent) GetNgranIdOk() (*GlobalRanNodeId, bool) {
	if o == nil {
		return nil, false
	}
	return o.NgranId.Get(), o.NgranId.IsSet()
}

// SetNgranId sets field value
func (o *NgranFailureEvent) SetNgranId(v GlobalRanNodeId) {
	o.NgranId.Set(&v)
}

// GetNgranFailureIndication returns the NgranFailureIndication field value
func (o *NgranFailureEvent) GetNgranFailureIndication() NgranFailureIndication {
	if o == nil {
		var ret NgranFailureIndication
		return ret
	}

	return o.NgranFailureIndication
}

// GetNgranFailureIndicationOk returns a tuple with the NgranFailureIndication field value
// and a boolean to check if the value has been set.
func (o *NgranFailureEvent) GetNgranFailureIndicationOk() (*NgranFailureIndication, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NgranFailureIndication, true
}

// SetNgranFailureIndication sets field value
func (o *NgranFailureEvent) SetNgranFailureIndication(v NgranFailureIndication) {
	o.NgranFailureIndication = v
}

func (o NgranFailureEvent) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o NgranFailureEvent) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["ngranId"] = o.NgranId.Get()
	toSerialize["ngranFailureIndication"] = o.NgranFailureIndication
	return toSerialize, nil
}

func (o *NgranFailureEvent) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"ngranId",
		"ngranFailureIndication",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varNgranFailureEvent := _NgranFailureEvent{}

	err = json.Unmarshal(bytes, &varNgranFailureEvent)

	if err != nil {
		return err
	}

	*o = NgranFailureEvent(varNgranFailureEvent)

	return err
}

type NullableNgranFailureEvent struct {
	value *NgranFailureEvent
	isSet bool
}

func (v NullableNgranFailureEvent) Get() *NgranFailureEvent {
	return v.value
}

func (v *NullableNgranFailureEvent) Set(val *NgranFailureEvent) {
	v.value = val
	v.isSet = true
}

func (v NullableNgranFailureEvent) IsSet() bool {
	return v.isSet
}

func (v *NullableNgranFailureEvent) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNgranFailureEvent(val *NgranFailureEvent) *NullableNgranFailureEvent {
	return &NullableNgranFailureEvent{value: val, isSet: true}
}

func (v NullableNgranFailureEvent) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNgranFailureEvent) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
