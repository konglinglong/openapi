/*
CAPIF_Events_API

API for event subscription management.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_CAPIF_Events_API

import (
	"encoding/json"
	"fmt"
)

// checks if the AefProfile type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &AefProfile{}

// AefProfile Represents the AEF profile data.
type AefProfile struct {
	// Identifier of the API exposing function
	AefId string `json:"aefId"`
	// API version
	Versions   []Version   `json:"versions"`
	Protocol   *Protocol   `json:"protocol,omitempty"`
	DataFormat *DataFormat `json:"dataFormat,omitempty"`
	// Security methods supported by the AEF
	SecurityMethods []SecurityMethod `json:"securityMethods,omitempty"`
	// Domain to which API belongs to
	DomainName *string `json:"domainName,omitempty"`
	// Interface details
	InterfaceDescriptions []InterfaceDescription `json:"interfaceDescriptions,omitempty"`
	AefLocation           *AefLocation           `json:"aefLocation,omitempty"`
}

type _AefProfile AefProfile

// NewAefProfile instantiates a new AefProfile object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAefProfile(aefId string, versions []Version) *AefProfile {
	this := AefProfile{}
	return &this
}

// NewAefProfileWithDefaults instantiates a new AefProfile object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAefProfileWithDefaults() *AefProfile {
	this := AefProfile{}
	return &this
}

// GetAefId returns the AefId field value
func (o *AefProfile) GetAefId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.AefId
}

// GetAefIdOk returns a tuple with the AefId field value
// and a boolean to check if the value has been set.
func (o *AefProfile) GetAefIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.AefId, true
}

// SetAefId sets field value
func (o *AefProfile) SetAefId(v string) {
	o.AefId = v
}

// GetVersions returns the Versions field value
func (o *AefProfile) GetVersions() []Version {
	if o == nil {
		var ret []Version
		return ret
	}

	return o.Versions
}

// GetVersionsOk returns a tuple with the Versions field value
// and a boolean to check if the value has been set.
func (o *AefProfile) GetVersionsOk() ([]Version, bool) {
	if o == nil {
		return nil, false
	}
	return o.Versions, true
}

// SetVersions sets field value
func (o *AefProfile) SetVersions(v []Version) {
	o.Versions = v
}

// GetProtocol returns the Protocol field value if set, zero value otherwise.
func (o *AefProfile) GetProtocol() Protocol {
	if o == nil || IsNil(o.Protocol) {
		var ret Protocol
		return ret
	}
	return *o.Protocol
}

// GetProtocolOk returns a tuple with the Protocol field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetProtocolOk() (*Protocol, bool) {
	if o == nil || IsNil(o.Protocol) {
		return nil, false
	}
	return o.Protocol, true
}

// HasProtocol returns a boolean if a field has been set.
func (o *AefProfile) HasProtocol() bool {
	if o != nil && !IsNil(o.Protocol) {
		return true
	}

	return false
}

// SetProtocol gets a reference to the given Protocol and assigns it to the Protocol field.
func (o *AefProfile) SetProtocol(v Protocol) {
	o.Protocol = &v
}

// GetDataFormat returns the DataFormat field value if set, zero value otherwise.
func (o *AefProfile) GetDataFormat() DataFormat {
	if o == nil || IsNil(o.DataFormat) {
		var ret DataFormat
		return ret
	}
	return *o.DataFormat
}

// GetDataFormatOk returns a tuple with the DataFormat field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetDataFormatOk() (*DataFormat, bool) {
	if o == nil || IsNil(o.DataFormat) {
		return nil, false
	}
	return o.DataFormat, true
}

// HasDataFormat returns a boolean if a field has been set.
func (o *AefProfile) HasDataFormat() bool {
	if o != nil && !IsNil(o.DataFormat) {
		return true
	}

	return false
}

// SetDataFormat gets a reference to the given DataFormat and assigns it to the DataFormat field.
func (o *AefProfile) SetDataFormat(v DataFormat) {
	o.DataFormat = &v
}

// GetSecurityMethods returns the SecurityMethods field value if set, zero value otherwise.
func (o *AefProfile) GetSecurityMethods() []SecurityMethod {
	if o == nil || IsNil(o.SecurityMethods) {
		var ret []SecurityMethod
		return ret
	}
	return o.SecurityMethods
}

// GetSecurityMethodsOk returns a tuple with the SecurityMethods field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetSecurityMethodsOk() ([]SecurityMethod, bool) {
	if o == nil || IsNil(o.SecurityMethods) {
		return nil, false
	}
	return o.SecurityMethods, true
}

// HasSecurityMethods returns a boolean if a field has been set.
func (o *AefProfile) HasSecurityMethods() bool {
	if o != nil && !IsNil(o.SecurityMethods) {
		return true
	}

	return false
}

// SetSecurityMethods gets a reference to the given []SecurityMethod and assigns it to the SecurityMethods field.
func (o *AefProfile) SetSecurityMethods(v []SecurityMethod) {
	o.SecurityMethods = v
}

// GetDomainName returns the DomainName field value if set, zero value otherwise.
func (o *AefProfile) GetDomainName() string {
	if o == nil || IsNil(o.DomainName) {
		var ret string
		return ret
	}
	return *o.DomainName
}

// GetDomainNameOk returns a tuple with the DomainName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetDomainNameOk() (*string, bool) {
	if o == nil || IsNil(o.DomainName) {
		return nil, false
	}
	return o.DomainName, true
}

// HasDomainName returns a boolean if a field has been set.
func (o *AefProfile) HasDomainName() bool {
	if o != nil && !IsNil(o.DomainName) {
		return true
	}

	return false
}

// SetDomainName gets a reference to the given string and assigns it to the DomainName field.
func (o *AefProfile) SetDomainName(v string) {
	o.DomainName = &v
}

// GetInterfaceDescriptions returns the InterfaceDescriptions field value if set, zero value otherwise.
func (o *AefProfile) GetInterfaceDescriptions() []InterfaceDescription {
	if o == nil || IsNil(o.InterfaceDescriptions) {
		var ret []InterfaceDescription
		return ret
	}
	return o.InterfaceDescriptions
}

// GetInterfaceDescriptionsOk returns a tuple with the InterfaceDescriptions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetInterfaceDescriptionsOk() ([]InterfaceDescription, bool) {
	if o == nil || IsNil(o.InterfaceDescriptions) {
		return nil, false
	}
	return o.InterfaceDescriptions, true
}

// HasInterfaceDescriptions returns a boolean if a field has been set.
func (o *AefProfile) HasInterfaceDescriptions() bool {
	if o != nil && !IsNil(o.InterfaceDescriptions) {
		return true
	}

	return false
}

// SetInterfaceDescriptions gets a reference to the given []InterfaceDescription and assigns it to the InterfaceDescriptions field.
func (o *AefProfile) SetInterfaceDescriptions(v []InterfaceDescription) {
	o.InterfaceDescriptions = v
}

// GetAefLocation returns the AefLocation field value if set, zero value otherwise.
func (o *AefProfile) GetAefLocation() AefLocation {
	if o == nil || IsNil(o.AefLocation) {
		var ret AefLocation
		return ret
	}
	return *o.AefLocation
}

// GetAefLocationOk returns a tuple with the AefLocation field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AefProfile) GetAefLocationOk() (*AefLocation, bool) {
	if o == nil || IsNil(o.AefLocation) {
		return nil, false
	}
	return o.AefLocation, true
}

// HasAefLocation returns a boolean if a field has been set.
func (o *AefProfile) HasAefLocation() bool {
	if o != nil && !IsNil(o.AefLocation) {
		return true
	}

	return false
}

// SetAefLocation gets a reference to the given AefLocation and assigns it to the AefLocation field.
func (o *AefProfile) SetAefLocation(v AefLocation) {
	o.AefLocation = &v
}

func (o AefProfile) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o AefProfile) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["aefId"] = o.AefId
	toSerialize["versions"] = o.Versions
	if !IsNil(o.Protocol) {
		toSerialize["protocol"] = o.Protocol
	}
	if !IsNil(o.DataFormat) {
		toSerialize["dataFormat"] = o.DataFormat
	}
	if !IsNil(o.SecurityMethods) {
		toSerialize["securityMethods"] = o.SecurityMethods
	}
	if !IsNil(o.DomainName) {
		toSerialize["domainName"] = o.DomainName
	}
	if !IsNil(o.InterfaceDescriptions) {
		toSerialize["interfaceDescriptions"] = o.InterfaceDescriptions
	}
	if !IsNil(o.AefLocation) {
		toSerialize["aefLocation"] = o.AefLocation
	}
	return toSerialize, nil
}

func (o *AefProfile) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"aefId",
		"versions",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varAefProfile := _AefProfile{}

	err = json.Unmarshal(bytes, &varAefProfile)

	if err != nil {
		return err
	}

	*o = AefProfile(varAefProfile)

	return err
}

type NullableAefProfile struct {
	value *AefProfile
	isSet bool
}

func (v NullableAefProfile) Get() *AefProfile {
	return v.value
}

func (v *NullableAefProfile) Set(val *AefProfile) {
	v.value = val
	v.isSet = true
}

func (v NullableAefProfile) IsSet() bool {
	return v.isSet
}

func (v *NullableAefProfile) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAefProfile(val *AefProfile) *NullableAefProfile {
	return &NullableAefProfile{value: val, isSet: true}
}

func (v NullableAefProfile) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAefProfile) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
