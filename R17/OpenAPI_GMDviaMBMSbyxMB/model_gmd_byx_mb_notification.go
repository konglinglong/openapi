/*
GMDviaMBMSbyxMB

API for Group Message Delivery via MBMS by xMB   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.2.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_GMDviaMBMSbyxMB

import (
	"encoding/json"
	"fmt"
)

// checks if the GMDByxMBNotification type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &GMDByxMBNotification{}

// GMDByxMBNotification Represents a group message delivery notification.
type GMDByxMBNotification struct {
	// string formatted according to IETF RFC 3986 identifying a referenced resource.
	Transaction string `json:"transaction"`
	// Indicates whether delivery of group message payload was successful(TRUE) or not (FALSE)
	DeliveryTriggerStatus bool `json:"deliveryTriggerStatus"`
}

type _GMDByxMBNotification GMDByxMBNotification

// NewGMDByxMBNotification instantiates a new GMDByxMBNotification object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGMDByxMBNotification(transaction string, deliveryTriggerStatus bool) *GMDByxMBNotification {
	this := GMDByxMBNotification{}
	this.Transaction = transaction
	this.DeliveryTriggerStatus = deliveryTriggerStatus
	return &this
}

// NewGMDByxMBNotificationWithDefaults instantiates a new GMDByxMBNotification object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGMDByxMBNotificationWithDefaults() *GMDByxMBNotification {
	this := GMDByxMBNotification{}
	return &this
}

// GetTransaction returns the Transaction field value
func (o *GMDByxMBNotification) GetTransaction() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Transaction
}

// GetTransactionOk returns a tuple with the Transaction field value
// and a boolean to check if the value has been set.
func (o *GMDByxMBNotification) GetTransactionOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Transaction, true
}

// SetTransaction sets field value
func (o *GMDByxMBNotification) SetTransaction(v string) {
	o.Transaction = v
}

// GetDeliveryTriggerStatus returns the DeliveryTriggerStatus field value
func (o *GMDByxMBNotification) GetDeliveryTriggerStatus() bool {
	if o == nil {
		var ret bool
		return ret
	}

	return o.DeliveryTriggerStatus
}

// GetDeliveryTriggerStatusOk returns a tuple with the DeliveryTriggerStatus field value
// and a boolean to check if the value has been set.
func (o *GMDByxMBNotification) GetDeliveryTriggerStatusOk() (*bool, bool) {
	if o == nil {
		return nil, false
	}
	return &o.DeliveryTriggerStatus, true
}

// SetDeliveryTriggerStatus sets field value
func (o *GMDByxMBNotification) SetDeliveryTriggerStatus(v bool) {
	o.DeliveryTriggerStatus = v
}

func (o GMDByxMBNotification) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o GMDByxMBNotification) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["transaction"] = o.Transaction
	toSerialize["deliveryTriggerStatus"] = o.DeliveryTriggerStatus
	return toSerialize, nil
}

func (o *GMDByxMBNotification) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"transaction",
		"deliveryTriggerStatus",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varGMDByxMBNotification := _GMDByxMBNotification{}

	err = json.Unmarshal(bytes, &varGMDByxMBNotification)

	if err != nil {
		return err
	}

	*o = GMDByxMBNotification(varGMDByxMBNotification)

	return err
}

type NullableGMDByxMBNotification struct {
	value *GMDByxMBNotification
	isSet bool
}

func (v NullableGMDByxMBNotification) Get() *GMDByxMBNotification {
	return v.value
}

func (v *NullableGMDByxMBNotification) Set(val *GMDByxMBNotification) {
	v.value = val
	v.isSet = true
}

func (v NullableGMDByxMBNotification) IsSet() bool {
	return v.isSet
}

func (v *NullableGMDByxMBNotification) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGMDByxMBNotification(val *GMDByxMBNotification) *NullableGMDByxMBNotification {
	return &NullableGMDByxMBNotification{value: val, isSet: true}
}

func (v NullableGMDByxMBNotification) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGMDByxMBNotification) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
