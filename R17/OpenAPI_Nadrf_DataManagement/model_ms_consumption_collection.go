/*
Nadrf_DataManagement

ADRF Data Management Service.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nadrf_DataManagement

import (
	"encoding/json"
	"fmt"
)

// checks if the MsConsumptionCollection type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &MsConsumptionCollection{}

// MsConsumptionCollection Contains the Media Streaming Consumption information collected for an UE Application via AF.
type MsConsumptionCollection struct {
	MsConsumps []string `json:"msConsumps"`
}

type _MsConsumptionCollection MsConsumptionCollection

// NewMsConsumptionCollection instantiates a new MsConsumptionCollection object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMsConsumptionCollection(msConsumps []string) *MsConsumptionCollection {
	this := MsConsumptionCollection{}
	this.MsConsumps = msConsumps
	return &this
}

// NewMsConsumptionCollectionWithDefaults instantiates a new MsConsumptionCollection object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMsConsumptionCollectionWithDefaults() *MsConsumptionCollection {
	this := MsConsumptionCollection{}
	return &this
}

// GetMsConsumps returns the MsConsumps field value
func (o *MsConsumptionCollection) GetMsConsumps() []string {
	if o == nil {
		var ret []string
		return ret
	}

	return o.MsConsumps
}

// GetMsConsumpsOk returns a tuple with the MsConsumps field value
// and a boolean to check if the value has been set.
func (o *MsConsumptionCollection) GetMsConsumpsOk() ([]string, bool) {
	if o == nil {
		return nil, false
	}
	return o.MsConsumps, true
}

// SetMsConsumps sets field value
func (o *MsConsumptionCollection) SetMsConsumps(v []string) {
	o.MsConsumps = v
}

func (o MsConsumptionCollection) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o MsConsumptionCollection) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	toSerialize["msConsumps"] = o.MsConsumps
	return toSerialize, nil
}

func (o *MsConsumptionCollection) UnmarshalJSON(bytes []byte) (err error) {
	// This validates that all required properties are included in the JSON object
	// by unmarshalling the object into a generic map with string keys and checking
	// that every required field exists as a key in the generic map.
	requiredProperties := []string{
		"msConsumps",
	}

	allProperties := make(map[string]interface{})

	err = json.Unmarshal(bytes, &allProperties)

	if err != nil {
		return err
	}

	for _, requiredProperty := range requiredProperties {
		if _, exists := allProperties[requiredProperty]; !exists {
			return fmt.Errorf("no value given for required property %v", requiredProperty)
		}
	}

	varMsConsumptionCollection := _MsConsumptionCollection{}

	err = json.Unmarshal(bytes, &varMsConsumptionCollection)

	if err != nil {
		return err
	}

	*o = MsConsumptionCollection(varMsConsumptionCollection)

	return err
}

type NullableMsConsumptionCollection struct {
	value *MsConsumptionCollection
	isSet bool
}

func (v NullableMsConsumptionCollection) Get() *MsConsumptionCollection {
	return v.value
}

func (v *NullableMsConsumptionCollection) Set(val *MsConsumptionCollection) {
	v.value = val
	v.isSet = true
}

func (v NullableMsConsumptionCollection) IsSet() bool {
	return v.isSet
}

func (v *NullableMsConsumptionCollection) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMsConsumptionCollection(val *MsConsumptionCollection) *NullableMsConsumptionCollection {
	return &NullableMsConsumptionCollection{value: val, isSet: true}
}

func (v NullableMsConsumptionCollection) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMsConsumptionCollection) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
