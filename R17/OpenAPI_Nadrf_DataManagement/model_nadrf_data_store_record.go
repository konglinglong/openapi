/*
Nadrf_DataManagement

ADRF Data Management Service.   © 2022, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).   All rights reserved.

API version: 1.0.2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package OpenAPI_Nadrf_DataManagement

import (
	"encoding/json"
)

// checks if the NadrfDataStoreRecord type satisfies the MappedNullable interface at compile time
var _ MappedNullable = &NadrfDataStoreRecord{}

// NadrfDataStoreRecord Represents an Individual ADRF Data Store Record.
type NadrfDataStoreRecord struct {
	DataNotif NullableDataNotification `json:"dataNotif,omitempty"`
	// List of analytics subscription notifications.
	AnaNotifications []NnwdafEventsSubscriptionNotification `json:"anaNotifications,omitempty"`
	// Represents the subscription information of the corresponding analytics notification.
	AnaSub []NnwdafEventsSubscription `json:"anaSub,omitempty"`
	// Represents the subscription information of the corresponding data notification.
	DataSub []DataSubscription `json:"dataSub,omitempty"`
}

// NewNadrfDataStoreRecord instantiates a new NadrfDataStoreRecord object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNadrfDataStoreRecord() *NadrfDataStoreRecord {
	this := NadrfDataStoreRecord{}
	return &this
}

// NewNadrfDataStoreRecordWithDefaults instantiates a new NadrfDataStoreRecord object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNadrfDataStoreRecordWithDefaults() *NadrfDataStoreRecord {
	this := NadrfDataStoreRecord{}
	return &this
}

// GetDataNotif returns the DataNotif field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *NadrfDataStoreRecord) GetDataNotif() DataNotification {
	if o == nil || IsNil(o.DataNotif.Get()) {
		var ret DataNotification
		return ret
	}
	return *o.DataNotif.Get()
}

// GetDataNotifOk returns a tuple with the DataNotif field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *NadrfDataStoreRecord) GetDataNotifOk() (*DataNotification, bool) {
	if o == nil {
		return nil, false
	}
	return o.DataNotif.Get(), o.DataNotif.IsSet()
}

// HasDataNotif returns a boolean if a field has been set.
func (o *NadrfDataStoreRecord) HasDataNotif() bool {
	if o != nil && o.DataNotif.IsSet() {
		return true
	}

	return false
}

// SetDataNotif gets a reference to the given NullableDataNotification and assigns it to the DataNotif field.
func (o *NadrfDataStoreRecord) SetDataNotif(v DataNotification) {
	o.DataNotif.Set(&v)
}

// SetDataNotifNil sets the value for DataNotif to be an explicit nil
func (o *NadrfDataStoreRecord) SetDataNotifNil() {
	o.DataNotif.Set(nil)
}

// UnsetDataNotif ensures that no value is present for DataNotif, not even an explicit nil
func (o *NadrfDataStoreRecord) UnsetDataNotif() {
	o.DataNotif.Unset()
}

// GetAnaNotifications returns the AnaNotifications field value if set, zero value otherwise.
func (o *NadrfDataStoreRecord) GetAnaNotifications() []NnwdafEventsSubscriptionNotification {
	if o == nil || IsNil(o.AnaNotifications) {
		var ret []NnwdafEventsSubscriptionNotification
		return ret
	}
	return o.AnaNotifications
}

// GetAnaNotificationsOk returns a tuple with the AnaNotifications field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NadrfDataStoreRecord) GetAnaNotificationsOk() ([]NnwdafEventsSubscriptionNotification, bool) {
	if o == nil || IsNil(o.AnaNotifications) {
		return nil, false
	}
	return o.AnaNotifications, true
}

// HasAnaNotifications returns a boolean if a field has been set.
func (o *NadrfDataStoreRecord) HasAnaNotifications() bool {
	if o != nil && !IsNil(o.AnaNotifications) {
		return true
	}

	return false
}

// SetAnaNotifications gets a reference to the given []NnwdafEventsSubscriptionNotification and assigns it to the AnaNotifications field.
func (o *NadrfDataStoreRecord) SetAnaNotifications(v []NnwdafEventsSubscriptionNotification) {
	o.AnaNotifications = v
}

// GetAnaSub returns the AnaSub field value if set, zero value otherwise.
func (o *NadrfDataStoreRecord) GetAnaSub() []NnwdafEventsSubscription {
	if o == nil || IsNil(o.AnaSub) {
		var ret []NnwdafEventsSubscription
		return ret
	}
	return o.AnaSub
}

// GetAnaSubOk returns a tuple with the AnaSub field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NadrfDataStoreRecord) GetAnaSubOk() ([]NnwdafEventsSubscription, bool) {
	if o == nil || IsNil(o.AnaSub) {
		return nil, false
	}
	return o.AnaSub, true
}

// HasAnaSub returns a boolean if a field has been set.
func (o *NadrfDataStoreRecord) HasAnaSub() bool {
	if o != nil && !IsNil(o.AnaSub) {
		return true
	}

	return false
}

// SetAnaSub gets a reference to the given []NnwdafEventsSubscription and assigns it to the AnaSub field.
func (o *NadrfDataStoreRecord) SetAnaSub(v []NnwdafEventsSubscription) {
	o.AnaSub = v
}

// GetDataSub returns the DataSub field value if set, zero value otherwise.
func (o *NadrfDataStoreRecord) GetDataSub() []DataSubscription {
	if o == nil || IsNil(o.DataSub) {
		var ret []DataSubscription
		return ret
	}
	return o.DataSub
}

// GetDataSubOk returns a tuple with the DataSub field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NadrfDataStoreRecord) GetDataSubOk() ([]DataSubscription, bool) {
	if o == nil || IsNil(o.DataSub) {
		return nil, false
	}
	return o.DataSub, true
}

// HasDataSub returns a boolean if a field has been set.
func (o *NadrfDataStoreRecord) HasDataSub() bool {
	if o != nil && !IsNil(o.DataSub) {
		return true
	}

	return false
}

// SetDataSub gets a reference to the given []DataSubscription and assigns it to the DataSub field.
func (o *NadrfDataStoreRecord) SetDataSub(v []DataSubscription) {
	o.DataSub = v
}

func (o NadrfDataStoreRecord) MarshalJSON() ([]byte, error) {
	toSerialize, err := o.ToMap()
	if err != nil {
		return []byte{}, err
	}
	return json.Marshal(toSerialize)
}

func (o NadrfDataStoreRecord) ToMap() (map[string]interface{}, error) {
	toSerialize := map[string]interface{}{}
	if o.DataNotif.IsSet() {
		toSerialize["dataNotif"] = o.DataNotif.Get()
	}
	if !IsNil(o.AnaNotifications) {
		toSerialize["anaNotifications"] = o.AnaNotifications
	}
	if !IsNil(o.AnaSub) {
		toSerialize["anaSub"] = o.AnaSub
	}
	if !IsNil(o.DataSub) {
		toSerialize["dataSub"] = o.DataSub
	}
	return toSerialize, nil
}

type NullableNadrfDataStoreRecord struct {
	value *NadrfDataStoreRecord
	isSet bool
}

func (v NullableNadrfDataStoreRecord) Get() *NadrfDataStoreRecord {
	return v.value
}

func (v *NullableNadrfDataStoreRecord) Set(val *NadrfDataStoreRecord) {
	v.value = val
	v.isSet = true
}

func (v NullableNadrfDataStoreRecord) IsSet() bool {
	return v.isSet
}

func (v *NullableNadrfDataStoreRecord) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNadrfDataStoreRecord(val *NadrfDataStoreRecord) *NullableNadrfDataStoreRecord {
	return &NullableNadrfDataStoreRecord{value: val, isSet: true}
}

func (v NullableNadrfDataStoreRecord) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNadrfDataStoreRecord) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
